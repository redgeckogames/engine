﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.ReptileEngine;
using Wytches.ReptileEngine.AnimationComponents;

namespace Wytches.Scripts
{
    public class Movement : Script
    {
        private bool boolInventory = false;

        public Movement(GameObject gameObject) : base(gameObject)
        {
        }

        public override void Update(GameTime gameTime)
        {
            KeyboardState state = Keyboard.GetState();
            if (state.GetPressedKeys().Length >= 2)
            {
                if (Input.GetKeyDown(Keys.D) && Input.GetKeyDown(Keys.S))
                {
                    GameObject.Transform.Translate((new Vector3(-0.5f, 0, -0.5f)));
                    GameObject.Forward = false;
                    GameObject.Backward = true;
                    GameObject.Right = true;
                    GameObject.Left = false;
                }
                else
                if (Input.GetKeyDown(Keys.S) && Input.GetKeyDown(Keys.A))
                {
                    GameObject.Transform.Translate((new Vector3(0.5f, 0, -0.5f)));
                    GameObject.Forward = false;
                    GameObject.Backward = true;
                    GameObject.Right = false;
                    GameObject.Left = true;
                }
                else
                if (Input.GetKeyDown(Keys.A) && Input.GetKeyDown(Keys.W))
                {
                    GameObject.Transform.Translate((new Vector3(0.5f, 0, 0.5f)));
                    GameObject.Forward = true;
                    GameObject.Backward = false;
                    GameObject.Right = false;
                    GameObject.Left = true;
                }
                else
                if (Input.GetKeyDown(Keys.W) && Input.GetKeyDown(Keys.D))
                {
                    GameObject.Transform.Translate((new Vector3(-0.5f, 0, 0.5f)));
                    GameObject.Forward = true;
                    GameObject.Backward = false;
                    GameObject.Right = true;
                    GameObject.Left = false;
                }
            }
            else
            {
                if (Input.GetKeyDown(Keys.D))
                {
                    GameObject.Transform.Translate(new Vector3(-0.5f, 0, 0));
                    GameObject.Forward = false;
                    GameObject.Backward = false;
                    GameObject.Right = true;
                    GameObject.Left = false;
                }
                if (Input.GetKeyDown(Keys.A))
                {
                    GameObject.Transform.Translate(new Vector3(0.5f, 0, 0));
                    GameObject.Forward = false;
                    GameObject.Backward = false;
                    GameObject.Right = false;
                    GameObject.Left = true;
                }
                if (Input.GetKeyDown(Keys.W))
                {
                    GameObject.Transform.Translate(new Vector3(0, 0, 0.5f));
                    GameObject.Forward = true;
                    GameObject.Backward = false;
                    GameObject.Right = false;
                    GameObject.Left = false;
                }
                if (Input.GetKeyDown(Keys.S))
                {
                    GameObject.Transform.Translate(new Vector3(0, 0, -0.5f));
                    GameObject.Forward = false;
                    GameObject.Backward = true;
                    GameObject.Right = false;
                    GameObject.Left = false;
                }
                if (Input.GetKeyDown((Keys.M)))
                {
                    GameObject.Transform.Rotate(new Vector3(0, -1, 0), 10);
                }
                if (Input.GetKeyDown((Keys.N)))
                {
                    GameObject.Transform.Rotate(new Vector3(0, 1, 0), 10);
                }
            }
            if (state.GetPressedKeys().Length == 0)
            {
                if (!(this.GameObject.GetComponent<Animation>().animationStateName == AnimationStateName.Idle))
                {
                    AnimationState.LoadAnimationForGameObject(this.GameObject, AnimationStateName.Idle);
                }
            }
            else if (!(this.GameObject.GetComponent<Animation>().animationStateName == AnimationStateName.Walking))
            {
                AnimationState.LoadAnimationForGameObject(this.GameObject, AnimationStateName.Walking);
            }

            base.Update(gameTime);
        }
    }
}