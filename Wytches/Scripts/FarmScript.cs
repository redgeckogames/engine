﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Wytches.GameOwn;
using Wytches.ReptileEngine;
using System.Timers;
using System.Diagnostics;
using Microsoft.Xna.Framework.Graphics;
using NLog;

namespace Wytches.Scripts
{
    public class FarmScript : Script
    {
        private FarmTile farmTile;
        private Timer growTimer;
        private Logger logger = LogManager.GetCurrentClassLogger();

        public FarmScript(GameObject gameObject) : base(gameObject)
        {
            farmTile = new FarmTile();
        }

        public void PlantSeed(Farmable plant)
        {
            this.farmTile.Plant = plant;
            logger.Info("Grow Status : " + farmTile.Plant.HowMuchHasGrown().ToString());
            //Debug.WriteLine("Grow Status : " + farmTile.Plant.HowMuchHasGrown().ToString());
            growTimer = new Timer(farmTile.Plant.getTimeNeededToGrow());
            growTimer.Elapsed += groWPlant;
            growTimer.AutoReset = true;

            growTimer.Start();
        }

        private void groWPlant(object sender, ElapsedEventArgs e)
        {
            this.farmTile.Plant.GrowSeed();
            logger.Info("Grow Status : " + (((Item)farmTile.Plant).name).ToString() + "  " + farmTile.Plant.HowMuchHasGrown().ToString());
            //Debug.WriteLine("Grow Status : " + (((Item)farmTile.Plant).name).ToString() + "  " + farmTile.Plant.HowMuchHasGrown().ToString());
        }

        public Farmable HarvestPlant()
        {
            return this.farmTile.Plant.Harvest();
        }

        public override void Update(GameTime gameTime)
        {
            if (farmTile.Plant.HowMuchHasGrown() == PlantGrowth.Seed)
            {
                GameObject.GetComponent<MeshRenderer>().LoadModel((ModelPlantDB.GetModel3dPath("seed")));
                GameObject.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath("seedDiffuseTexture"));
                GameObject.GetComponent<MeshRenderer>().NormalTexture = MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath("seedNormalTexture"));
            }
            if (farmTile.Plant.HowMuchHasGrown() == PlantGrowth.UnRipe)
            {
                GameObject.GetComponent<MeshRenderer>().LoadModel((ModelPlantDB.GetModel3dPath("unrpie")));
                GameObject.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath("unrpieDiffuseTexture"));
                GameObject.GetComponent<MeshRenderer>().NormalTexture = MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath("unrpieNormalTexture"));
            }
            if (farmTile.Plant.HowMuchHasGrown() == PlantGrowth.FullyGrown)
            {
                GameObject.GetComponent<MeshRenderer>().LoadModel((ModelPlantDB.GetModel3dPath(((Item)farmTile.Plant).name)));
                GameObject.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath(((Item)farmTile.Plant).name +"DiffuseTexture"));
                GameObject.GetComponent<MeshRenderer>().NormalTexture = MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath( ((Item)farmTile.Plant).name +"NormalTexture"));
            }

            base.Update(gameTime);
        }

        public override void Draw(Camera camera)
        {
            base.Draw(camera);
        }
    }
}