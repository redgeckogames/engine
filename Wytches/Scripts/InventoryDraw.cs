﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.GameOwn;

using Wytches.ReptileEngine;

namespace Wytches.Scripts
{
    public class InventoryDraw : DrawableGameComponent
    {
        private bool _isDragging = false;

        protected Game game;

        protected SpriteBatch spriteBatch;
        private BasicEffect basicEffect;
        private InventoryScript inventoryScript;
        private Camera camera;
        private List<ItemSlot> Inventory;
        private Vector2 inventoryPosition;

        public Vector2 InventoryPosition
        {
            get
            {
                return this.inventoryPosition;
            }
        }

        private const int _tileSize = 70;
        private int[] sizeInventory = new int[] { 4, 4 };

        private SpriteFont _defaultFont;

        public InventoryDraw(Microsoft.Xna.Framework.Game game, Camera camera, InventoryScript inventoryScript) : base(game)
        {
            this.game = game;
            spriteBatch = MySpriteBatch.returnSpriteBatch();
            this.camera = camera;
            this.Inventory = inventoryScript.Inventory;
            this.inventoryScript = inventoryScript;
            inventoryScript.InventoryChanged += OnInventoryChanged;

            inventoryPosition = new Vector2(GraphicsDevice.Viewport.TitleSafeArea.Width / 2 + _tileSize / 2 +100, (GraphicsDevice.Viewport.TitleSafeArea.Height - Inventory.Count / sizeInventory[1] * _tileSize) / 2 +20);

            _defaultFont = MyContentManager.Load<SpriteFont>("DefaultFont");

            basicEffect = new BasicEffect(GraphicsDevice)
            {
                TextureEnabled = true,
                VertexColorEnabled = true,
            };
        }

        private void OnInventoryChanged(object sender, EventArgs e)
        {
            this.Inventory = inventoryScript.Inventory;
        }

        public override void Draw(GameTime gameTime)
        {
            Game.IsMouseVisible = true;

            //wyłącznie Z buffora i rysowanie spirtów na rzeczach z 3d
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.DepthStencilState = DepthStencilState.None;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            Vector3 textPosition = new Vector3(0, 300, 0);

            basicEffect.World = Matrix.CreateScale(1, -1, 1);
            basicEffect.View = camera.view;
            basicEffect.Projection = camera.projection;
            //start drawing

            spriteBatch.Begin();

            //DrawText();
            DrawBoard();
            //DrawDraggableTile();

            //end drawing
            spriteBatch.End();

            base.Draw(gameTime);
        }

        //Draws the text on the screen
        private void DrawText()
        {
            spriteBatch.DrawString(_defaultFont, "Tilebased XNA drag and drop sample", new Vector2(100, 20), Color.White);
            spriteBatch.DrawString(_defaultFont, "Drag new tile onto board with left mousebutton", new Vector2(100, 50), Color.White);
            spriteBatch.DrawString(_defaultFont, "Remove tiles with right-click", new Vector2(100, 90), Color.White);
            spriteBatch.DrawString(_defaultFont, "www.Xnafan.net", new Vector2(725, 665), Color.White);
        }

        ////draws the draggable tile either under the mouse, if it is currently being dragged, or in its default position
        //private void DrawDraggableTile()
        //{
        //    if (_isDragging)
        //    {
        //        spriteBatch.Draw(MyContentManager.Load<Texture2D>("Objects\\inventory\\cauldron1"), new Rectangle((int)(_currentMousePosition.X - _tileSize / 2), (int)(_currentMousePosition.Y - _tileSize / 2), _tileSize, _tileSize), Color.White);
        //    }

        //}

        private Texture2D getTextureByName(string name)
        {
            return MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath(name));
        }

        // Draws the game board
        private void DrawBoard()
        {
            float opacity;                                      //how opaque/transparent to draw the square
            Color colorToUse = Color.White;                     //background color to use
            Rectangle squareToDrawPosition = new Rectangle();   //the square to draw (local variable to avoid creating a new variable per square)

            Texture2D inventoryBackground = MyContentManager.Load<Texture2D>("Objects\\inventory\\eqMieszek");
            Texture2D eqBack = MyContentManager.Load<Texture2D>("Objects\\inventory\\eqMieszekBack");
            spriteBatch.Draw(eqBack, new Rectangle(805, 250, eqBack.Width,eqBack.Height), null, Color.White);
            //for all list
            for (int i = 0; i < Inventory.Count; i++)
            {
                //figure out where to draw the square
                squareToDrawPosition = new Rectangle((int)(Inventory[i].PositionInInventory.X * _tileSize + inventoryPosition.X), (int)(Inventory[i].PositionInInventory.Y * _tileSize + inventoryPosition.Y), _tileSize, _tileSize);

                if (Inventory[i].Item == null)
                    spriteBatch.Draw(getTextureByName("empty"), squareToDrawPosition, colorToUse);
                else
                    spriteBatch.Draw(getTextureByName(Inventory[i].Item?.name), squareToDrawPosition, colorToUse);

                //make the square the mouse is over red
                //if (IsMouseInsideBoard() && IsMouseOnTile(Inventory[i].PositionInInventory.X, Inventory[i].PositionInInventory.Y))
                //{
                //    colorToUse = Color.Red;
                //    opacity = .5f;
                //}
                //else
                //{
                //    colorToUse = Color.White;
                //}
                spriteBatch.Draw(inventoryBackground, new Rectangle(698, 0, inventoryBackground.Width, inventoryBackground.Height), null, Color.White);
            }
        }
    }
}