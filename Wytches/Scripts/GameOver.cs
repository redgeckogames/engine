﻿using System;
using System.Runtime.Serialization;

namespace Wytches.Scripts
{
    [Serializable]
    internal class GameOver : Exception
    {
        public GameOver()
        {
        }

        public GameOver(string message) : base(message)
        {
        }

        public GameOver(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected GameOver(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}