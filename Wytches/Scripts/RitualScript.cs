﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.GameOwn;
using Wytches.ReptileEngine;
using Wytches.ReptileEngine.AnimationComponents;
using Wytches.ReptileEngine.Lights;

namespace Wytches.Scripts
{
    public class RitualScript : Script
    {
        private DemonSummoning DemonSummoning { get; }
        private IButtonState ibuttonState = IButtonState.None;
        public int MaxItemCount { get; set; }
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private List<ItemSlot> ritualItemSlots;

        public List<ItemSlot> RitualItemSlots
        {
            get
            {
                return this.ritualItemSlots;
            }
        }

        public event EventHandler alterOpened;

        //  public event EventHandler<ItemEventArgs> brewPotion;

        public RitualScript(GameObject gameObject) : base(gameObject)
        {
            MaxItemCount = 3;
            DemonSummoning = new DemonSummoning(new DemonFactory(new DemonNameGenerator()));

            ritualItemSlots = new List<ItemSlot>();
            InitializeEmptySlots();
        }

        private void InitializeEmptySlots()
        {
            for (int i = 0; i < MaxItemCount; i++)
            {
                ItemSlot emptySlot = new ItemSlot(i, 0);
                ritualItemSlots.Add(emptySlot);
            }
        }

        public void SwapItemPosition(ItemSlot itemSlot1, ItemSlot itemSlot2)
        {
            ItemSlot tmp = new ItemSlot();
            tmp.Item = new Item(itemSlot1.Item);
            tmp.ItemTooltip = itemSlot1.ItemTooltip.ToString();

            if (itemSlot2.Item != null)
            {
                itemSlot1.Item = new Item(itemSlot2.Item);
                itemSlot1.ItemTooltip = itemSlot2.ItemTooltip.ToString();
            }
            else
            {
                itemSlot1.Item = null;
                itemSlot1.ItemTooltip = "";
            }
            itemSlot2.Item = new Item(tmp.Item);
            itemSlot2.ItemTooltip = tmp.ItemTooltip.ToString();
        }

        public void SwapItemInventory(ItemSlot itemSlot1, ItemSlot itemSlot2)
        {
            if (itemSlot2.Item != null)
            {
                itemSlot1.Item = new Item(itemSlot2.Item);
                itemSlot1.ItemTooltip = itemSlot2.ItemTooltip.ToString();
            }
            else
            {
                itemSlot1.Item = null;
                itemSlot1.ItemTooltip = "";
            }
        }

        public void makeRitual()
        {
            logger.Info("Starting Summoning Ritual");
            if (this.ritualItemSlots.Any(x => x.Item == null))
            {
                throw new NoOfferingsException("Not enough Offerings was Provided, you need to add " + ritualItemSlots.Count(slot => slot.Item == null) + " more");
            }

            GameObject witch = Game1.scene.FindGameObjectWithtag("wiedzma");
            Demon demon = new Demon();
            if (witch.GetComponent<WitchScript>().Attributes.currentSpellPower < CalculateRitualCost())
            {
                throw new NotEnoughSpellPowerException("You don't have enough Spell Power to perform this ritual needed " + CalculateRitualCost());
            }
            else
            {
                ritualItemSlots.ForEach(slot => DemonSummoning.AddOffering(slot?.Item));
                witch.GetComponent<WitchScript>().Attributes.currentSpellPower -= CalculateRitualCost();

                demon = DemonSummoning.Perform() as Demon;
                ClearSlots();
                InitializeEmptySlots();
            }

            GameObject demonObject = new GameObject(Game1.alter.GetComponent<Transform>().Position + new Vector3(10, 0, 10));
            demonObject.Tag = "Demon";
            demonObject.AddComponent<Animation>();
            AnimationState.LoadAnimationForGameObject(demonObject, AnimationStateName.Idle);
            demonObject.AddComponent<BoxCollider>();
            demonObject.AddComponent<PathComponent>();
            demonObject.AddComponent<DemonScript>().Demon = demon;

            Game1.demonList.Add(demonObject);
            Game1.scene.AddGameObject(demonObject);
        }

        private int CalculateRitualCost()
        {
            return (RitualItemSlots
                .Select(slots => slots.Item)
                .Select(item => item.usageEffect)
                .Select(effect => effect.GetUssageEffect())
                .Sum(value => value.currentSpellPower)) / ritualItemSlots.Count;
        }

        private void ClearSlots()
        {
            ritualItemSlots.Clear();
        }

        protected void OnAlterOpened()
        {
            if (alterOpened != null)
                alterOpened(this, EventArgs.Empty);
        }

        protected override void OncollisionExit(object sender, GameObjectEventArgs e)
        {
            if (!e.gameObject.Tag.Equals("Alter") && Game1.boolAlter == true)
            {
                OnAlterOpened();
                Game1.boolAlter = !(Game1.boolAlter);
                Game1.shouldDrawInventory = !(Game1.shouldDrawInventory);
                Game1.boolInventory = !(Game1.boolInventory);
                ibuttonState = IButtonState.None;
            }
        }

        protected override void OnCollision(object sender, GameObjectEventArgs e)
        {
            if (Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.E) && ibuttonState == IButtonState.None)
            {
                if (e.gameObject.Tag.Equals("Alter"))
                {
                    OnAlterOpened();
                    Game1.boolAlter = !(Game1.boolAlter);
                    Game1.shouldDrawInventory = !(Game1.shouldDrawInventory);
                    Game1.boolInventory = !(Game1.boolInventory);
                    ibuttonState = IButtonState.IsBeingPressd;
                }
            }
            if (Input.GetKeyReleased(Microsoft.Xna.Framework.Input.Keys.E) && ibuttonState == IButtonState.IsBeingPressd)
            {
                ibuttonState = IButtonState.None;
            }
        }
    }
}