﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.ReptileEngine;

namespace Wytches.Scripts
{
    public class PathDestinationFinder
    {
        private List<GameObject> targetList;
        private MultiValueDictionary<DestinationTarget, GameObject> possibleTargets;
        private static PathDestinationFinder instance;
        private static volatile object SyncLock = new object();

        public static PathDestinationFinder Instance
        {
            get
            {
                lock (SyncLock)
                {
                    if (instance == null)
                    {
                        instance = new PathDestinationFinder();
                    }
                }
                return instance;
            }
        }

        private PathDestinationFinder()
        {
            possibleTargets = new MultiValueDictionary<DestinationTarget, GameObject>();
        }

        public void AddDestinationTargetRange(DestinationTarget targetType, IEnumerable<GameObject> targets)
        {
            possibleTargets.AddRange(targetType, targets);
        }

        public void AddDestinationTarget(DestinationTarget targetType, GameObject gameObject)
        {
            possibleTargets.Add(targetType, gameObject);
        }

        public GameObject GetRandomTarget(DestinationTarget targetType)
        {
            IReadOnlyCollection<GameObject> targets = null;
            if (possibleTargets.TryGetValue(targetType, out targets))
            {
                return targets.ElementAt(new Random().Next(0, targets.Count - 1)); ;
            }
            else
            {
                throw new TargetNotFoundException("Destination Target of Type" + targetType.ToString() + " was not Found");
            }
        }

        public void RemoveTarger(DestinationTarget key, GameObject value)
        {
            possibleTargets.Remove(key, value);
        }

        public PathDestinationFinder(List<GameObject> targetList)
        {
            this.targetList = targetList;
        }

        public void AddTarget(GameObject gameObject)
        {
            targetList.Add(gameObject);
        }

        public GameObject randomTarget()
        {
            return targetList.ElementAt(new Random().Next(0, targetList.Count - 1));
        }
    }
}