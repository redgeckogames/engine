﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Wytches.GameOwn;
using Wytches.ReptileEngine;

namespace Wytches.Scripts
{
    internal class DemonScript : Script
    {
        public Demon Demon { get; set; }
        private PathComponent pathComponent;
        public PathDestinationFinder PathFinder { get; set; }

        private Logger logger = LogManager.GetCurrentClassLogger();

        private Timer attackTimer;
        private Timer timeToLive;

        private GameObject attackTarget;

        private static volatile object SyncLock = new object();

        public DemonScript(GameObject gameObject)
            : base(gameObject)
        {
            timeToLive = new Timer(1000);
            this.timeToLive.Elapsed += OnLifeTimer;
            this.timeToLive.AutoReset = true;
            this.timeToLive.Start();
            SeekAnDDestroy();
        }

        public void SeekAnDDestroy()
        {
            logger.Info("Searching for Target");

            this.pathComponent = this.gameObject.GetComponent<PathComponent>();

            try
            {
                attackTarget = PathDestinationFinder.Instance.GetRandomTarget(DestinationTarget.House);

                logger.Info("Attack target Found : " + attackTarget?.Transform?.ToString());
                attackTarget.GetComponent<StructureScript>().Structure.StructureDestroyed += OnStructoreDestroyed;

                pathComponent.SetDestination(attackTarget);

                this.pathComponent.DestinationReached += OnDestinationReached;
                this.pathComponent.FindPath();
                this.pathComponent.StartGoPath();

                AnimationState.LoadAnimationForGameObject(this.GameObject, AnimationStateName.Walking);
            }
            catch (TargetNotFoundException ex)
            {
                logger.Info("no more targets time to die " + Demon?.Name);
                this.gameObject.Dispose();

                throw new Victory("Congratulations You have Prevailed !!!!");
            }
        }

        private void OnDestinationReached(object sender, EventArgs e)
        {
            logger.Info("Demon Destination Was reached");
            if (sender is PathComponent)
            {
                PathComponent component = sender as PathComponent;

                component.DestinationReached -= OnDestinationReached;

                attackTimer = new Timer(3000);
                attackTimer.Elapsed += OnAttack;
                attackTimer.AutoReset = true;
                attackTimer.Start();
            }
        }

        private void OnAttack(object sender, ElapsedEventArgs e)
        {
            if (attackTarget != null)
            {
                AnimationState.LoadAnimationForGameObject(this.GameObject, AnimationStateName.Attacking);
                try
                {
                    logger.Info("Attacking" + attackTarget.Tag);
                    attackTarget.GetComponent<StructureScript>().InteractWithStructure("HitMe", 5);
                }
                catch (ComponentNotFoundException ex)
                {
                    logger.Error(ex.Message);
                    attackTimer.Stop();
                    SeekAnDDestroy();
                }
            }
        }

        private void OnLifeTimer(object sender, ElapsedEventArgs e)
        {
            if (this.Demon != null)
            {
                this.Demon.Attributes.currentSpellPower--;

                if (this.Demon.Attributes.currentSpellPower <= 0)
                {
                    logger.Info("Time to Die " + this.Demon.Name);
                    Dispose(true);
                    this.gameObject.Dispose();
                }
            }
        }

        private void OnStructoreDestroyed(object sender, EventArgs e)
        {
            logger.Info("Target Was Destroyed");
            attackTimer?.Stop();
            if (attackTarget.ContainsComponent<StructureScript>())
            {
                attackTarget.GetComponent<StructureScript>().Structure.StructureDestroyed -= OnStructoreDestroyed;
                logger.Info("disposing structure");
                PathDestinationFinder.Instance.RemoveTarger(DestinationTarget.House, attackTarget);

                attackTarget.Dispose();
            }
            attackTarget = null;
            SeekAnDDestroy();
        }

        protected override void Dispose(bool disposing)
        {
            attackTimer?.Dispose();
            timeToLive?.Dispose();
            this.gameObject.GetComponent<PathComponent>().Dispose();
            base.Dispose(disposing);
        }
    }
}