﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.GameOwn;
using Wytches.ReptileEngine;

namespace Wytches.Scripts
{
    public class HarverstingScript : Script
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public HarverstingScript(GameObject gameObject) : base(gameObject)
        {
        }

        protected override void OnCollision(object sender, GameObjectEventArgs e)
        {
            if (Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.E))
            {
                if (e.gameObject.ContainsComponent<FarmScript>())
                {
                    try
                    {
                        HarvestItem(e.gameObject.GetComponent<FarmScript>().HarvestPlant() as Item);
                    }
                    catch (PlantNotFulluGrownException exception)
                    {
                        logger.Error(exception.Message);
                    }
                }
            }
        }

        private void HarvestItem(Item item)
        {
            try
            {
                this.GameObject.GetComponent<InventoryScript>().AddItem(item);
            }
            catch (InventoryFullException e)
            {
                logger.Error(e.Message);
            }
        }
    }
}