﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.ReptileEngine;

namespace Wytches.Scripts
{
    public class PlaneScript : Script
    {
        private VertexPositionTexture[] floorVerts;
        private BasicEffect effect;

        public PlaneScript(GameObject gameObject) : base(gameObject)
        {
            //dodanie plane'a
            floorVerts = new VertexPositionTexture[6];
            floorVerts[0].Position = new Vector3(-20, -20, 0);
            floorVerts[1].Position = new Vector3(-20, 20, 0);
            floorVerts[2].Position = new Vector3(20, -20, 0);
            floorVerts[3].Position = floorVerts[1].Position;
            floorVerts[4].Position = new Vector3(20, 20, 0);
            floorVerts[5].Position = floorVerts[2].Position;

            // effect = new BasicEffect(Game1. graphicsDevice.GraphicsDevice);
        }

        //public override void Draw(Camera camera)
        //{
        //    foreach (var pass in effect.CurrentTechnique.Passes)
        //    {
        //        pass.Apply();

        //        graphicsDevice.GraphicsDevice.DrawUserPrimitives(
        //            // We’ll be rendering two trinalges
        //            PrimitiveType.TriangleList,
        //            // The array of verts that we want to render
        //            floorVerts,
        //            // The offset, which is 0 since we want to start
        //            // at the beginning of the floorVerts array
        //            0,
        //            // The number of triangles to draw
        //            2);
        //    }

        //    base.Draw(camera);
        //}
    }
}