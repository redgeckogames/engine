﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.ReptileEngine;
using Wytches.Wytches_Game;

namespace Wytches.Scripts
{
    public class AlterDraw : DrawableGameComponent
    {

        private List<ItemSlot> alterItemSlots;
        private int MaxItemCount;
        private bool AlterOpened { get; set; } = false;

        protected SpriteBatch spriteBatch;
        private Texture2D altarTexture2D;

        private Camera camera;
        private BasicEffect basicEffect;
        private const int _tileSize = 62;
        private Vector2 alterListPosition;
        private Button summonButton;

        public event EventHandler altarOpenedEvent;

        public Vector2 AlterListPosition
        {
            get
            {
                return this.alterListPosition;
            }
        }

        private Vector2 alterTextPosition;

        public Vector2 AlterTextPosition
        {
            get
            {
                return this.alterTextPosition;
            }
        }

        private RitualScript alterScript;
        private SpriteFont _defaultFont;

        public AlterDraw(Game game, Camera camera, RitualScript alterScript) : base(game)
        {
            alterListPosition = new Vector2(329, 441);
            alterTextPosition = new Vector2(300, 400);
            _defaultFont = MyContentManager.Load<SpriteFont>("DefaultFont");
            altarTexture2D = MyContentManager.Load<Texture2D>("Objects\\altarsprite");
            this.alterScript = alterScript;

           this.alterScript.alterOpened += OnAlterOpened;

            this.alterItemSlots = alterScript.RitualItemSlots;
            this.MaxItemCount = alterScript.MaxItemCount;
            this.camera = camera;
            basicEffect = new BasicEffect(GraphicsDevice);
            spriteBatch = MySpriteBatch.returnSpriteBatch();
            summonButton = new Button(MyContentManager.Load<Texture2D>("Objects\\buttons\\ButtonDemon"), (alterTextPosition+ new Vector2(70, 0) ), game, camera);
        }

        private void OnAlterOpened(object sender, EventArgs e)
        {
            this.AlterOpened = !AlterOpened;
            //altarOpenedEvent(this, EventArgs.Empty);
        }

        public override void Draw(GameTime gameTime)
        {
            //wyłącznie Z buffora i rysowanie spirtów na rzeczach z 3d

            if (AlterOpened)
            {
                Game.IsMouseVisible = true;

                GraphicsDevice.BlendState = BlendState.AlphaBlend;
                GraphicsDevice.DepthStencilState = DepthStencilState.None;
                GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

                basicEffect.World = Matrix.CreateScale(1, -1, 1);
                basicEffect.View = camera.view;
                basicEffect.Projection = camera.projection;
                //start drawing

                spriteBatch.Begin();

                DrawBoard();
               // DrawText();

                //end drawing
                spriteBatch.End();

                summonButton.Draw();
            }

            base.Draw(gameTime);
        }

        private void DrawBoard()
        {
            Rectangle squareToDrawPosition = new Rectangle();   //the square to draw (local variable to avoid creating a new variable per square)
            Color colorToUse = Color.White;
            //for all columns
            Rectangle altarSpriteRectangle = new Rectangle(100, 100, altarTexture2D.Width, altarTexture2D.Height);
            spriteBatch.Draw(altarTexture2D, altarSpriteRectangle, Color.White);
            for (int i = 0; i < alterItemSlots.Count; i++)
            {
                //   squareToDrawPosition = new Rectangle((int)(1.5f * x * _tileSize + 100), (int)(_tileSize + 50), _tileSize, _tileSize);
                squareToDrawPosition = new Rectangle((int)(alterItemSlots[i].PositionInInventory.X * _tileSize + alterListPosition.X), (int)(alterItemSlots[i].PositionInInventory.Y * _tileSize + alterListPosition.Y), _tileSize, _tileSize);

                if (alterItemSlots[i].Item == null)
                    spriteBatch.Draw(getTextureByName("empty"), squareToDrawPosition, colorToUse);
                else
                    spriteBatch.Draw(getTextureByName(alterItemSlots[i].Item?.name), squareToDrawPosition, colorToUse);
            }
        }


        private void DrawText()
        {
            spriteBatch.DrawString(_defaultFont, "Wezwij Demona ", new Vector2(alterTextPosition.X, alterTextPosition.Y), Color.White);
        }

        private Texture2D getTextureByName(string name)
        {
            return MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath(name));
        }

        public void Update()
        {
            summonButton.Update();
        }
    }
}
