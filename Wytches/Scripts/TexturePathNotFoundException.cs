﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.Scripts
{
    public class TexturePathNotFoundException : Exception
    {
        public TexturePathNotFoundException(string message) : base(message)
        {
        }
    }
}