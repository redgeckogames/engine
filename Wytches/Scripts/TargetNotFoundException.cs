﻿using System;
using System.Runtime.Serialization;

namespace Wytches.Scripts
{
    [Serializable]
    internal class TargetNotFoundException : Exception
    {
        public TargetNotFoundException()
        {
        }

        public TargetNotFoundException(string message) : base(message)
        {
        }

        public TargetNotFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected TargetNotFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}