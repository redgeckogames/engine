﻿using System;
using System.Runtime.Serialization;

namespace Wytches.Scripts
{
    [Serializable]
    public class ItemTextureNameException : Exception
    {
        public ItemTextureNameException()
        {
        }

        public ItemTextureNameException(string message) : base(message)
        {
        }

        public ItemTextureNameException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ItemTextureNameException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}