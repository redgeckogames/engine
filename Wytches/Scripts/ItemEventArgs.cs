﻿using System;
using Wytches.GameOwn;

namespace Wytches.Scripts
{
    public class ItemEventArgs : EventArgs
    {
        public Item Item { get; set; }
    }
}