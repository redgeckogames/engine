﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.Scripts
{
    internal class ModelPlantDB
    {
        private static Dictionary<string, string> model3d;

        public ModelPlantDB(string textureDbFilePath)
        {
            model3d = new Dictionary<string, string>();
            LoadModelFromFile(textureDbFilePath);
        }

        public static string GetModel3dPath(string plantName)
        {
            string modelPath;
            var trimmedItemName = plantName.Replace(" ", "");

            if (model3d.TryGetValue(trimmedItemName, out modelPath) == false)
            {
                throw new TexturePathNotFoundException("Texture for the item "
                    + plantName + " was not found make sure you have entered correct name");
            }
            else
            {
                return modelPath;
            }
        }

        public static void AddModel(string plantName, string modelPath)
        {
            if (model3d.ContainsKey(plantName) == false)
            {
                model3d.Add(plantName, modelPath);
            }
            else
            {
                throw new ItemTextureNameException("Item called " + plantName + " already has a texture mapped to it");
            }
        }

        private void LoadModelFromFile(string filePath)
        {
            using (var mappedFile1 = MemoryMappedFile.CreateFromFile(filePath))
            {
                using (Stream mmStream = mappedFile1.CreateViewStream())
                {
                    using (StreamReader sr = new StreamReader(mmStream, ASCIIEncoding.UTF8))
                    {
                        while (!sr.EndOfStream)
                        {
                            var line = sr.ReadLine();
                            var lineWords = line.Split(' ');
                            AddModel(lineWords[0].Trim(), lineWords[1].Trim());
                        }
                    }
                }
            }
        }
    }
}