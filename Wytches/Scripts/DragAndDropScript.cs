﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.GameOwn;
using Wytches.Scripts;

namespace Wytches.Scripts
{
    public class DragAndDropScript
    {
        private Vector2 _currentMousePosition;          //the current position of the mouse
        private Vector2 _mouseDownPosition;             //where the mouse was clicked down
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        //stores the previous and current states of the mouse
        //makes it possible to know if a button was just clicked
        //or whether it was up/down previously as well.
        private MouseState _oldMouse, _currentMouse;

        private bool _isDragging = false;

        private Vector2 tileCopy;

        private InventoryScript inventoryScript;
        private CauldronScript cauldronScript;
        private RitualScript ritualScript;
        private Vector2 inventoryPosition, cauldronPosition, cauldronTextPosition, ritualPosition, ritualTextPosition;
        private int _tileSizeInventory = 70;
        private int _tileSizeCauldron = 100;
        private int _titleSizeAlter = 62;
        private bool boolInventory, boolCauldron, boolAlter;
        private List<ItemSlot> Inventory;
        private List<ItemSlot> cauldronItemSlots;
        private List<ItemSlot> alterItemSlots;

        private Texture2D _xnafan;

        public DragAndDropScript(InventoryScript inventoryScript, CauldronScript cauldronScript, RitualScript ritualScript, Vector2 inventoryPosition, Vector2 cauldronPosition,
            Vector2 cauldronTextPosition, Vector2 ritualPosition, Vector2 ritualTextPosition)
        {
            this.inventoryScript = inventoryScript;
            this.cauldronScript = cauldronScript;
            this.ritualScript = ritualScript;

            this.inventoryPosition = inventoryPosition;
            this.cauldronPosition = cauldronPosition;

            this.Inventory = inventoryScript.Inventory;
            this.cauldronItemSlots = cauldronScript.CauldronItemSlots;
            this.cauldronTextPosition = cauldronTextPosition;

            this.ritualPosition = ritualPosition;
            this.ritualTextPosition = ritualTextPosition;
            this.alterItemSlots = ritualScript.RitualItemSlots;

            boolInventory = false;
            boolCauldron = false;
            boolAlter = false;
        }


        public void Draw()
        {
            if (_isDragging)
            {
                Game1.spriteBatch.Begin();
                Game1.spriteBatch.Draw(_xnafan, new Rectangle((int)(_currentMousePosition.X - _tileSizeInventory / 2), (int)(_currentMousePosition.Y - _tileSizeInventory / 2), _tileSizeInventory, _tileSizeInventory), Color.White);
                Game1.spriteBatch.End();
            }

        }

        public void Update(bool drawInventory, bool drawCauldron, bool drawAlter)
        {
            //get the current state of the mouse (position, buttons, etc.)
            _currentMouse = Mouse.GetState();

            //remember the mouseposition for use in this Update and subsequent Draw
            _currentMousePosition = new Vector2(_currentMouse.X, _currentMouse.Y);

            try
            {
                CheckForLeftButtonDown(drawInventory, drawCauldron, drawAlter);
                CheckForLeftButtonRelease(drawInventory, drawCauldron, drawAlter);
                CheckForRightButtonReleaseOverBoard(drawInventory, drawCauldron);
            }
            catch (NotEnoughSpellPowerException e)
            {
                logger.Error(e.Message);
            }
            catch (NoOfferingsException e1)
            {
                logger.Error(e1.Message);
            }

            //store the current state of the mouse as the old
            _oldMouse = _currentMouse;
        }

        private bool isInventory()
        {
            if (_currentMousePosition.X >= inventoryPosition.X && _currentMousePosition.X <= inventoryPosition.X + 4 * _tileSizeInventory && _currentMousePosition.Y >= inventoryPosition.Y && _currentMousePosition.Y <= inventoryPosition.Y + 4 * _tileSizeInventory)
            {
                Debug.WriteLine("iN INVENTORY");
                return true;
            }
            else
            { return false; }
        }

        private bool isCauldron()
        {
            if (_currentMousePosition.X >= cauldronPosition.X && _currentMousePosition.X <= cauldronPosition.X + 3 * _tileSizeCauldron && _currentMousePosition.Y >= cauldronPosition.Y && _currentMousePosition.Y <= cauldronPosition.Y + 1 * _tileSizeCauldron)
            {
                Debug.WriteLine("iN CAULDROn");
                return true;
            }
            else
            { return false; }
        }

        private bool isAlter()
        {
            if (_currentMousePosition.X >= ritualPosition.X && _currentMousePosition.X <= ritualPosition.X + 3 * _titleSizeAlter && _currentMousePosition.Y >= ritualPosition.Y && _currentMousePosition.Y <= ritualPosition.Y + 1 * _titleSizeAlter)
            {
                Debug.WriteLine("iN alter");
                return true;
            }
            else
            { return false; }
        }

        private bool isCauldronText()
        {
            if (_currentMousePosition.X >= cauldronTextPosition.X - 150 && _currentMousePosition.X <= cauldronTextPosition.X + 150 && _currentMousePosition.Y >= cauldronTextPosition.Y - 150 && _currentMousePosition.Y <= cauldronTextPosition.Y + 150)
            {
                Debug.WriteLine("iN CAULDROn text");
                return true;
            }
            else
            { return false; }
        }

        private bool isAlterText()
        {
            if (_currentMousePosition.X >= ritualTextPosition.X - 150 && _currentMousePosition.X <= ritualTextPosition.X + 150 && _currentMousePosition.Y >= ritualTextPosition.Y - 150 && _currentMousePosition.Y <= ritualTextPosition.Y + 150)
            {
                Debug.WriteLine("iN alter text");
                return true;
            }
            else
            { return false; }
        }

        private void CheckForLeftButtonDown(bool drawInventory, bool drawCauldron, bool drawAlter)
        {
            if (_currentMouse.LeftButton == ButtonState.Pressed)
            {
                //if this Update() is a new click - store the mouse-down position
                if (_oldMouse.LeftButton == ButtonState.Released)
                {
                    _mouseDownPosition = _currentMousePosition;
                }
                if (drawInventory && isInventory() && !(_isDragging))
                {
                    tileCopy = GetSquareFromCurrentMousePositionInventory();
                    if (Inventory[(int)(tileCopy.X + 4 * tileCopy.Y)].Item != null)
                    {
                        boolInventory = true;
                        _isDragging = true;
                        _xnafan = ReptileEngine.MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath(Inventory[(int)(tileCopy.X + 4 * tileCopy.Y)].Item.name));
                    }
                }
                if (drawCauldron && isCauldron() && !(_isDragging))
                {
                    tileCopy = GetSquareFromCurrentMousePositionCauldron();
                    if (cauldronItemSlots[(int)(tileCopy.X + 1 * tileCopy.Y)].Item != null)
                    {
                        boolCauldron = true;
                        _isDragging = true;
                        _xnafan = ReptileEngine.MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath(cauldronItemSlots[(int)(tileCopy.X + 1 * tileCopy.Y)].Item.name));
                    }
                }
                if (drawCauldron && isCauldronText() && !(_isDragging))
                {
                    inventoryScript.AddItem(cauldronScript.BrewPotion());
                    _isDragging = true;
                }

                if (drawAlter && isAlter() && !(_isDragging))
                {
                    tileCopy = GetSquareFromCurrentMousePositionAlter();
                    if (alterItemSlots[(int)(tileCopy.X + 1 * tileCopy.Y)].Item != null)
                    {
                        boolAlter = true;
                        _isDragging = true;
                        _xnafan = ReptileEngine.MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath((alterItemSlots[(int)(tileCopy.X + 1 * tileCopy.Y)].Item.name)));
                    }
                }
                if (drawAlter && isAlterText() && !(_isDragging))
                {
                    ritualScript.makeRitual();

                    _isDragging = true;
                }
            }
        }

        private void CheckForLeftButtonRelease(bool drawInventory, bool drawCauldron, bool drawAlter)
        {
            //if the user just released the mousebutton - set _isDragging to false, and check if we should add the tile to the board
            if (_oldMouse.LeftButton == ButtonState.Pressed && _currentMouse.LeftButton == ButtonState.Released && _isDragging)
            {
                _isDragging = false;

                if (drawInventory && boolInventory && isInventory())
                {
                    //find out which square the mouse is over
                    Vector2 tile = GetSquareFromCurrentMousePositionInventory();

                    inventoryScript.SwapItemPosition(Inventory[(int)(tileCopy.X + 4 * tileCopy.Y)], Inventory[(int)(tile.X + 4 * tile.Y)]);
                    boolInventory = false;
                }

                if (drawCauldron && boolCauldron && isCauldron())
                {
                    //find out which square the mouse is over
                    Vector2 tile = GetSquareFromCurrentMousePositionCauldron();

                    cauldronScript.SwapItemPosition(cauldronItemSlots[(int)(tileCopy.X + 0 * tileCopy.Y)], cauldronItemSlots[(int)(tile.X + 0 * tile.Y)]);
                    boolCauldron = false;
                }

                if (drawCauldron && drawInventory && boolInventory && isCauldron()) //mamy tileCOpy czyli pozycję czegoś z inventory i przenosimy do kociłka
                {
                    //find out which square the mouse is over
                    Vector2 tile = GetSquareFromCurrentMousePositionCauldron();
                    ItemSlot tmp = new ItemSlot();
                    tmp.Item = Inventory[(int)(tileCopy.X + 4 * tileCopy.Y)].Item;
                    tmp.ItemTooltip = Inventory[(int)(tileCopy.X + 4 * tileCopy.Y)].ItemTooltip;

                    inventoryScript.SwapItemCauldron(Inventory[(int)(tileCopy.X + 4 * tileCopy.Y)], cauldronItemSlots[(int)(tile.X + 0 * tile.Y)]);
                    cauldronScript.SwapItemInventory(cauldronItemSlots[(int)(tile.X + 0 * tile.Y)], tmp);

                    boolInventory = false;
                }

                if (drawInventory && drawCauldron && boolCauldron && isInventory())
                {
                    //find out which square the mouse is over
                    Vector2 tile = GetSquareFromCurrentMousePositionInventory();
                    ItemSlot tmp = new ItemSlot();
                    tmp.Item = cauldronItemSlots[(int)(tileCopy.X + 0 * tileCopy.Y)].Item;
                    tmp.ItemTooltip = cauldronItemSlots[(int)(tileCopy.X + 0 * tileCopy.Y)].ItemTooltip;

                    cauldronScript.SwapItemInventory(cauldronItemSlots[(int)(tileCopy.X + 0 * tileCopy.Y)], Inventory[(int)(tile.X + 4 * tile.Y)]);
                    inventoryScript.SwapItemCauldron(Inventory[(int)(tile.X + 4 * tile.Y)], tmp);

                    boolCauldron = false;
                }

                if (drawAlter && boolAlter && isAlter())
                {
                    //find out which square the mouse is over
                    Vector2 tile = GetSquareFromCurrentMousePositionAlter();

                    ritualScript.SwapItemPosition(alterItemSlots[(int)(tileCopy.X + 0 * tileCopy.Y)], alterItemSlots[(int)(tile.X + 0 * tile.Y)]);
                    boolAlter = false;
                }
                if (drawAlter && drawInventory && boolInventory && isAlter()) //mamy tileCOpy czyli pozycję czegoś z inventory i przenosimy do kociłka
                {
                    //find out which square the mouse is over
                    Vector2 tile = GetSquareFromCurrentMousePositionAlter();
                    ItemSlot tmp = new ItemSlot();
                    tmp.Item = Inventory[(int)(tileCopy.X + 4 * tileCopy.Y)].Item;
                    tmp.ItemTooltip = Inventory[(int)(tileCopy.X + 4 * tileCopy.Y)].ItemTooltip;

                    inventoryScript.SwapItemCauldron(Inventory[(int)(tileCopy.X + 4 * tileCopy.Y)], alterItemSlots[(int)(tile.X + 0 * tile.Y)]);
                    ritualScript.SwapItemInventory(alterItemSlots[(int)(tile.X + 0 * tile.Y)], tmp);

                    boolInventory = false;
                }
                if (drawInventory && drawAlter && boolAlter && isInventory())
                {
                    //find out which square the mouse is over
                    Vector2 tile = GetSquareFromCurrentMousePositionInventory();
                    ItemSlot tmp = new ItemSlot();
                    tmp.Item = alterItemSlots[(int)(tileCopy.X + 0 * tileCopy.Y)].Item;
                    tmp.ItemTooltip = alterItemSlots[(int)(tileCopy.X + 0 * tileCopy.Y)].ItemTooltip;

                    ritualScript.SwapItemInventory(alterItemSlots[(int)(tileCopy.X + 0 * tileCopy.Y)], Inventory[(int)(tile.X + 4 * tile.Y)]);
                    inventoryScript.SwapItemCauldron(Inventory[(int)(tile.X + 4 * tile.Y)], tmp);

                    boolAlter = false;
                }
            }
        }

        private void CheckForRightButtonReleaseOverBoard(bool drawInventory, bool drawCauldron)
        {
            //find out if right button was just clicked over the board - and remove a tile from that square
            if (_oldMouse.RightButton == ButtonState.Released && _currentMouse.RightButton == ButtonState.Pressed && isInventory())
            {
                Vector2 boardSquare = GetSquareFromCurrentMousePositionInventory();
                if (Inventory[(int)(boardSquare.X + 4 * boardSquare.Y)].Item != null)
                {
                    inventoryScript.UseSelectedItem(Inventory[(int)(boardSquare.X + 4 * boardSquare.Y)]);
                }
            }
            //find out if right button was just clicked over the board - and remove a tile from that square
            //if (_oldMouse.RightButton == ButtonState.Released && _currentMouse.RightButton == ButtonState.Pressed && isCauldron())
            //{
            //    Vector2 boardSquare = GetSquareFromCurrentMousePositionCauldron();
            //    // _board[(int)boardSquare.X, (int)boardSquare.Y] = false;
            //    Debug.WriteLine("pozycja plytyki" + boardSquare.ToString());
            //}
        }

        //// Checks to see whether a given coordinate is within the board
        //private bool IsMouseOnTile(int x, int y)
        //{
        //    //do an integerdivision (whole-number) of the coordinates relative to the board offset with the tilesize in mind
        //    return (int)(_currentMousePosition.X - inventoryPosition.X) / _tileSize == x && (int)(_currentMousePosition.Y - inventoryPosition.Y) / _tileSize == y;
        //}

        ////get the column/row on the board for a given coordinate
        private Vector2 GetSquareFromCurrentMousePositionInventory()
        {
            //adjust for the boards offset (_boardPosition) and do an integerdivision
            return new Vector2((int)(_currentMousePosition.X - inventoryPosition.X) / _tileSizeInventory, (int)(_currentMousePosition.Y - inventoryPosition.Y) / _tileSizeInventory);
        }

        private Vector2 GetSquareFromCurrentMousePositionCauldron()
        {
            //adjust for the boards offset (_boardPosition) and do an integerdivision
            return new Vector2((int)(_currentMousePosition.X - cauldronPosition.X) / _tileSizeCauldron, (int)(_currentMousePosition.Y - cauldronPosition.Y) / _tileSizeCauldron);
        }

        private Vector2 GetSquareFromCurrentMousePositionAlter()
        {
            //adjust for the boards offset (_boardPosition) and do an integerdivision
            return new Vector2((int)(_currentMousePosition.X - ritualPosition.X) / _titleSizeAlter, (int)(_currentMousePosition.Y - ritualPosition.Y) / _titleSizeAlter);
        }
    }
}