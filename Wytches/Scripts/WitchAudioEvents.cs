﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.GameOwn;
using Wytches.Scripts;
using Wytches.ReptileEngine;

namespace Wytches.Scripts
{
    public class WitchAudioEvents : Script
    {
        private InventoryScript inventoryScript;

        public WitchAudioEvents(GameObject gameobject) : base(gameobject)
        {
            inventoryScript = gameobject.GetComponent<InventoryScript>();
            inventoryScript.ItemConsumed += OnItemConsumed;
        }

        private void OnItemConsumed(object sender, ItemEventArgs e)
        {
            if (e.Item.name == "Health Rejuvenation Potion")
            {
                Game1.audioManager.PlaySoundEffect(0);
                Game1.tex = 2;
                Game1.createParticleEngine = true;
                Game1.particleStarted = true;
            }
            else if (e.Item.name == "Minor Spell Power Potion")
            {
                Game1.audioManager.PlaySoundEffect(0);
                Game1.tex = 1;
                Game1.createParticleEngine = true;
                Game1.particleStarted = true;
            }
        }
    }
}