﻿using System;
using System.Runtime.Serialization;

namespace Wytches.Scripts
{
    [Serializable]
    internal class NotEnoughSpellPowerException : Exception
    {
        public NotEnoughSpellPowerException()
        {
        }

        public NotEnoughSpellPowerException(string message) : base(message)
        {
        }

        public NotEnoughSpellPowerException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NotEnoughSpellPowerException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}