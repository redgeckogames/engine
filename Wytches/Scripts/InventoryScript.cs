﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.ReptileEngine;
using Wytches.GameOwn;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;
using System.Runtime.Serialization;
using System.Diagnostics;
using Wytches.Scripts;
using NLog;

namespace Wytches.Scripts
{
    [DataContract]
    public class InventoryScript : Script
    {
        [DataMember]
        private List<ItemSlot> inventory;

        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public List<ItemSlot> Inventory
        {
            get
            {
                return this.inventory;
            }
        }

        private readonly int MaxItemCount;

        public event EventHandler InventoryChanged;

        public event EventHandler<ItemEventArgs> ItemConsumed;

        public InventoryScript(GameObject gameObject) : base(gameObject)
        {
            MaxItemCount = 16;

            inventory = new List<ItemSlot>();

            InitializeEmptySlots();
        }

        private void InitializeEmptySlots()
        {
            int row = 0;
            int column = 0;
            for (int i = 0; i < MaxItemCount; i++)
            {
                ItemSlot emptySlot = new ItemSlot(column, row);
                inventory.Add(emptySlot);
                column++;
                if (column == MaxItemCount / 4)
                {
                    row++;
                    column = 0;
                }
            }
        }

        internal void RemoveItem(ItemSlot itemSlot)
        {
            itemSlot.Item = null;
            itemSlot.ItemTooltip = null;
        }

        protected void OnInventoryChanged()
        {
            if (InventoryChanged != null)
                InventoryChanged(this, EventArgs.Empty);
        }

        public void AddItem(Item item)
        {
            if (IsInventoryFull() == true)
            {
                throw new InventoryFullException("Inventory Is Full");
            }
            else
            {
                ItemSlot slot = GetFirstFreeItemSlot();
                slot.Item = item;
                slot.ItemTooltip = item?.name;
                OnInventoryChanged();
            }
        }

        public void SwapItemPosition(ItemSlot itemSlot1, ItemSlot itemSlot2)
        {
            ItemSlot tmp = new ItemSlot();
            tmp.Item = new Item(itemSlot1.Item);
            tmp.ItemTooltip = itemSlot1.ItemTooltip.ToString();

            if (itemSlot2.Item != null)
            {
                itemSlot1.Item = new Item(itemSlot2.Item);
                itemSlot1.ItemTooltip = itemSlot2.ItemTooltip.ToString();

                // itemSlot2.ItemTooltip = tmp.ItemTooltip;
            }
            else
            {
                itemSlot1.Item = null;
                itemSlot1.ItemTooltip = "";
            }
            itemSlot2.Item = new Item(tmp.Item);
            itemSlot2.ItemTooltip = tmp.ItemTooltip.ToString();
        }

        public void SwapItemCauldron(ItemSlot itemSlot1, ItemSlot itemSlot2) //inventory z kociołkiem
        {
            if (itemSlot2.Item != null)
            {
                itemSlot1.Item = new Item(itemSlot2.Item);
                itemSlot1.ItemTooltip = itemSlot2.ItemTooltip.ToString();
            }
            else
            {
                itemSlot1.Item = null;
                itemSlot1.ItemTooltip = "";
            }
        }

        private bool IsInventoryFull()
        {
            return inventory.Where(slot => slot?.Item != null).ToList().Count == MaxItemCount;
        }

        private ItemSlot GetFirstFreeItemSlot()
        {
            return inventory.Where(slot => slot?.Item == null).First();
        }

        public void UseSelectedItem(ItemSlot itemSlot)
        {
            logger.Info("wypilem" + itemSlot.Item.name);
            if (itemSlot.Item is Consumable)
            {
                OnItemConsumed(itemSlot.Item);
            }

            RemoveItem(itemSlot);
        }

        protected void OnItemConsumed(Item item)
        {
            if (ItemConsumed != null)
            {
                ItemConsumed(this, new ItemEventArgs { Item = item });
            }
        }

        public override void Update(GameTime dataTime)
        {
            base.Update(dataTime);
        }
    }

    public class ItemSlot
    {
        public Item Item { get; set; }
        public GridPosition PositionInInventory { get; set; }
        public string ItemTooltip { get; set; }

        public ItemSlot()
        {
        }

        public ItemSlot(int column, int row)
        {
            this.PositionInInventory = new GridPosition()
            {
                X = column,
                Y = row
            };
        }
    }

    public class GridPosition
    {
        public int X { get; set; }
        public int Y { get; set; }
    }
}