﻿using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Wytches.GameOwn;
using Wytches.ReptileEngine;

namespace Wytches.Scripts
{
    public class CauldronScript : Script
    {
        private Cauldron Cauldron { get; }

        public int MaxItemCount { get; set; }
        private IButtonState ibuttonState = IButtonState.None;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private List<ItemSlot> cauldronItemSlots;

        public List<ItemSlot> CauldronItemSlots
        {
            get
            {
                return this.cauldronItemSlots;
            }
        }

        public event EventHandler cauldronOpened;

        public event EventHandler healthPotionMade;
        public event EventHandler manaPotionMade;

        public event EventHandler<ItemEventArgs> brewPotion;

        public CauldronScript(GameObject gameObject) : base(gameObject)
        {
            MaxItemCount = 3;
            Cauldron = new SmallCauldron(new StandardPotionFactory(new PotionRecipeList()));
            cauldronItemSlots = new List<ItemSlot>();
            InitializeEmptySlots();
        }

        private void InitializeEmptySlots()
        {
            for (int i = 0; i < MaxItemCount; i++)
            {
                ItemSlot emptySlot = new ItemSlot(i, 0);
                cauldronItemSlots.Add(emptySlot);
            }
        }

        public void SwapItemPosition(ItemSlot itemSlot1, ItemSlot itemSlot2)
        {
            ItemSlot tmp = new ItemSlot();
            tmp.Item = new Item(itemSlot1.Item);
            tmp.ItemTooltip = itemSlot1.ItemTooltip.ToString();

            if (itemSlot2.Item != null)
            {
                itemSlot1.Item = new Item(itemSlot2.Item);
                itemSlot1.ItemTooltip = itemSlot2.ItemTooltip.ToString();
            }
            else
            {
                itemSlot1.Item = null;
                itemSlot1.ItemTooltip = "";
            }
            itemSlot2.Item = new Item(tmp.Item);
            itemSlot2.ItemTooltip = tmp.ItemTooltip.ToString();
        }

        public void SwapItemInventory(ItemSlot itemSlot1, ItemSlot itemSlot2)
        {
            if (itemSlot2.Item != null)
            {
                itemSlot1.Item = new Item(itemSlot2.Item);
                itemSlot1.ItemTooltip = itemSlot2.ItemTooltip.ToString();
            }
            else
            {
                itemSlot1.Item = null;
                itemSlot1.ItemTooltip = "";
            }
        }

        public Potion BrewPotion()
        {
            Potion potion = null;
            try
            {
                cauldronItemSlots.ForEach(slot => Cauldron.AddIngedient(slot?.Item));

                potion = Cauldron.BrewPotion();
                ClearSlots();
                OnPotionBrewd(potion);
            }
            catch (NotEnoughIngredients e)
            {
                logger.Error(e.Message);
            }
            return potion;
        }

        private void ClearSlots()
        {
            cauldronItemSlots.Clear();
        }

        protected void OnPotionBrewd(Item item)
        {
            InitializeEmptySlots();
            if(item.name.Equals("Health Rejuvenation Potion"))
                healthPotionMade(this, EventArgs.Empty);
            if(item.name.Equals("Minor Spell Power Potion"))
                manaPotionMade(this, EventArgs.Empty);
        }

        protected void OnCauldronOpened()
        {
            if (cauldronOpened != null)
                cauldronOpened(this, EventArgs.Empty);
        }

        protected override void OncollisionExit(object sender, GameObjectEventArgs e)
        {
            if (!e.gameObject.Tag.Equals("Cauldron") && Game1.boolCauldron == true)
            {
                OnCauldronOpened();
                Game1.boolCauldron = !(Game1.boolCauldron);
                Game1.shouldDrawInventory = !(Game1.shouldDrawInventory);
                Game1.boolInventory = !(Game1.boolInventory);
                ibuttonState = IButtonState.None;
            }
        }

        protected override void OnCollision(object sender, GameObjectEventArgs e)
        {
            if (Input.GetKeyDown(Microsoft.Xna.Framework.Input.Keys.E) && ibuttonState == IButtonState.None)
            {
                if (e.gameObject.Tag.Equals("Cauldron"))
                {
                    OnCauldronOpened();
                    Game1.boolCauldron = !(Game1.boolCauldron);
                    Game1.shouldDrawInventory = !(Game1.shouldDrawInventory);
                    Game1.boolInventory = !(Game1.boolInventory);
                    ibuttonState = IButtonState.IsBeingPressd;
                }
            }
            if (Input.GetKeyReleased(Microsoft.Xna.Framework.Input.Keys.E) && ibuttonState == IButtonState.IsBeingPressd)
            {
                ibuttonState = IButtonState.None;
            }
        }
    }
}