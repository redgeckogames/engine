﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.Scripts
{
    public class TextureDB
    {
        private static Dictionary<string, string> textures;

        public TextureDB(string textureDbFilePath)
        {
            textures = new Dictionary<string, string>();
            LoadTexturesFromFile(textureDbFilePath);
        }

        public static string GetTexturePath(string itemName)
        {
            string texturePath;
            var trimmedItemName = itemName.Replace(" ", "");

            if (textures.TryGetValue(trimmedItemName, out texturePath) == false)
            {
                throw new TexturePathNotFoundException("Texture for the item "
                     + itemName + " was not found make sure you have entered correct name");
            }
            else
            {
                return texturePath;
            }
        }

        public static void AddTexture(string itemName, string texturePath)
        {
            if (textures.ContainsKey(itemName) == false)
            {
                textures.Add(itemName, texturePath);
            }
            else
            {
                throw new ItemTextureNameException("Item called " + itemName + " already has a texture mapped to it");
            }
        }

        private void LoadTexturesFromFile(string filePath)
        {
            using (var mappedFile1 = MemoryMappedFile.CreateFromFile(filePath))
            {
                using (Stream mmStream = mappedFile1.CreateViewStream())
                {
                    using (StreamReader sr = new StreamReader(mmStream, ASCIIEncoding.UTF8))
                    {
                        while (!sr.EndOfStream)
                        {
                            var line = sr.ReadLine();
                            var lineWords = line.Split(' ');
                            AddTexture(lineWords[0].Trim(), lineWords[1].Trim());
                        }
                    }
                }
            }
        }
    }
}