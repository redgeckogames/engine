﻿using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Wytches.GameOwn;
using Wytches.ReptileEngine;

namespace Wytches.Scripts
{
    public class HumanScript : Script
    {
        public Human Human { get; set; }

        private GameObject destinationTarget;
        private static Logger logger = LogManager.GetCurrentClassLogger();
        private Timer treeCuttingTimer;
        private Timer treeFindingTimer;

        public HumanScript(GameObject gameObject) : base(gameObject)
        {
            this.GameObject.AddComponent<PathComponent>();
            treeFindingTimer = new Timer(5000);
            treeFindingTimer.Elapsed += TreeFindingTimer_Elapsed;
            treeFindingTimer.AutoReset = true;
            treeFindingTimer.Start();
        }

        private void TreeFindingTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            startLookingForATree();
        }

        private void startLookingForATree()
        {
            try
            {
                FindTreeToCut();
            }
            catch (TargetNotFoundException e)
            {
                logger.Error("no tree to cut yet");
            }
        }

        public void FindTreeToCut()
        {
            try
            {
                destinationTarget = PathDestinationFinder.Instance.GetRandomTarget(DestinationTarget.Tree);
                treeFindingTimer.Stop();
                PathComponent pathComponent = this.GameObject.GetComponent<PathComponent>();

                logger.Info("Tree to cut Found : " + destinationTarget.Transform.ToString());
                destinationTarget.GetComponent<StructureScript>().Structure.StructureDestroyed += OnStructoreDestroyed;

                pathComponent.SetDestination(destinationTarget);

                pathComponent.DestinationReached += OnDestinationReached;
                pathComponent.FindPath();
                pathComponent.StartGoPath();
                AnimationState.LoadAnimationForGameObject(this.GameObject, AnimationStateName.Running);
            }
            catch (TargetNotFoundException e)
            {
                logger.Error("no more trees to cut");
                this.GameObject.Dispose();

                throw new GameOver("All the trees are gone \n ... \n and so are you");
            }
        }

        private void OnDestinationReached(object sender, EventArgs e)
        {
            logger.Info("Human Destination Was reached");
            if (sender is PathComponent)
            {
                PathComponent component = sender as PathComponent;

                component.DestinationReached -= OnDestinationReached;

                treeCuttingTimer = new Timer(3000);
                treeCuttingTimer.Elapsed += OnAxeChop;
                treeCuttingTimer.AutoReset = true;
                treeCuttingTimer.Start();
            }
        }

        private void OnAxeChop(object sender, ElapsedEventArgs e)
        {
            if (destinationTarget != null)
            {
               // TODO: dodac do chopingu animacje do pliku
                AnimationState.LoadAnimationForGameObject(this.GameObject, AnimationStateName.Chopping);
                try
                {
                    logger.Info("Cutting" + destinationTarget.Tag);
                    destinationTarget.GetComponent<StructureScript>().InteractWithStructure("HitMe", 5);
                }
                catch (ComponentNotFoundException ex)
                {
                    logger.Error(ex.Message);
                    treeCuttingTimer.Stop();
                    FindTreeToCut();
                }
            }
        }

        private void OnStructoreDestroyed(object sender, EventArgs e)
        {
            logger.Info("Tree Was Cut");
            treeCuttingTimer.Stop();
            destinationTarget.GetComponent<StructureScript>().Structure.StructureDestroyed -= OnStructoreDestroyed;
            logger.Info("disposing structure");
            PathDestinationFinder.Instance.RemoveTarger(DestinationTarget.Tree, destinationTarget);

            destinationTarget.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>("pieniek\\Bark_1_hue");
            destinationTarget.GetComponent<MeshRenderer>().Model = MyContentManager.Load<Model>("pieniek\\stump");
            destinationTarget = null;
            Game1.scene.FindGameObjectWithtag("wiedzma").GetComponent<WitchScript>().Attributes.currentHealthPoints -= 10;
           // AnimationState.LoadAnimationForGameObject(this.GameObject, AnimationStateName.Idle);
            FindTreeToCut();
        }

        private void ReturnHome()
        {
            destinationTarget = PathDestinationFinder.Instance.GetRandomTarget(DestinationTarget.House);
            PathComponent pathComponent = this.GameObject.GetComponent<PathComponent>();

            logger.Info("Going Home : " + destinationTarget.Transform.ToString());

            pathComponent.SetDestination(destinationTarget);

            pathComponent.DestinationReached += HomeReached;
            pathComponent.FindPath();
            pathComponent.StartGoPath();
        }

        private void HomeReached(object sender, EventArgs e)
        {
            if (sender is PathComponent)
            {
                PathComponent pathComponent = sender as PathComponent;
                pathComponent.DestinationReached -= HomeReached;
                logger.Info("arrived home : " + destinationTarget.Transform.ToString());
                destinationTarget = null;
            }
            FindTreeToCut();
        }
    }
}