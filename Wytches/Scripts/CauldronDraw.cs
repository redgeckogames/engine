﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Wytches.ReptileEngine;
using Wytches.Wytches_Game;

namespace Wytches.Scripts
{
    internal class CauldronDraw : DrawableGameComponent
    {
        private List<ItemSlot> cauldronItemSlots;
        private int MaxItemCount;
        private bool CauldornOpened { get; set; } = false;

        protected SpriteBatch spriteBatch;

        private Camera camera;
        private BasicEffect basicEffect;
        private const int _tileSize = 100;
        private Texture2D cauldronTexture;
        private Vector2 cauldronListPosition;
        private Button mixButton;

        public Vector2 CauldronListPosition
        {
            get
            {
                return this.cauldronListPosition;
            }
        }

        private Vector2 cauldronTextPosition;

        public Vector2 CauldronTextPosition
        {
            get
            {
                return this.cauldronTextPosition;
            }
        }

        private CauldronScript cauldronScript;
        private SpriteFont _defaultFont;

        public CauldronDraw(Game game, Camera camera, CauldronScript cauldronScript) : base(game)
        {
            cauldronListPosition = new Vector2(250, 200);
            cauldronTextPosition = new Vector2(350, 400);
            _defaultFont = MyContentManager.Load<SpriteFont>("DefaultFont");
            this.cauldronScript = cauldronScript;
            this.cauldronScript.cauldronOpened += OnCauldronOpened;
            this.cauldronItemSlots = cauldronScript.CauldronItemSlots;
            this.MaxItemCount = cauldronScript.MaxItemCount;
            this.camera = camera;
            basicEffect = new BasicEffect(GraphicsDevice);
            spriteBatch = MySpriteBatch.returnSpriteBatch();
            cauldronTexture = MyContentManager.Load<Texture2D>("Objects\\kociolekSprite");
            mixButton = new Button(MyContentManager.Load<Texture2D>("Objects\\buttons\\ButtonEliksir"),cauldronTextPosition, game, camera);
        }

        private void OnCauldronOpened(object sender, EventArgs e)
        {
            this.CauldornOpened = !CauldornOpened;
        }

        public override void Draw(GameTime gameTime)
        {
            //wyłącznie Z buffora i rysowanie spirtów na rzeczach z 3d

            if (CauldornOpened)
            {
                Game.IsMouseVisible = true;

                GraphicsDevice.BlendState = BlendState.AlphaBlend;
                GraphicsDevice.DepthStencilState = DepthStencilState.None;
                GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

                basicEffect.World = Matrix.CreateScale(1, -1, 1);
                basicEffect.View = camera.view;
                basicEffect.Projection = camera.projection;
                //start drawing

                spriteBatch.Begin();

                DrawBoard();
                DrawCauldron();
                

                //end drawing
                spriteBatch.End();
                DrawText();
            }

            base.Draw(gameTime);
        }

        private void DrawBoard()
        {
            Rectangle squareToDrawPosition = new Rectangle();   //the square to draw (local variable to avoid creating a new variable per square)
            Color colorToUse = Color.White;
            //for all columns
            for (int i = 0; i < cauldronItemSlots.Count; i++)
            {
                //   squareToDrawPosition = new Rectangle((int)(1.5f * x * _tileSize + 100), (int)(_tileSize + 50), _tileSize, _tileSize);
                squareToDrawPosition = new Rectangle((int)(cauldronItemSlots[i].PositionInInventory.X * _tileSize + cauldronListPosition.X), (int)(cauldronItemSlots[i].PositionInInventory.Y * _tileSize + cauldronListPosition.Y), _tileSize, _tileSize);

                if (cauldronItemSlots[i].Item == null)
                    spriteBatch.Draw(getTextureByName("empty"), squareToDrawPosition, colorToUse);
                else
                    spriteBatch.Draw(getTextureByName(cauldronItemSlots[i].Item?.name), squareToDrawPosition, colorToUse);
            }
        }

        private void DrawCauldron()
        {
            //spriteBatch.Draw(cauldronTexture, new Rectangle(250, 550, 200, 200), Color.White);
            spriteBatch.Draw(cauldronTexture, new Rectangle(77, -25, cauldronTexture.Width, cauldronTexture.Height), null, Color.White);
        }

        private void DrawText()
        {
            //spriteBatch.DrawString(_defaultFont, "MIESZAJ ", new Vector2(cauldronTextPosition.X, cauldronTextPosition.Y), Color.White);
            mixButton.Draw();
        }

        private Texture2D getTextureByName(string name)
        {
            return MyContentManager.Load<Texture2D>(TextureDB.GetTexturePath(name));
        }

        public void Update()
        {
            mixButton.Update();
        }
    }
}