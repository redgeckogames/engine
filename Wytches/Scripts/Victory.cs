﻿using System;
using System.Runtime.Serialization;

namespace Wytches.Scripts
{
    [Serializable]
    internal class Victory : Exception
    {
        public Victory()
        {
        }

        public Victory(string message) : base(message)
        {
        }

        public Victory(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected Victory(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}