﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Wytches.GameOwn;
using Wytches.ReptileEngine;
using NLog;

namespace Wytches.Scripts
{
    public class StructureScript : Script
    {
        private Structure structure;

        public Structure Structure

        {
            get
            {
                return structure;
            }
            set
            {
                structure = value;
                structure.StructureDestroyed += Structure_StructureDestroyed;
            }
        }

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public StructureScript(GameObject gameObject) : base(gameObject)
        {
            //Structure.StructureDestroyed += Structure_StructureDestroyed;
        }

        private void Structure_StructureDestroyed(object sender, EventArgs e)
        {
        }

        public void InteractWithStructure(string interActionName, object interactionParam)
        {
            logger.Info("interaction " + interActionName + " with parameter value " + interactionParam);
            Structure?.Interact(interActionName, interactionParam);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);

            Structure.IsStructureDestroyed();
        }
    }
}