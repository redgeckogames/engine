﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Wytches.GameOwn;
using Wytches.ReptileEngine;

namespace Wytches.Scripts
{
    public class WitchScript : Script
    {
        private InventoryScript inventory;
        public WitchAttribiutes Attributes { get; set; }

        public WitchScript(GameObject gameObject) : base(gameObject)
        {
            Attributes = new WitchAttribiutes()
            {
                currentHealthPoints = 100,
                maxHealthPoints = 100,
                currentSpellPower = 0,
                maxSpellPower = 200
            };

            inventory = this.GameObject.GetComponent<InventoryScript>();
            inventory.ItemConsumed += OnItemConsumed;
        }

        private void OnItemConsumed(object sender, ItemEventArgs e)
        {
            Consumable consumable = e.Item as Consumable;

            ConsumeItem(consumable.Consume().GetUssageEffect());
        }

        private void ConsumeItem(WitchAttribiutes attributes)
        {
            if (CanConsumeFully(attributes))
            {
                this.Attributes += attributes;
            }
            else
            {
                ConsumeAsMuchAsPossible(attributes);
            }
        }

        private void ConsumeAsMuchAsPossible(WitchAttribiutes attributes)
        {
            WitchAttribiutes currentAttrributes = new WitchAttribiutes(this.Attributes);

            currentAttrributes += attributes;

            if (currentAttrributes.currentHealthPoints > currentAttrributes.maxHealthPoints)
            {
                Attributes.currentHealthPoints = Attributes.maxHealthPoints;
            }
            if (currentAttrributes.currentSpellPower > currentAttrributes.maxSpellPower)
            {
                Attributes.currentSpellPower = Attributes.maxSpellPower;
            }
            Attributes.healthRegeneration = currentAttrributes.healthRegeneration;
        }

        public bool CanConsumeFully(WitchAttribiutes attributes)
        {
            WitchAttribiutes currentAttrributes = new WitchAttribiutes(this.Attributes);

            currentAttrributes += attributes;

            if (currentAttrributes.currentHealthPoints > currentAttrributes.maxHealthPoints)
            {
                return false;
            }
            else if (currentAttrributes.currentSpellPower > currentAttrributes.maxSpellPower)
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        public float getCurrentWitchHealth()
        {
            return getCurretWitchStat(this.Attributes.currentHealthPoints, this.Attributes.maxHealthPoints);
        }

        internal float getCurrentWitchMana()
        {
            return getCurretWitchStat(this.Attributes.currentSpellPower, this.Attributes.maxSpellPower);
        }

        private float getCurretWitchStat(float current, float maximum)
        {
            return current / maximum;
        }

        public override void Update(GameTime gameTime)
        {
            if (this.Attributes.currentHealthPoints <= 0)
            {
                throw new GameOver("You have Died!");
            }
            base.Update(gameTime);
        }
    }
}