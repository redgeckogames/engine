﻿using System;
using System.Diagnostics;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.ReptileEngine;
using Microsoft.Xna.Framework;

namespace Wytches.Scripts
{
    public class CameraFollow : Script
    {
        private Camera mainCam;

        public CameraFollow(GameObject gameObject) : base(gameObject)
        {
            mainCam = gameObject.GetComponent<Camera>();
        }

        public override void Update(GameTime gameTime)
        {
            mainCam.Position.X = GameObject.Transform.Position.X + mainCam.startMove.X;
            mainCam.Position.Y = GameObject.Transform.Position.Y + mainCam.startMove.Y;
            mainCam.Position.Z = GameObject.Transform.Position.Z + mainCam.startMove.Z;
            mainCam.view = Matrix.CreateLookAt(mainCam.Position, GameObject.Transform.Position, Vector3.Up);
            //   Debug.WriteLine("dsdhfsiufbk");

            base.Update(gameTime);
        }

        public Camera getMainCam()
        {
            return mainCam;
        }
    }
}