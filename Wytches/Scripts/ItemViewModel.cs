﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.GameOwn;

namespace Wytches.Scripts
{
    public class ItemViewModel
    {
        public Item Item { get; set; }
        public Texture2D ItemTexture { get; set; }
    }
}
