﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.GameOwn
{
    public class DemonFactory : NpcFactory
    {
        private NameGenerator<WitchAttribiutes> nameGenerator;
        private Logger logger = LogManager.GetCurrentClassLogger();

        public DemonFactory(NameGenerator<WitchAttribiutes> nameGenerator)
        {
            this.nameGenerator = nameGenerator;
        }

        public Npc CreateNpc(WitchAttribiutes mixedNpcAttributes)
        {
            Demon demon = new Demon()
            {
                Attributes = mixedNpcAttributes,
                Name = nameGenerator.GenerateName(mixedNpcAttributes)
            };
            demon.Attributes.currentHealthPoints += 10;

            demon.Attributes.currentSpellPower += 10;
            logger.Info("Created Demon with Name :" + demon.Name);
            return demon;
        }
    }
}