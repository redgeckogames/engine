﻿namespace Wytches.GameOwn
{
    public interface PotionFactory
    {
        Potion CreatePotion(WitchAttribiutes mixedItemUsageEffects);
    }
}