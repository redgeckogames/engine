﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    public class HealthRestoration : UsageEffectDecorator
    {
        public HealthRestoration(UsageEffect newEffect) : base(newEffect)
        {
        }

        public override WitchAttribiutes GetUssageEffect()
        {
            WitchAttribiutes attributeBonus = new WitchAttribiutes();
            attributeBonus.currentHealthPoints = 10;

            return this.effect.GetUssageEffect() + attributeBonus;
        }
    }
}