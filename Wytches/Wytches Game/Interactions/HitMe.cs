﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.GameOwn.Interactions
{
    public class HitMe : Interaction
    {
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public HitMe()
        {
        }

        public void performAction(Interactable interActable, object interactionParameter)
        {
            logger.Info("Intractable Health before Hitting" + interActable.Attributes.currentHealthPoints);
            int? damageValue = 0;
            if (interactionParameter != null)
            {
                damageValue = interactionParameter as int?;
            }
            interActable.Attributes.currentHealthPoints -= (int)damageValue;
            logger.Info("Intractable Health after Hitting" + interActable.Attributes.currentHealthPoints);
        }
    }
}