﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    public class FarmTile
    {
        private Farmable plant;

        public Farmable Plant
        {
            get { return plant; }
            set
            {
                plant = value;
            }
        }

        public FarmTile()
        {
        }
    }
}