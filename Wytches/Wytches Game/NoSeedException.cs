﻿using System;
using System.Runtime.Serialization;

namespace Wytches.GameOwn
{
    [Serializable]
    internal class NoSeedException : Exception
    {
        public NoSeedException()
        {
        }

        public NoSeedException(string message) : base(message)
        {
        }

        public NoSeedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoSeedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}