﻿namespace Wytches.GameOwn
{
    public interface Interaction
    {
        void performAction(Interactable interActable, object interactionParameter);
    }
}