﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    public class MinorManaRestoration : UsageEffectDecorator
    {
        public MinorManaRestoration(UsageEffect newEffect) : base(newEffect)
        {
        }

        public override WitchAttribiutes GetUssageEffect()
        {
            var bonusAttr = new WitchAttribiutes();
            bonusAttr.currentSpellPower = 50;

            return effect.GetUssageEffect() + bonusAttr;
        }
    }
}