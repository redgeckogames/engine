﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.GameOwn
{
    public interface NameGenerator<T> where T : class
    {
        string GenerateName(T modifiers);
    }
}