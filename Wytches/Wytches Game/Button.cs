﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Wytches.ReptileEngine;

namespace Wytches.Wytches_Game
{
    class Button : DrawableGameComponent
    {
        private Texture2D texture;
        private Vector2 position;
        private Rectangle rectangle;

        private Color color;

        private bool down;
        public bool isClicked;
        private SpriteBatch spriteBatch;
        private Game game;
        private Camera camera;
        private BasicEffect basicEffect;

        public Button(Texture2D texture, Vector2 position, Microsoft.Xna.Framework.Game game, Camera camera) : base(game)
        {
            this.game = game;
            this.camera = camera;
            this.texture = texture;
            this.position = position;
            color = Color.White;
            this.rectangle = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);
            spriteBatch = MySpriteBatch.returnSpriteBatch();
            basicEffect = new BasicEffect(GraphicsDevice);
        }

        public void Update()
        {
            MouseState mouse = Mouse.GetState();

            Rectangle mouseRectangle = new Rectangle(mouse.X, mouse.Y, 1, 1);

            if (mouseRectangle.Intersects(rectangle))
            {
                if (color.A == 255) down = false;
                if (color.A == 0) down = true;
                if (down) color.A += 3;
                else color.A -= 3;
                if (mouse.LeftButton == ButtonState.Pressed)
                {
                    isClicked = true;
                    color.A = 255;
                }
            }
            else if (color.A < 255)
            {
                color.A += 3;
            }
        }

        public void Draw()
        {
            Game.IsMouseVisible = true;
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.DepthStencilState = DepthStencilState.None;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            basicEffect.World = Matrix.CreateScale(1, -1, 1);
            basicEffect.View = camera.view;
            basicEffect.Projection = camera.projection;
            //start drawing

            spriteBatch.Begin();

            spriteBatch.Draw(texture, rectangle, color);

            //end drawing
            spriteBatch.End();
            
        }
    }
}
