﻿namespace Wytches.GameOwn
{
    public interface Consumable
    {
        UsageEffect Consume();
    }
}