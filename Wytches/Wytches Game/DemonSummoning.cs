﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.GameOwn
{
    public class DemonSummoning : Ritual
    {
        private NpcFactory npcFactory;

        private List<Item> offerings;

        public DemonSummoning(NpcFactory npcFactory)
        {
            this.npcFactory = npcFactory;
            this.offerings = new List<Item>();
        }

        public void AddOffering(Item offering)
        {
            this.offerings.Add(offering);
        }

        public Npc Perform()
        {
            Demon demon = npcFactory.CreateNpc(MixAttributesFromOfferings()) as Demon;

            return demon;
        }

        private WitchAttribiutes MixAttributesFromOfferings()
        {
            WitchAttribiutes attributes = new WitchAttribiutes();
            if (offerings.Count > 0)
            {
                offerings.ForEach(offering => attributes += offering?.usageEffect?.GetUssageEffect());
            }
            else
            {
                throw new NoOfferingsException("No Offerings Were Provided");
            }

            return attributes;
        }
    }
}