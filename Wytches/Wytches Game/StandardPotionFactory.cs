﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    public class StandardPotionFactory : PotionFactory
    {
        private IEnumerable<Potion> potionRecipeList;

        public StandardPotionFactory(IEnumerable<Potion> potionRecipeList)
        {
            this.potionRecipeList = potionRecipeList;
        }

        public Potion CreatePotion(WitchAttribiutes mixedItemUsageEffects)
        {
            Potion createdPotion = FindPotionOnTheRecipeList(mixedItemUsageEffects);
            if (createdPotion == null)
            {
                return new Potion(0, "Unsuccessful Potion", new SimpleEffect());
            }
            else
            {
                return createdPotion;
            }
        }

        private Potion FindPotionOnTheRecipeList(WitchAttribiutes mixedItemUsageEffects)
        {
            return potionRecipeList
                .Where(p => p.usageEffect.GetUssageEffect()
                .Equals(mixedItemUsageEffects))
                .FirstOrDefault();
        }
    }
}