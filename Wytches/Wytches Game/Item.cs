﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    [Serializable]
    public class Item
    {
        public int id;
        public string name;
        public UsageEffect usageEffect;

        public Item(int id, string name, UsageEffect usageEffect)
        {
            this.id = id;
            this.name = name;
            this.usageEffect = usageEffect;
        }

        public Item(Item other)
        {
            this.id = other.id;
            this.name = other.name;
            this.usageEffect = (UsageEffect)other.usageEffect.Clone();
        }

        public override bool Equals(object obj)
        {
            Item itemObj = obj as Item;
            if (itemObj == null)
            {
                return false;
            }
            else
            {
                return id.Equals(itemObj.id) && name.Equals(itemObj.name) && usageEffect.Equals(itemObj.usageEffect);
            }

            // return base.Equals(obj);
        }
    }
}