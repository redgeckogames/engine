﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.GameOwn
{
    public class DemonNameGenerator : NameGenerator<WitchAttribiutes>
    {
        private List<string> nameList;
        private MultiValueDictionary<Attribute, string> attributeBasedNames;

        public DemonNameGenerator()
        {
        }

        private void InitBaseDemonNames()
        {
            nameList = new List<string>()
            {
                "Amon",
                "Abaddon",
                "Abraxas",
                "Alastor",
                "Archon",
                "Baal",
                "Behemoth",
                "Gorgon",
                "Gremory",
                "Legion",
                "Lilith",
            };
        }

        private void InitAttributeBasedNames()
        {
            attributeBasedNames = new MultiValueDictionary<Attribute, string>();

            attributeBasedNames.AddRange(Attribute.Health,
                new List<string>()
                {
                    "The Tenacious",
                    "The Undying"
                });
            attributeBasedNames.AddRange(Attribute.SpellPower,
               new List<string>()
               {
                    "The Strong",
                    "The Mighty",
                    "The Reaper"
               });
        }

        public string GenerateName(WitchAttribiutes attributes)
        {
            InitBaseDemonNames();
            InitAttributeBasedNames();

            StringBuilder nameBuilder = new StringBuilder();

            nameBuilder.Append(nameList.ElementAt(GetRandomNumber(nameList.Count)) + " ");

            Attribute dominatingAttribute = FindDominatingAttribute(attributes);

            nameBuilder.Append(GetRandomAttributeBasedName(dominatingAttribute));

            return nameBuilder.ToString();
        }

        private Attribute FindDominatingAttribute(WitchAttribiutes attributes)
        {
            if (attributes.maxSpellPower > attributes.maxHealthPoints)
            {
                return Attribute.SpellPower;
            }
            else
            {
                return Attribute.Health;
            }
        }

        private string GetRandomAttributeBasedName(Attribute attribute)
        {
            IReadOnlyCollection<string> attributeNames;

            this.attributeBasedNames.TryGetValue(attribute, out attributeNames);

            return attributeNames.ElementAt(GetRandomNumber(attributeNames.Count));
        }

        private int GetRandomNumber(int maxNumber)
        {
            Random random = new Random();

            return random.Next(maxNumber);
        }
    }

    internal enum Attribute
    {
        Health,
        SpellPower
    }
}