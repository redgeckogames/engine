﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    public class SmallCauldron : Cauldron
    {
        public SmallCauldron(PotionFactory potionFactory) : base(potionFactory)
        {
        }

        public override void AddIngedient(Item ingredient)
        {
            if (IsCauldronFull() == false)
            {
                if (ingredient != null)
                    this.ingredients?.Add(ingredient);
            }
            else
                throw new CauldronFullException();
        }

        protected override bool IsCauldronFull()
        {
            return ingredients.Count == 3;
        }

        public override Potion BrewPotion()
        {
            WitchAttribiutes mixedEffects = null;

            if (IsCauldronFull())
            {
                mixedEffects = MixItemsSpecialEffects();

                this.ingredients.Clear();
            }
            else
            {
                throw new NotEnoughIngredients();
            }

            return potionFactory.CreatePotion(mixedEffects);
        }

        private WitchAttribiutes MixItemsSpecialEffects()
        {
            WitchAttribiutes attr = new WitchAttribiutes();
            foreach (var item in Ingredients)
            {
                attr += item?.usageEffect?.GetUssageEffect();
            }
            return attr;
        }
    }
}