﻿namespace Wytches.GameOwn
{
    public interface Npc : Interactable
    {
        string Name { get; set; }
    }
}