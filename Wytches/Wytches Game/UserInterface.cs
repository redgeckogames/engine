﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Wytches.GameOwn;
using Wytches.ReptileEngine;
using Wytches.Scripts;

namespace Wytches.Wytches_Game
{
    public class UserInterface : DrawableGameComponent
    {
        protected SpriteBatch spriteBatch;
        private Camera camera;
        private BasicEffect basicEffect;

        private Texture2D manaLifeBars;
        private Vector2 barsPosition;

        private Texture2D manaGauge;
        private Texture2D lifeGauge;

        private Vector2 manaGaugePosition;
        private Vector2 lifeGaugePosition;

        private int fullLife;
        private int fullMana;

        private Texture2D circles;

        private int currentMana = 500;
        private Rectangle manaGaugeRectangle;

        // private Rectangle manaBarRectangle;
        private Rectangle lifeGaugeRectangle;

        private Rectangle manaLifeBarsRectangle;
        private Rectangle circlesRectangle;

        private int maxHealthGaugeWidth;
        private int maxManaGaugeWidth;

        public UserInterface(Game game, Camera camera) : base(game)
        {
            barsPosition = new Vector2(100, 50);
            manaGaugePosition = new Vector2(150, 118);
            lifeGaugePosition = new Vector2(150, -53);
            manaLifeBars = MyContentManager.Load<Texture2D>("Objects\\bars\\barspr1");
            manaGauge = MyContentManager.Load<Texture2D>("Objects\\bars\\manabarsprite");
            lifeGauge = MyContentManager.Load<Texture2D>("Objects\\bars\\lifebarsprite");
            circles = MyContentManager.Load<Texture2D>("Objects\\bars\\pngbar");

            this.camera = camera;
            basicEffect = new BasicEffect(GraphicsDevice);
            spriteBatch = MySpriteBatch.returnSpriteBatch();

            fullLife = lifeGauge.Width;
            fullMana = manaGauge.Width;

            manaGaugeRectangle = new Rectangle(23, 27, manaGauge.Width / 4, manaGauge.Height / 4);
            //manaBarRectangle = new Rectangle(0, 0, manaGauge.Width/2, manaGauge.Height/2);
            lifeGaugeRectangle = new Rectangle(23, -15, lifeGauge.Width / 4, lifeGauge.Height / 4);
            manaLifeBarsRectangle = new Rectangle(10, 10, manaLifeBars.Width / 4, manaLifeBars.Height / 4);
            circlesRectangle = new Rectangle(10, 10, circles.Width / 4, circles.Height / 4);

            maxHealthGaugeWidth = lifeGaugeRectangle.Width;
            maxManaGaugeWidth = manaGaugeRectangle.Width;
        }

        public override void Draw(GameTime gameTime)
        {
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.DepthStencilState = DepthStencilState.None;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            basicEffect.World = Matrix.CreateScale(1, -1, 1);
            basicEffect.View = camera.view;
            basicEffect.Projection = camera.projection;
            //start drawing

            spriteBatch.Begin();

            DrawBars();

            //end drawing
            spriteBatch.End();
        }

        private void DrawBars()
        {
            spriteBatch.Draw(manaLifeBars, manaLifeBarsRectangle, new Rectangle(0, 0, manaLifeBars.Width, manaLifeBars.Height), Color.White);
            spriteBatch.Draw(manaGauge, manaGaugeRectangle, null, Color.White);
            spriteBatch.Draw(lifeGauge, lifeGaugeRectangle, null, Color.White);
            spriteBatch.Draw(circles, circlesRectangle, null, Color.White);
        }

        public void Update(GameObject playerWitch)
        {
            manaGaugeRectangle.Width = (int)Math.Round((maxManaGaugeWidth * playerWitch.GetComponent<WitchScript>().getCurrentWitchMana()), 0);
            lifeGaugeRectangle.Width = (int)Math.Round((maxHealthGaugeWidth * playerWitch.GetComponent<WitchScript>().getCurrentWitchHealth()), 0);
            //  Debug.WriteLine("szerokosc" + lifeGaugeRectangle.Width);
        }
    }
}