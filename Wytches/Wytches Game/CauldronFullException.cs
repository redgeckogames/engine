﻿using System;
using System.Runtime.Serialization;

namespace Wytches.GameOwn
{
    [Serializable]
    public class CauldronFullException : Exception
    {
        public CauldronFullException()
        {
            Game1.isInformationWindowOpened = true;
            Game1.isCounterActive = true;
            Game1.infoMessage = "Dodales za malo skladnikow!";
        }

        public CauldronFullException(string message) : base(message)
        {
            
        }

        public CauldronFullException(string message, Exception innerException) : base(message, innerException)
        {
            
        }

        protected CauldronFullException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
            
        }
    }
}