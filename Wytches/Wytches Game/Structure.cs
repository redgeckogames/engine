﻿using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.GameOwn
{
    public class Structure : Interactable, ICloneable
    {
        public WitchAttribiutes Attributes { get; set; }

        public InteractionSet Interactions { get; set; }

        private static Logger logger = LogManager.GetCurrentClassLogger();

        public Structure()
        {
        }

        public Structure(Structure other)
        {
            this.Interactions = new InteractionSet(other.Interactions);
            this.Attributes = new WitchAttribiutes(other.Attributes);
        }

        public event EventHandler StructureDestroyed;

        public void Interact(string interactionName, object interactionParameter)
        {
            Interactions.GetInterAction(interactionName).performAction(this, interactionParameter);
        }

        public void IsStructureDestroyed()
        {
            if (this.Attributes.currentHealthPoints <= 0)
            {
                OnStructureDestroy();
            }
        }

        protected virtual void OnStructureDestroy()
        {
            StructureDestroyed?.Invoke(this, EventArgs.Empty);
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }
    }
}