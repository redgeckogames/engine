﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    public enum PlantGrowth
    {
        Seed,
        UnRipe,
        FullyGrown
    }
}