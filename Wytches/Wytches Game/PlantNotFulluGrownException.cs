﻿using System;
using System.Runtime.Serialization;

namespace Wytches.GameOwn
{
    [Serializable]
    internal class PlantNotFulluGrownException : Exception
    {
        public PlantNotFulluGrownException()
        {
        }

        public PlantNotFulluGrownException(string message) : base(message)
        {
        }

        public PlantNotFulluGrownException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected PlantNotFulluGrownException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}