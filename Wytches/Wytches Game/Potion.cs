﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    public class Potion : Item, Consumable
    {
        public Potion(int id, string name, UsageEffect effect) : base(id, name, effect)
        {
        }

        public UsageEffect Consume()
        {
            return this.usageEffect;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                Potion tmp = obj as Potion;
                return this.usageEffect.Equals(tmp.usageEffect);
            }
        }
    }
}