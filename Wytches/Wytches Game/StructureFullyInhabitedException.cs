﻿using System;
using System.Runtime.Serialization;

namespace Wytches.GameOwn
{
    [Serializable]
    public class StructureFullyInhabitedException : Exception
    {
        public StructureFullyInhabitedException()
        {
        }

        public StructureFullyInhabitedException(string message) : base(message)
        {
        }

        public StructureFullyInhabitedException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected StructureFullyInhabitedException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}