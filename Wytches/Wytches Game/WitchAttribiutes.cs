﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    [Serializable]
    public class WitchAttribiutes
    {
        public int maxHealthPoints = 0;
        public int currentHealthPoints = 0;
        public int healthRegeneration = 0;
        public int maxSpellPower = 0;
        public int currentSpellPower = 0;

        public WitchAttribiutes()
        {
        }

        public WitchAttribiutes(WitchAttribiutes other)
        {
            this.currentHealthPoints = other.currentHealthPoints;
            this.currentSpellPower = other.currentSpellPower;
            this.healthRegeneration = other.healthRegeneration;
            this.maxHealthPoints = other.maxHealthPoints;
            this.maxSpellPower = other.maxSpellPower;
        }

        public static WitchAttribiutes operator +(WitchAttribiutes w1, WitchAttribiutes w2)
        {
            WitchAttribiutes bonusToAtributes = new WitchAttribiutes();

            bonusToAtributes.healthRegeneration = w1.healthRegeneration + w2.healthRegeneration;
            bonusToAtributes.maxHealthPoints = w1.maxHealthPoints + w2.maxHealthPoints;
            bonusToAtributes.currentHealthPoints = w1.currentHealthPoints + w2.currentHealthPoints;
            bonusToAtributes.currentSpellPower = w1.currentSpellPower + w2.currentSpellPower;
            bonusToAtributes.maxSpellPower = w1.maxSpellPower + w2.maxSpellPower;

            return bonusToAtributes;
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                WitchAttribiutes itemObj = obj as WitchAttribiutes;

                return maxHealthPoints.Equals(itemObj.maxHealthPoints)
                    && maxSpellPower.Equals(itemObj.maxSpellPower) 
                    && healthRegeneration.Equals(itemObj.healthRegeneration) 
                    && currentSpellPower.Equals(itemObj.currentSpellPower);
            }
        }

        public override int GetHashCode()
        {
            return (healthRegeneration.GetHashCode() ^ maxHealthPoints.GetHashCode() & maxSpellPower.GetHashCode());
        }
    }
}