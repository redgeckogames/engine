﻿using System;
using System.Runtime.Serialization;

namespace Wytches.GameOwn
{
    [Serializable]
    internal class HomeAlreadySetException : Exception
    {
        public HomeAlreadySetException()
        {
        }

        public HomeAlreadySetException(string message) : base(message)
        {
        }

        public HomeAlreadySetException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected HomeAlreadySetException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}