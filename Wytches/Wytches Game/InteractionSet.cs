﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.GameOwn
{
    public class InteractionSet : ICloneable
    {
        private Dictionary<string, Interaction> availableInteractions;

        public InteractionSet()
        {
            availableInteractions = new Dictionary<string, Interaction>();
        }

        public InteractionSet(InteractionSet other)
        {
            this.availableInteractions = new Dictionary<string, Interaction>(other.availableInteractions);
        }

        public void AddInterAction(string name, Interaction interaction)
        {
            availableInteractions.Add(name, interaction);
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public Interaction GetInterAction(string interactionName)
        {
            Interaction interaction = null;
            if (availableInteractions.TryGetValue(interactionName, out interaction))
                return interaction;
            else
                throw new NoInterActionFoundException("Interaction with the name " + interactionName + " was not found in this Entity's InteractionSet");
        }
    }
}