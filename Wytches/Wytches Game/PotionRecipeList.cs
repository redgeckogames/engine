﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    public class PotionRecipeList : IEnumerable<Potion>
    {
        private List<Potion> potions;

        public PotionRecipeList()
        {
            Potion[] recepies =
            {
                new Potion(2,"Minor Spell Power Potion",
                new MinorManaRestoration(new MinorManaRestoration(new MinorManaRestoration(new SimpleEffect())))),

                new Potion(1,"Health Rejuvenation Potion",
                  new HealthRestoration(new HealthRestoration(new HealthRestoration(new SimpleEffect()))))

               
            };
            potions = new List<Potion>(recepies);
        }

        public IEnumerator<Potion> GetEnumerator()
        {
            return this.potions.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return this.potions.GetEnumerator();
        }
    }
}