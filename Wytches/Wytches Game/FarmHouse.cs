﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.GameOwn
{
    public class FarmHouse : Structure
    {
        private List<Npc> inhabitants;
        public int NpcCapacity { get; set; }

        public FarmHouse()
        {
            this.inhabitants = new List<Npc>();
        }

        public FarmHouse(Structure other) : base(other)
        {
            if (other is FarmHouse)
            {
                FarmHouse otherHouse = other as FarmHouse;
                this.NpcCapacity = otherHouse.NpcCapacity;
                this.inhabitants = new List<Npc>();
            }
        }

        public void AddNpcToTheStructure(Npc npc)
        {
            if (inhabitants.Count < NpcCapacity)
            {
                inhabitants.Add(npc);
            }
            else
            {
                throw new StructureFullyInhabitedException("Structure is already occupied by maximum number of Inhabitants" +
                    "which for this structure is " + NpcCapacity);
            }
        }
    }
}