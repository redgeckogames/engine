﻿using System;
using System.Runtime.Serialization;

namespace Wytches.GameOwn
{
    [Serializable]
    internal class NoInterActionFoundException : Exception
    {
        public NoInterActionFoundException()
        {
        }

        public NoInterActionFoundException(string message) : base(message)
        {
        }

        public NoInterActionFoundException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoInterActionFoundException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}