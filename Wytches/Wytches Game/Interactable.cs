﻿using System.Collections.Generic;

namespace Wytches.GameOwn
{
    public interface Interactable
    {
        InteractionSet Interactions { get; set; }

        WitchAttribiutes Attributes { get; set; }

        void Interact(string interactionName, object interactionParameter);
    }
}