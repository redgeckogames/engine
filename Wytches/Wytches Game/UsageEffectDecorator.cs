﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    public abstract class UsageEffectDecorator : UsageEffect
    {
        protected UsageEffect effect;

        public UsageEffectDecorator(UsageEffect newEffect)
        {
            this.effect = newEffect;
        }

        public virtual WitchAttribiutes GetUssageEffect()
        {
            return effect.GetUssageEffect();
        }

        public object Clone()
        {
            return this.MemberwiseClone();
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                UsageEffectDecorator tmp = obj as UsageEffectDecorator;
                return tmp.GetUssageEffect().Equals(this.GetUssageEffect());
            }
        }
    }
}