﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    public class Plant : Item, Consumable, Farmable
    {
        private int timeNeededToGrow;
        private PlantGrowth growth;

        public Plant(Plant other)
             : base(other)
        {
            this.growth = other.growth;
            this.timeNeededToGrow = other.timeNeededToGrow;
        }

        public Plant(int id, string name, UsageEffect usageEffect, int timeNeededToGrow) : base(id, name, usageEffect)
        {
            this.timeNeededToGrow = timeNeededToGrow;
            growth = PlantGrowth.Seed;
        }

        public UsageEffect Consume()
        {
            return this.usageEffect;
        }

        public Farmable Harvest()
        {
            Farmable grownPlant = null;

            if (HowMuchHasGrown().Equals(PlantGrowth.FullyGrown))
            {
                grownPlant = new Plant(this);

                this.growth = PlantGrowth.UnRipe;
            }
            else
            {
                throw new PlantNotFulluGrownException("This plant is not fully grown yet");
            }

            return grownPlant;
        }

        public void GrowSeed()
        {
            if (this.growth != PlantGrowth.FullyGrown)
            {
                this.growth++;
            }
        }

        public PlantGrowth HowMuchHasGrown()
        {
            return this.growth;
        }

        public int getTimeNeededToGrow()
        {
            return this.timeNeededToGrow;
        }

        public override bool Equals(object obj)
        {
            Plant plantObj = obj as Plant;
            if (plantObj == null)
                return false;
            else
                return base.Equals(obj) && this.growth.Equals(plantObj.growth) && this.timeNeededToGrow.Equals(plantObj.timeNeededToGrow);
        }
    }
}