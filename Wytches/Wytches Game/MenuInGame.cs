﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.GameOwn;
using Wytches.ReptileEngine;

namespace Wytches.Wytches_Game
{
    class MenuInGame : DrawableGameComponent
    {

        protected Game game;
        
        private Camera camera;
        public Vector2 menuPosition;
        private BasicEffect basicEffect;
        protected SpriteBatch spriteBatch;
        private Rectangle menuRectangle;
        private Texture2D inventoryBackground;

        public MenuInGame(Microsoft.Xna.Framework.Game game, Camera camera, Vector2 position) : base(game)
        {
            this.game = game;
            this.camera = camera;
            basicEffect = new BasicEffect(GraphicsDevice);
            spriteBatch = MySpriteBatch.returnSpriteBatch();
            //this.menuPosition = new Vector2(700, 250);
            this.menuPosition = position;
            inventoryBackground = MyContentManager.Load<Texture2D>("Objects\\kocspriteMenu");
            this.menuRectangle = new Rectangle((int)menuPosition.X ,(int)menuPosition.Y, inventoryBackground.Width/2, inventoryBackground.Height/2);
        }


        public override void Draw(GameTime gameTime)
        {
                Game.IsMouseVisible = true;
                GraphicsDevice.BlendState = BlendState.AlphaBlend;
                GraphicsDevice.DepthStencilState = DepthStencilState.None;
                GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
                GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

                basicEffect.World = Matrix.CreateScale(1, -1, 1);
                basicEffect.View = camera.view;
                basicEffect.Projection = camera.projection;
                //start drawing

                spriteBatch.Begin();

                DrawMenu();

                //end drawing
                spriteBatch.End();
        }

        private void DrawMenu()
        {
            //Color colorToUse = Color.White;                     //background color to use
           // Rectangle squareToDrawPosition = new Rectangle();   //the square to draw (local variable to avoid creating a new variable per square)

            Texture2D inventoryBackground = MyContentManager.Load<Texture2D>("Objects\\kocspriteMenu");
            spriteBatch.Draw(inventoryBackground, menuRectangle, null, Color.White);
        }

        public void Update()
        {
            
        }
}
}
