﻿using System;
using System.Runtime.Serialization;

namespace Wytches.GameOwn
{
    [Serializable]
    internal class NoOfferingsException : Exception
    {
        public NoOfferingsException()
        {
        }

        public NoOfferingsException(string message) : base(message)
        {
        }

        public NoOfferingsException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NoOfferingsException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}