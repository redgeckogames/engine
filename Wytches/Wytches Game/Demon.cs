﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.GameOwn
{
    public class Demon : Npc
    {
        public string Name { get; set; }

        public InteractionSet Interactions { get; set; }

        public WitchAttribiutes Attributes { get; set; }

        public Demon()
        {
        }

        public Demon(InteractionSet Interactions)
        {
            this.Interactions = Interactions;
        }

        public void Interact(string interactionName, object interactionParameter)
        {
            Interactions.GetInterAction(interactionName).performAction(this, interactionParameter);
        }
    }
}