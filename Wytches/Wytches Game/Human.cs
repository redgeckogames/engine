﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.GameOwn
{
    public class Human : Npc
    {
        public string Name { get; set; }

        public InteractionSet Interactions { get; set; }

        public WitchAttribiutes Attributes { get; set; }

        public FarmHouse Home { get; private set; }

        public Human()
        {
        }

        public void Interact(string interactionName, object interactionParameter)
        {
            Interactions.GetInterAction(interactionName).performAction(this, interactionParameter);
        }

        public void InhabitStructure(FarmHouse structure)
        {
            if (Home == null)
            {
                structure.AddNpcToTheStructure(this);
                this.Home = structure;
                structure.StructureDestroyed += OnHomeDestroyed;
            }
            else
            {
                throw new HomeAlreadySetException("This Human already Has a Home Set");
            }
        }

        private void OnHomeDestroyed(object sender, EventArgs e)
        {
            //TODO: flee from the village
        }
    }
}