﻿namespace Wytches.GameOwn
{
    public interface Farmable
    {
        void GrowSeed();

        Farmable Harvest();

        PlantGrowth HowMuchHasGrown();

        int getTimeNeededToGrow();
    }
}