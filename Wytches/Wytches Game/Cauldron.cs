﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Wytches.GameOwn
{
    public abstract class Cauldron
    {
        protected PotionFactory potionFactory;
        protected List<Item> ingredients;

        public List<Item> Ingredients
        {
            get
            {
                return this.ingredients;
            }
        }

        public Cauldron(PotionFactory potionFactory)
        {
            this.potionFactory = potionFactory;
            ingredients = new List<Item>();
        }

        public abstract Potion BrewPotion();

        public abstract void AddIngedient(Item ingredient);

        protected abstract bool IsCauldronFull();
    }
}