﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    public enum AnimationStateName
    {
        NotLoaded,
        Idle,
        Walking,
        Running,
        Attacking,
        Chopping
    }
}