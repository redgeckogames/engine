﻿using System.Runtime.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Wytches.ReptileEngine
{
    [DataContract]
    public class Camera : Behaviour
    {
        public Matrix projection { get; private set; }
        public Matrix view { get; set; }
        public Vector3 Position;

        public Vector3 startMove { get; private set; }

        public Camera(GameObject gameObject) : base(gameObject)
        {
            startMove = new Vector3(0, 60, -70);
            projection = Game1.projection;
            Position = new Vector3(gameObject.Transform.Position.X + startMove.X, gameObject.Transform.Position.Y + startMove.Y, gameObject.Transform.Position.Z + startMove.Z);
            view = Matrix.CreateLookAt(Position, Vector3.Forward, Vector3.Up);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}