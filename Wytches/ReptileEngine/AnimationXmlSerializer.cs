﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Wytches.ReptileEngine
{
    public class AnimationXmlSerializer<T> : ISerializer<T> where T : class, new()
    {
        private XmlSerializer serializer;
        public AnimationXmlSerializer()
        {
            serializer = new XmlSerializer(typeof(T));
        }
        public T ReadObject(string fileName)
        {
            T deserializedObject;

            using (FileStream fs = new FileStream(fileName + ".xml", FileMode.Open))
            {
                deserializedObject = (T)serializer.Deserialize(fs);
            }
            return deserializedObject;
        }

        public void WriteObject(string fileName, T Object)
        {
            if (fileName == null)
                throw new ArgumentNullException(nameof(fileName));
            if (Object == null)
                throw new ArgumentNullException(nameof(Object));
            using (FileStream fs = new FileStream(fileName + ".xml", FileMode.Create))
            {
                serializer.Serialize(fs, Object);
            }
        }
    }
}
