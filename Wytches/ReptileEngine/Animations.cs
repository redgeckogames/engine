﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Wytches.ReptileEngine
{
    [XmlRoot(ElementName = "Animations")]
    public class Animations
    {
        [XmlElement(ElementName = "GameObject")]
        public List<GameObjectAnimations> gameObjectAnimation { get; set; }
    }
}