﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches
{
    public enum Space
    {
        World,
        Self
    }

    public enum ScenesEnum
    {
        Logo,
        MainMenu,
        GameScene,
        Victory,
        GameOver,
        End
    }
}