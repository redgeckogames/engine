﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Wytches.ReptileEngine.ParticlesNew
{
    class ParticleRenderer
    {
        // Global particle renderer
        private static ParticleRenderer instance;
        public static ParticleRenderer Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new ParticleRenderer();
                }
                return instance;
            }
        }

        private Random random;
        private List<Particle> particles;

        private List<Texture2D> textures;
        public bool particlesActive;
        public float particlesLife;

        private ParticleRenderer()
        {
            particles = new List<Particle>();
            random = new Random();
            this.textures = new List<Texture2D>();
            textures.Add(MyContentManager.Load<Texture2D>("Objects\\Particles\\circle"));
            textures.Add(MyContentManager.Load<Texture2D>("Objects\\Particles\\star"));
            textures.Add(MyContentManager.Load<Texture2D>("Objects\\Particles\\diamond"));
            particlesActive = false;
        }

        public void addParticle(Vector3 position, Texture2D texture, TimeSpan creationTime)
        {
            particles.Add(new Particle(position, textures[1], creationTime, random));
        }

        public void Update(GameTime gameTime, Camera mainCam)
        {
            for (int i = particles.Count - 1; i >= 0; i--)
            {
                if (particles[i].isAlive) particles[i].Update(gameTime.TotalGameTime);
                else particles.RemoveAt(i);
            }
            //this.Draw(mainCam);
        }

        public void StartParticles(GameObject witchPlayer, Camera mainCam, GameTime gameTime, float life, int tex)
        {
           
            //if (particlesActive)
            //{
                for (int i = 0; i < 500; i++)
                {
                    addParticle(witchPlayer.Transform.Position, textures[tex], gameTime.TotalGameTime);
                }
           // }
            //this.Draw(mainCam);
            //this.Update(gameTime, mainCam);
        }

        public void Draw(Camera mainCam)
        {
            GraphicsDevice graphicsDevice = Game1.graphics.GraphicsDevice;
            Camera camera = mainCam;

            Matrix invertY = Matrix.CreateScale(1, -1, 1);

            BasicEffect effect = new BasicEffect(graphicsDevice);
            effect.View = camera.view;
            effect.Projection = camera.projection;
            effect.TextureEnabled = true;

            VertexPositionTexture[] verts = new VertexPositionTexture[4];
            verts[0].TextureCoordinate = new Vector2(0.0f, 1.0f);
            verts[1].TextureCoordinate = new Vector2(0.0f, 0.0f);
            verts[2].TextureCoordinate = new Vector2(1.0f, 1.0f);
            verts[3].TextureCoordinate = new Vector2(1.0f, 0.0f);

            particles.Sort();

            foreach (Particle i in particles)
            {
                effect.Texture = i.texture;
                //effect.DiffuseColor = new Vector3(i.color.R, i.color.G, i.color.B);
                effect.Alpha = i.alpha;
                Vector3 direction = Vector3.Subtract(camera.Position, i.position);
                i.distanceToCamera = direction.Length();
                direction.Normalize();
                Vector3 t1 = new Vector3(1f, 1f, (-direction.X - direction.Y) / direction.Z);
                t1.Normalize();
                Vector3 t2 = Vector3.Cross(direction, t1);
                t2.Normalize();
                t1 = Vector3.Multiply(t1, i.halfSize);
                t2 = Vector3.Multiply(t2, i.halfSize);

                verts[0].Position = Vector3.Add(i.position, -t1);
                verts[1].Position = Vector3.Add(i.position, t2);
                verts[2].Position = Vector3.Add(i.position, -t2);
                verts[3].Position = Vector3.Add(i.position, t1);

                foreach (EffectPass pass in effect.CurrentTechnique.Passes)
                {
                    pass.Apply();

                    graphicsDevice.DrawUserPrimitives(
                        PrimitiveType.TriangleStrip,
                        verts,
                        0,
                        2
                    );
                }
            }
        }
    }
}
