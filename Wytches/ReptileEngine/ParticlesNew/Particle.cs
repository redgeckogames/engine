﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Wytches.ReptileEngine.ParticlesNew
{
    public class Particle : IComparable
    {
        public Vector3 position;
        public bool isAlive;
        public TimeSpan creationTime;
        public TimeSpan lifespan;
        public TimeSpan removeTime;
        public Vector3 translation;
        public float maxSpeed;
        public float acceleration;
        public float alpha;
        public float distanceToCamera;
        public float halfSize;
        public bool decay;
        public Texture2D texture;
        public Color color;
        public double directionX;
        public double directionZ;

        public Particle(Vector3 position, Texture2D texture, TimeSpan creationTime, Random random)
        {
            this.position = position;
            this.creationTime = creationTime;
            lifespan = new TimeSpan(0, 0, 0, 0, random.Next(500, 900));
            removeTime = creationTime + lifespan;
            isAlive = true;
            maxSpeed = 15.0f;
            halfSize = ((float)random.NextDouble() * 0.7f) + 0.2f;
            alpha = 1.0f;
            decay = false;
            acceleration = ((float)random.NextDouble() * 0.1f) + 0.5f;
            directionX = (float)random.NextDouble()*2 -1;
            if (directionX < 0)
                directionX = -1;
            else
            {
                directionX = 1;
            }
            directionZ = (float)random.NextDouble() * 2 - 1;
            if (directionZ < 0)
                directionZ = -1;
            else
            {
                directionZ = 1;
            }
            translation = new Vector3((float)random.NextDouble() * maxSpeed * (float)directionX, (float)random.NextDouble() * maxSpeed + 1f, (float)random.NextDouble() * maxSpeed * (float)directionZ);
            this.texture = texture;
            this.color = new Color(
                        (float)random.NextDouble(),
                        (float)random.NextDouble(),
                        (float)random.NextDouble());
        }

        public void Update(TimeSpan totalGameTime)
        {
            if (removeTime < totalGameTime)
            {
                isAlive = false;
                return;
            }
            position += translation;
            translation *= acceleration;
            if (decay)
                alpha = (float)((removeTime - totalGameTime).TotalMilliseconds / lifespan.TotalMilliseconds);
        }

        public int CompareTo(object obj)
        {
            if (obj == null) return 1;
            Particle otherParticle = obj as Particle;
            return otherParticle.distanceToCamera.CompareTo(this.distanceToCamera);
        }
    }
}
