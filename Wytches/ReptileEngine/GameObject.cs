﻿//using AgileObjects.AgileMapper;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using Wytches.Scripts;

namespace Wytches.ReptileEngine
{
    [DataContract(IsReference = true, Name = "GameObject")]

    #region KnownTypes

    [KnownType(typeof(Colider))]
    [KnownType(typeof(Behaviour))]
    [KnownType(typeof(Camera))]
    [KnownType(typeof(Renderer))]
    [KnownType(typeof(MeshRenderer))]
    [KnownType(typeof(Script))]
    [KnownType(typeof(Colider))]
    [KnownType(typeof(Transform))]
    [KnownType(typeof(Light))]
    [KnownType(typeof(PathComponent))]
    [KnownType(typeof(AnimationComponents.Animation))]
    [KnownType(typeof(Scripts.InventoryScript))]

    #endregion KnownTypes

    public class GameObject : IDisposable
    {
        [DataMember(Name = "Components")]
        private List<Component> components;

        [DataMember]
        public string Tag { get; set; } = "";

        [DataMember]
        public bool isActive { get; set; } = true;

        [DataMember]
        public Transform Transform { get; private set; }

        [DataMember]
        public bool IsDynamic { get; set; }

        [DataMember]
        public Scene Scene { get; set; }

        public event EventHandler<GameObjectEventArgs> collisionHandler;

        public event EventHandler<GameObjectEventArgs> CollisionExit;

        public bool Forward { get; set; }
        public bool Backward { get; set; }
        public bool Right { get; set; }
        public bool Left { get; set; }
        public bool collisionExit { get; set; } = false;

        public GameObject()
        {
            components = new List<Component>();
            Transform transform = new Transform(this);

            Transform = transform;
            components.Add(transform);
        }

        public GameObject(Vector3 position)
        {
            components = new List<Component>();
            Transform transform = new Transform(this, position);

            Transform = transform;
            components.Add(transform);
        }

        public GameObject(Vector3 position, Vector3 rotationVector3, float angle)
        {
            components = new List<Component>();
            Transform transform = new Transform(this, position, rotationVector3, angle);

            Transform = transform;
            components.Add(transform);
        }

        public GameObject(GameObject other)
        {
            components = new List<Component>();
            Transform transform = new Transform(this);

            Transform = transform;
            components.Add(transform);
            this.isActive = other.isActive;
            this.Tag = other.Tag;

            foreach (var c in other.components)
            {
                if (c.GetType() != typeof(Transform) && c.GetType() != typeof(Colider))
                    this.components.Add((Activator.CreateInstance(c.GetType(), other) as Component));
                else if (c.GetType() == typeof(Colider))
                {
                    Activator.CreateInstance(c.GetType(), this);
                }
            }
        }

        public T AddComponent<T>() where T : Component
        {
            if (ContainsComponent<T>() == true)
                throw new ComponentAlreadyAddedException("This Object Already Contains " + typeof(T).FullName + " Component");

            T component = Activator.CreateInstance(typeof(T), this) as T;

            components.Add(component);

            return component;
        }

        private void AddComponent(Type component)
        {
            var comp = Activator.CreateInstance(component, this) as Component;
            components.Add(comp);
        }

        public bool ContainsComponent<T>()
        {
            if (components?.Find(c => c.GetType() == typeof(T)) != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public T GetComponent<T>() where T : Component
        {
            if (ContainsComponent<T>() == false)
                throw new ComponentNotFoundException("Component of type " + typeof(T).FullName + " is not attached to this GameObject");

            return components.Find(component => component.GetType().Equals(typeof(T))) as T;
        }

        public static GameObject Instantiate(GameObject originalObject, Vector3 position, Vector3 rotationVector3, float angle)
        {
            GameObject instatiatedObject = new GameObject(originalObject);
            //GameObject instatiatedObject = Clone(originalObject);
            instatiatedObject.Transform.Translate(position);
            instatiatedObject.Transform.Rotate(rotationVector3, angle);

            return instatiatedObject;
        }

        public void Update(GameTime gameTime)
        {
            if (components != null)
            {
                foreach (Component component in components)
                {
                    component.Update(gameTime);
                }
            }
        }

        public void Draw(Camera camera)
        {
            if (this.ContainsComponent<MeshRenderer>())
            {
                GetComponent<MeshRenderer>().Draw(camera);
            }
            else if (this.ContainsComponent<AnimationComponents.Animation>())
            {
                GetComponent<AnimationComponents.Animation>().Draw(camera);
            }
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                GameObject gameObj = obj as GameObject;
                return gameObj.Tag == this.Tag && gameObj.components.Equals(this.components);
            }
        }

        // override object.GetHashCode
        public override int GetHashCode()
        {
            return this.components.GetHashCode() + Tag.GetHashCode() ^ 31;
        }

        internal void OnCollision(object sender, EventArgs e)
        {
        }

        public void OnObjectColided(object sender, CollisionEventArgs args)
        {
            if (args.GameObject.ContainsComponent<BoxCollider>() || args.GameObject.ContainsComponent<SphereCollider>())
            {
                if (args.GameObject.ContainsComponent<Colider>())
                {
                    if (args.GameObject.GetComponent<Colider>().IsTrigger)
                    {
                        Game1.ligthingObject.GetComponent<Transform>().Position = args.GameObject.GetComponent<Transform>().Position;
                    }
                }
                else
                {

                    if (Forward)
                    {
                        Transform.position.Z -= 0.5f;
                    }
                    else if (Backward)
                    {
                        Transform.position.Z += 0.5f;
                    }
                    else if (Right)
                    {
                        Transform.position.X += 0.5f;
                    }
                    else if (Left)
                    {
                        Transform.position.X -= 0.5f;
                    }
                }
                
            }

        }

        public void OnCollisionStay(GameObject other)
        {
            if (collisionHandler != null)
                collisionHandler(this, new GameObjectEventArgs { gameObject = other });
        }

        public void OnCllisionExit(GameObject other)
        {
            if (CollisionExit != null)
                CollisionExit(this, new GameObjectEventArgs { gameObject = other });
        }

       // public void OnTriggerStay(GameObject other)

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    Scene?.RemoveGameObject(this);
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.
                components.ForEach(c => c.Dispose());
                components = null;
                Transform = null;
                Scene = null;
                Tag = null;
                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        ~GameObject()
        {
            //     //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(false);
        }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}