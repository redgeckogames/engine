﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    public class ModelDictionary
    {
        public  Model Model { get; private set; }
        public Texture2D DiffuseTexture { get; private set; }
        public Texture2D NormalTexture { get; private set; }

        public ModelDictionary()
        {

        }

        public ModelDictionary(string pathModel, string pathDiffuseTexture, string pathNormalTexture)
        {
            Model = MyContentManager.Load<Model>(pathModel);
            DiffuseTexture = MyContentManager.Load<Texture2D>(pathDiffuseTexture);
            NormalTexture = MyContentManager.Load<Texture2D>(pathNormalTexture);
        }

        public ModelDictionary(string pathModel, string pathDiffuseTexture)
        {
            Model = MyContentManager.Load<Model>(pathModel);
            DiffuseTexture = MyContentManager.Load<Texture2D>(pathDiffuseTexture);
        }

        public void setModel(string path)
        {
            Model = MyContentManager.Load<Model>(path);
        }

        public void setDiffuseTexture(string path)
        {
            DiffuseTexture = MyContentManager.Load<Texture2D>(path);
        }

        public void setNormalTexture(string path)
        {
            NormalTexture = MyContentManager.Load<Texture2D>(path);
        }

    }
}
