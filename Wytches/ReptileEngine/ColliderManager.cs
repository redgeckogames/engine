﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.ReptileEngine.AnimationComponents;
using Wytches.ReptileEngine.EventSystem;
using Wytches.Scripts;

namespace Wytches.ReptileEngine
{
    internal class ColliderManager
    {
        private bool isCollision;
        private List<GameObject> gameObjectsOnScene;

        public event EventHandler<CollisionEventArgs> ObjectColided;

        private Matrix world1;
        private Matrix world2;

        private bool collisionExit = false;

        public ColliderManager(List<GameObject> gameObjectsOnScene)
        {
            isCollision = false;
            this.gameObjectsOnScene = (gameObjectsOnScene).Where(o => (!o.ContainsComponent<Light>())).ToList();
            //  lastModelPosition = new Vector3[gameObjectsOnScene.Count];
        }

        public void Update()
        {
            List<GameObject> dynamicObjects = (gameObjectsOnScene).Where(o => o.IsDynamic).ToList();

            for (int j = 0; j < dynamicObjects.Count; j++)
            {
                if (dynamicObjects[j].ContainsComponent<MeshRenderer>())
                {
                    world1 = dynamicObjects[j].GetComponent<MeshRenderer>().GetMatrix();
                }
                else
                {
                    world1 = dynamicObjects[j].GetComponent<AnimationComponents.Animation>().GetMatrix();
                }

                for (int k = 0; k < (gameObjectsOnScene).Count; k++)
                {
                    if (dynamicObjects[j] != gameObjectsOnScene[k])
                    {
                        if (gameObjectsOnScene[k].ContainsComponent<MeshRenderer>())
                        {
                            world2 = gameObjectsOnScene[k].GetComponent<MeshRenderer>().GetMatrix();
                        }
                        else if (gameObjectsOnScene[k].ContainsComponent<Animation>())
                        {
                            world2 = gameObjectsOnScene[k].GetComponent<Animation>().GetMatrix();
                        }

                        isCollision = PreciseCollisionTest(dynamicObjects[j], world1,
                         gameObjectsOnScene[k], world2);
                        if (isCollision)
                        {
                            OnObjectColided(gameObjectsOnScene[k]);
                        }
                    }
                }
            }
        }

        private bool OverallCollisionTest(GameObject object1, Matrix world1, GameObject object2, Matrix world2)
        {
            //    Debug.WriteLine("qqqqqqqqqqqqqqqqqq {0}", object1.ContainsComponent<SphereCollider>());
            
            if (object1.ContainsComponent<SphereCollider>() && object2.ContainsComponent<SphereCollider>())
            {
                BoundingSphere origSphere1 = object1.GetComponent<SphereCollider>().GetCompleteBoundingSphere();
                BoundingSphere sphere1 = Colider.TransformBoundingSphere(origSphere1, world1);

                BoundingSphere origSphere2 = object2.GetComponent<SphereCollider>().GetCompleteBoundingSphere();
                BoundingSphere sphere2 = Colider.TransformBoundingSphere(origSphere2, world2);

                bool collision = sphere1.Intersects(sphere2);
                if (collision)
                {
                    object1.collisionExit = true;
                    object1.OnCollisionStay(object2);
                    

                    //  Debug.WriteLine("Overall collision");
                }
                else if(object1.collisionExit)
                {
                    object1.collisionExit = false;
                    object1.OnCllisionExit(object2);
                    //Debug.WriteLine("nope");
                }
                return collision;
            }
            else if (object1.ContainsComponent<BoxCollider>() && object2.ContainsComponent<BoxCollider>())
            {
                BoundingSphere origSphere1 = object1.GetComponent<BoxCollider>().GetCompleteBoundingSphere();
                BoundingSphere sphere1 = Colider.TransformBoundingSphere(origSphere1, world1);

                BoundingSphere origSphere2 = object2.GetComponent<BoxCollider>().GetCompleteBoundingSphere();
                BoundingSphere sphere2 = Colider.TransformBoundingSphere(origSphere2, world2);

                bool collision = sphere1.Intersects(sphere2);
                if (collision)
                {
                    object2.collisionExit = true;
                    object1.OnCollisionStay(object2);


                    //  Debug.WriteLine("Overall collision");
                }
                else if (object2.collisionExit && ( object2.Tag.Equals("Cauldron") || object2.Tag.Equals("Alter") ))
                {
                    object2.collisionExit = false;
                    object1.OnCllisionExit(object1);
                    //Debug.WriteLine("nope");
                }

                return collision;
            }
            else if (object1.ContainsComponent<BoxCollider>() && object2.ContainsComponent<SphereCollider>())
            {
                BoundingSphere origSphere1 = object1.GetComponent<BoxCollider>().GetCompleteBoundingSphere();
                BoundingSphere sphere1 = Colider.TransformBoundingSphere(origSphere1, world1);

                BoundingSphere origSphere2 = object2.GetComponent<SphereCollider>().GetCompleteBoundingSphere();
                BoundingSphere sphere2 = Colider.TransformBoundingSphere(origSphere2, world2);

                bool collision = sphere1.Intersects(sphere2);
                if (collision)
                {
                    object1.OnCollisionStay(object2);
                    //Debug.WriteLine("Overall collision");
                }
                else
                {
                    // Debug.WriteLine("nope");
                }

                return collision;
            }
            else if (object1.ContainsComponent<SphereCollider>() && object2.ContainsComponent<BoxCollider>())
            {
                BoundingSphere origSphere1 = object1.GetComponent<SphereCollider>().GetCompleteBoundingSphere();
                BoundingSphere sphere1 = Colider.TransformBoundingSphere(origSphere1, world1);

                BoundingSphere origSphere2 = object2.GetComponent<BoxCollider>().GetCompleteBoundingSphere();
                BoundingSphere sphere2 = Colider.TransformBoundingSphere(origSphere2, world2);

                bool collision = sphere1.Intersects(sphere2);
                if (collision)
                {
                    object1.OnCollisionStay(object2);
                    //Debug.WriteLine("Overall collision");
                }
                else
                {
                    //Debug.WriteLine("nope");
                }

                return collision;
            }

            return false;
        }

        private bool PreciseCollisionTest(GameObject object1, Matrix world1, GameObject object2, Matrix world2)
        {
            //jeżeli jest kolizaj to mam to wysłać to notifier

            if (OverallCollisionTest(object1, world1, object2, world2) == false)
                return false;
            //Debug.WriteLine("TTRRRUUEEE");

            if (object1.ContainsComponent<SphereCollider>() && object2.ContainsComponent<SphereCollider>())
            {
                BoundingSphere[] model1Spheres = calulateModelSphere(object1, world1);
                BoundingSphere[] model2Spheres = calulateModelSphere(object2, world2);

                bool collision = false;

                for (int i = 0; i < model1Spheres.Length; i++)
                    for (int j = 0; j < model2Spheres.Length; j++)
                        if (model1Spheres[i].Intersects(model2Spheres[j]))
                        {
                            //  Debug.WriteLine("Precise collision SS");
                            return true;
                        }

                return collision;
            }
            else if (object1.ContainsComponent<BoxCollider>() && object2.ContainsComponent<BoxCollider>())
            {
                BoundingBox[] object1Colliders = calulateModelBox(object1, world1);
                BoundingBox[] object2Colliders = calulateModelBox(object2, world2);

                bool collision = false;

                for (int i = 0; i < object1Colliders.Length; i++)
                    for (int j = 0; j < object2Colliders.Length; j++)
                        if (object1Colliders[i].Intersects(object2Colliders[j]))
                        {
                            //  Debug.WriteLine("Precise collision BB");
                            return true;
                        }

                return collision;
            }
            else if (object1.ContainsComponent<SphereCollider>() && object2.ContainsComponent<BoxCollider>())
            {
                //BoundingSphere[] object1Colliders = object1.GetComponent<SphereCollider>().GetPreciseBoundingSpheres();

                BoundingSphere[] model1Spheres = calulateModelSphere(object1, world1);
                BoundingBox[] object2Colliders = calulateModelBox(object2, world2);

                bool collision = false;

                for (int i = 0; i < model1Spheres.Length; i++)
                    for (int j = 0; j < object2Colliders.Length; j++)
                        if (model1Spheres[i].Intersects(object2Colliders[j]))
                        {
                            //  Debug.WriteLine("Precise collision SB");
                            return true;
                        }

                return collision;
            }
            else if (object1.ContainsComponent<BoxCollider>() && object2.ContainsComponent<SphereCollider>())
            {
                //BoundingSphere[] object1Colliders = object1.GetComponent<SphereCollider>().GetPreciseBoundingSpheres();

                BoundingSphere[] model2Spheres = calulateModelSphere(object2, world2);
                BoundingBox[] object1Colliders = calulateModelBox(object1, world1);

                bool collision = false;

                for (int i = 0; i < object1Colliders.Length; i++)
                    for (int j = 0; j < model2Spheres.Length; j++)
                        if (object1Colliders[i].Intersects(model2Spheres[j]))
                        {
                            //  Debug.WriteLine("Precise collision BS");
                            return true;
                        }

                return collision;
            }

            return false;
        }

        private BoundingSphere[] calulateModelSphere(GameObject object1, Matrix world1)
        {
            Matrix[] model1Transforms;
            ModelMesh mesh;
            BoundingSphere[] modelSpheres;

            if (object1.ContainsComponent<MeshRenderer>())
            {
                model1Transforms = new Matrix[object1.GetComponent<MeshRenderer>().Model.Bones.Count];
                object1.GetComponent<MeshRenderer>().Model.CopyAbsoluteBoneTransformsTo(model1Transforms);
                modelSpheres = new BoundingSphere[object1.GetComponent<MeshRenderer>().Model.Meshes.Count];
                for (int i = 0; i < object1.GetComponent<MeshRenderer>().Model.Meshes.Count; i++)
                {
                    mesh = object1.GetComponent<MeshRenderer>().Model.Meshes[i];
                    BoundingSphere origSphere = mesh.BoundingSphere;
                    Matrix trans = model1Transforms[mesh.ParentBone.Index] * world1;
                    BoundingSphere transSphere = Colider.TransformBoundingSphere(origSphere, trans);
                    modelSpheres[i] = transSphere;
                }
            }
            else
            {
                model1Transforms = new Matrix[object1.GetComponent<AnimationComponents.Animation>().Model.Bones.Count];
                object1.GetComponent<AnimationComponents.Animation>().Model.CopyAbsoluteBoneTransformsTo(model1Transforms);
                modelSpheres = new BoundingSphere[object1.GetComponent<AnimationComponents.Animation>().Model.Meshes.Count];
                for (int i = 0; i < object1.GetComponent<AnimationComponents.Animation>().Model.Meshes.Count; i++)
                {
                    mesh = object1.GetComponent<AnimationComponents.Animation>().Model.Meshes[i];
                    BoundingSphere origSphere = mesh.BoundingSphere;
                    Matrix trans = model1Transforms[mesh.ParentBone.Index] * world1;
                    BoundingSphere transSphere = Colider.TransformBoundingSphere(origSphere, trans);
                    modelSpheres[i] = transSphere;
                }
            }
            return modelSpheres;
        }

        public BoundingBox[] calulateModelBox(GameObject object1, Matrix world1)
        {
            BoundingBox[] boxes;
            BoundingBox[] boundingBoxes = object1.GetComponent<BoxCollider>().GetPreciseBoundingBoxes();

            if (object1.ContainsComponent<MeshRenderer>())
            {
                boxes = new BoundingBox[object1.GetComponent<MeshRenderer>().Model.Meshes.Count];
                for (int i = 0; i < object1.GetComponent<MeshRenderer>().Model.Meshes.Count; i++)
                {
                    boxes[i] = CheckMinMax(boundingBoxes[i].Min, boundingBoxes[i].Max);
                }
            }
            else
            {
                boxes = new BoundingBox[object1.GetComponent<AnimationComponents.Animation>().Model.Meshes.Count];
                for (int i = 0; i < object1.GetComponent<AnimationComponents.Animation>().Model.Meshes.Count; i++)
                {
                    boxes[i] = CheckMinMax(boundingBoxes[i].Min, boundingBoxes[i].Max);
                }
            }
            //   Debug.WriteLine(boxes[0].Min + "  " + boxes[0].Max);
            return boxes;
        }

        public BoundingBox CheckMinMax(Vector3 min, Vector3 max)
        {
            Vector3 temp;
            temp = min;
            if (temp.X > max.X)
            {
                min.X = max.X;
                max.X = temp.X;
            }
            if (temp.Y > max.Y)
            {
                min.Y = max.Y;
                max.Y = temp.Y;
            }
            if (temp.Z > max.Z)
            {
                min.Z = max.Z;
                max.Z = temp.Z;
            }
            //    Debug.WriteLine("po zamianie" + min + " " + max);
            return new BoundingBox(min, max);
        }

        protected virtual void OnObjectColided(GameObject gameObject)
        {
            if (ObjectColided != null)
                ObjectColided(this, new CollisionEventArgs() { GameObject = gameObject });
        }
    }
}