﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.EventSystem
{
    public interface Notifier<T, E> where T : class
    {
        void Notify(T recipient, E notificationArgument);
    }
}