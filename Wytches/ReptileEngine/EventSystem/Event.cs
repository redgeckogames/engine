﻿using System;

namespace Wytches.ReptileEngine.EventSystem
{
    public class Event<EventArgs>
    {
        public string Name { get; set; }
        public EventArgs args { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                Event<EventArgs> otherEvent = obj as Event<EventArgs>;
                return this.Name.Equals(otherEvent.Name);
            }
        }

        public override int GetHashCode()
        {
            return Name.GetHashCode();
        }
    }
}