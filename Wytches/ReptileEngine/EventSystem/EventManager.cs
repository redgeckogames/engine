﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.EventSystem
{
    public class EventManager
    {
        private static Dictionary<string, Action> eventList;
        

        public Dictionary<string, Action> EventList
        {
            get
            {
                return eventList;
            }
        }

        public EventManager()
        {
            eventList = new Dictionary<string, Action>();
        }

        public static void PublishEvent(string eventName, Action action)
        {
            eventList.Add(eventName, action);
        }

        public static void TriggerEvent(string eventName)
        {
            Action action;
            if (eventList.TryGetValue(eventName, out action) == true)
            {
                action?.Invoke();
            }
            else
            {
                throw new EventNotFoundException("Event with a name " + eventName
                    + " was not found, make sure to publish event first using PublishEvent method");
            }
        }
    }
}