﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.EventSystem
{
    public class EventNotFoundException : ReptileRuntimeException
    {
        public EventNotFoundException(string message) : base(message)
        {
        }
    }
}