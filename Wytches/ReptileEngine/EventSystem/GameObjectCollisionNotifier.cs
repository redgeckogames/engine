﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.EventSystem
{
    public class GameObjectCollisionNotifier : Notifier<GameObject, GameObject>
    {
        private Dictionary<GameObject, Action<GameObject>> dynamicObjectsOnScene;

        public GameObjectCollisionNotifier(List<GameObject> dynamicGamObjectOnScene)
        {
            dynamicObjectsOnScene = new Dictionary<GameObject, Action<GameObject>>();

            dynamicGamObjectOnScene
                .ForEach(obj => this.dynamicObjectsOnScene.Add(obj, obj.OnCollisionStay));
        }

        public void Notify(GameObject recipient, GameObject notificationArgument)
        {
            Action<GameObject> collisionNotification;
            if (dynamicObjectsOnScene.TryGetValue(recipient, out collisionNotification) == true)
            {
                collisionNotification?.Invoke(notificationArgument);
            }
        }
    }
}