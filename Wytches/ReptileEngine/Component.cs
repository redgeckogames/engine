﻿using System;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using Microsoft.Xna.Framework;
using Newtonsoft.Json;

namespace Wytches.ReptileEngine
{
    [DataContract(IsReference = true)]
    [KnownType(typeof(Scripts.InventoryScript))]
    [Serializable]
    public class Component : ICloneable, IDisposable
    {
        protected GameObject gameObject;

        [DataMember(Name = "GameObject")]
        public GameObject GameObject
        {
            get { return gameObject; }
            set { gameObject = value; }
        }

        protected Component()
        {
        }

        public Component(GameObject gameObject)
        {
            this.gameObject = gameObject;
        }

        public Component(Component component, GameObject gameObject)
            : this(gameObject)
        {
        }

        public virtual void OnLoad()
        {
        }

        public virtual void Update(GameTime gameTime)
        {
        }

        public virtual void Draw(Camera camera)
        {
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            return true;
        }

        public override int GetHashCode()
        {
            return base.GetHashCode();
        }

        public virtual object Clone()
        {
            MemoryStream ms = new MemoryStream();
            BinaryFormatter formatter = new BinaryFormatter();
            formatter.Serialize(ms, this);
            object clone = formatter.Deserialize(ms);
            return clone;
        }

        #region IDisposable Support

        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~Component() {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }

        #endregion IDisposable Support
    }
}