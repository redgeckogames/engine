﻿using Microsoft.Xna.Framework;
using NLog;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using Wytches.ReptileEngine.AStar;

namespace Wytches.ReptileEngine
{
    [DataContract]
    internal class PathComponent : Component
    {
        private PathFinder pathFinder;
        private Vector3 start;
        private Rectangle end;
        private bool diagonalsEnabled;
        private float estimatedDistanceWeighting;
        private Timer timer;
        private Queue<Vector3> queue;
        private bool goPath;
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();

        public event EventHandler DestinationReached;

        public bool GoPath
        {
            get
            {
                return goPath;
            }
        }

        public PathComponent(GameObject gameObject) : base(gameObject)
        {
            queue = new Queue<Vector3>();
            this.goPath = false;
            diagonalsEnabled = true;
            estimatedDistanceWeighting = 5;
        }

        public void SetDestination(GameObject end)
        {
            this.start = gameObject.GetComponent<Transform>().Position;

            Vector3 min = Vector3.Zero, max = Vector3.Zero;
            if (end.ContainsComponent<BoxCollider>())
            {
                BoundingBox[] boxes = new BoundingBox[end.GetComponent<MeshRenderer>().Model.Meshes.Count];
                BoundingBox[] boundingBoxes = end.GetComponent<BoxCollider>().GetPreciseBoundingBoxes();
                for (int i = 0; i < end.GetComponent<MeshRenderer>().Model.Meshes.Count; i++)
                {
                    boxes[i] = CheckMinMax(boundingBoxes[i].Min, boundingBoxes[i].Max);
                }

                foreach (BoundingBox bbox in boxes)
                {
                    min = bbox.Min; max = bbox.Max;
                    if (min.X > bbox.Min.X && min.Y > bbox.Min.Y && min.Z > bbox.Min.Z)
                    {
                        min = bbox.Min;
                    }

                    if (max.X < bbox.Max.X && max.Y < bbox.Max.Y && max.Z < bbox.Max.Z)
                    {
                        max = bbox.Max;
                    }
                }
            }
            this.end = new Rectangle((int)min.X - 1, (int)min.Z - 1,
               (int)(max.X - min.X) + 2, (int)(max.Z - min.Z) + 2);

            pathFinder = new PathFinder(this.start, this.end, diagonalsEnabled, estimatedDistanceWeighting);

            pathFinder.map.SetWall(this.gameObject, Game1.scene.GetAllGameObjects());

            timer = new Timer(100);
            timer.Elapsed += OnTimedEvent;
            timer.AutoReset = true;
            timer.Start();
        }

        public void FindPath()
        {
            do
            {
                pathFinder.ConsiderMove();
            }
            while (!pathFinder.PathFound);

            if (pathFinder.PathFound)
            {
                queue = new Queue<Vector3>();
                foreach (var move in pathFinder.Path)
                {
                    queue.Enqueue(move);
                }
            }
        }

        public void StartGoPath()
        {
            this.goPath = true;
        }

        private void OnTimedEvent(object sender, ElapsedEventArgs e)
        {
            if (pathFinder.PathFound == true && this.goPath == true)
            {
                if (queue.Count > 0)
                {
                    if (gameObject.ContainsComponent<Transform>())
                    {
                        try
                        {
                            Vector3 temp = queue.Dequeue();
                            gameObject.GetComponent<Transform>().Position = new Vector3(temp.X, gameObject.GetComponent<Transform>().Position.Y, temp.Z);
                        }
                        catch (InvalidOperationException operationEx)
                        {
                            logger.Error(operationEx, "tried to perform Dequeue");
                        }
                        catch (ArgumentOutOfRangeException queueException)
                        {
                            logger.Error(queueException, "just in case something werid happened");
                        }
                    }
                }
                else
                {
                    OnDestinationReached();
                }
            }
        }

        protected virtual void OnDestinationReached()
        {
            DestinationReached?.Invoke(this, EventArgs.Empty);
        }

        protected override void Dispose(bool disposing)
        {
            timer?.Dispose();
            base.Dispose(disposing);
        }

        public BoundingBox CheckMinMax(Vector3 min, Vector3 max)
        {
            Vector3 temp;
            temp = min;
            if (temp.X > max.X)
            {
                min.X = max.X;
                max.X = temp.X;
            }
            if (temp.Y > max.Y)
            {
                min.Y = max.Y;
                max.Y = temp.Y;
            }
            if (temp.Z > max.Z)
            {
                min.Z = max.Z;
                max.Z = temp.Z;
            }
            //    Debug.WriteLine("po zamianie" + min + " " + max);
            return new BoundingBox(min, max);
        }
    }
}