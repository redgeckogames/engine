﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    public class JsonSerializer<T> : ISerializer<T> where T : class, new()
    {
        public T ReadObject(string fileName)
        {
            T obj = JsonConvert.DeserializeObject<T>(File.ReadAllText(fileName),
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All,
                    ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                    // ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

            return obj;
        }

        public void WriteObject(string filename, T Object)
        {
            string serializeWithJson = JsonConvert.SerializeObject(Object, Formatting.Indented,
                new JsonSerializerSettings
                {
                    TypeNameHandling = TypeNameHandling.All,
                    // ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    ConstructorHandling = ConstructorHandling.AllowNonPublicDefaultConstructor,
                    PreserveReferencesHandling = PreserveReferencesHandling.All
                });

            using (StreamWriter file = File.CreateText(filename))
            {
                file.WriteLine(serializeWithJson);
            }
        }
    }
}