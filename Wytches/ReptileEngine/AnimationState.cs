﻿using Microsoft.Xna.Framework.Graphics;
using NLog;
using System;
using System.Collections.Generic;
using System.IO;
using System.IO.MemoryMappedFiles;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.ReptileEngine.AnimationComponents;

namespace Wytches.ReptileEngine
{
    public class AnimationState
    {
        public static Animations animations = new Animations();
        private static Logger logger = LogManager.GetCurrentClassLogger();

        public AnimationState()
        {
        }

        public static void LoadAnimationForGameObject(GameObject gameObject, AnimationStateName animationStateName)
        {
            if (!isGameObjectAnimationStateName(gameObject, animationStateName))
            {
                string animationPath = FindAnimationPathForGameObject(gameObject, animationStateName);
                gameObject.GetComponent<Animation>().animationStateName = animationStateName;
                logger.Info("animation Path found " + animationPath);
                gameObject.GetComponent<Animation>().Model = MyContentManager.Load<Model>(animationPath);

                gameObject.GetComponent<Animation>().LoadAnimation("Take 001");
            }
        }

        private static bool isGameObjectAnimationStateName(GameObject gameObject, AnimationStateName animationstateName)
        {
            return gameObject.GetComponent<Animation>().animationStateName.Equals(animationstateName);
        }

        public static string FindAnimationPathForGameObject(GameObject gameObject, AnimationStateName animationState)
        {
            var animationPath = animations.gameObjectAnimation
                .Where(g => g.Tag.Equals(gameObject.Tag))
                .Select(a => a.listAnimation.Where(x => x.Name == animationState.ToString()).FirstOrDefault())
                .Select(p => p.Path)
                .First();

            return animationPath;
        }

        public static void LoadAnimationsFromTheFile(string filePath)
        {
            AnimationXmlSerializer<Animations> serializer = new AnimationXmlSerializer<Animations>();
            AnimationState.animations = serializer.ReadObject(filePath);
        }
    }
}