﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{

    public class Billboard
    {
        public const float BILLBOARD_SIZE = 128.0f;

        public Texture2D texture;
        public VertexBuffer vertexBuffer;
        public IndexBuffer indexBuffer;
        GraphicsDevice GraphicsDevice;

        public Billboard(GraphicsDevice GraphicsDevice, float width, float height, Vector3 position, Texture2D texture)
        {
            this.GraphicsDevice = GraphicsDevice;
            this.texture = texture;
            InitBillboard(width, height, position);

        }


        private void InitBillboard(float width, float height, Vector3 position)
        {
            float x = width * 0.5f;
            float y = height * 0.5f;

            Vector3 upperLeft = new Vector3(-x, y, 30.0f);
            Vector3 upperRight = new Vector3(x, y, 30.0f);
            Vector3 lowerLeft = new Vector3(-x, -y, 0.0f);
            Vector3 lowerRight = new Vector3(x, -y, 0.0f);

            VertexPositionTexture[] vertices =
            {
                new VertexPositionTexture(position + upperLeft,  new Vector2(0.0f, 0.0f)),  // 0
                new VertexPositionTexture(position + upperRight, new Vector2(1.0f, 0.0f)),  // 1
                new VertexPositionTexture(position + lowerLeft,  new Vector2(0.0f, 1.0f)),  // 2
                new VertexPositionTexture(position + lowerRight, new Vector2(1.0f, 1.0f)),  // 3
            };

            vertexBuffer = new VertexBuffer(GraphicsDevice, typeof(VertexPositionTexture), vertices.Length, BufferUsage.WriteOnly);
            vertexBuffer.SetData(vertices);

            short[] indices =
            {
                0, 1, 2,
                2, 1, 3
            };

            indexBuffer = new IndexBuffer(GraphicsDevice, IndexElementSize.SixteenBits, indices.Length, BufferUsage.WriteOnly);
            indexBuffer.SetData(indices);
        }

        public void DrawBillboard(Camera camera, Effect effect)
        {
            // Enable alpha blending
            GraphicsDevice.BlendState = BlendState.AlphaBlend;

            GraphicsDevice.SetVertexBuffer(vertexBuffer);
            GraphicsDevice.Indices = indexBuffer;

            RasterizerState prevRasterizerState = GraphicsDevice.RasterizerState;
            BlendState prevBlendState = GraphicsDevice.BlendState;

            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            // GraphicsDevice.BlendState = BlendState.NonPremultiplied;
            GraphicsDevice.RasterizerState = RasterizerState.CullNone;

   
            // Create a matrix that will rotate the billboard so that it will
            // always face the camera.

            Matrix billboardMatrix = Matrix.CreateConstrainedBillboard(Vector3.Zero,
                camera.startMove, Vector3.Up, new Vector3(0,0,0), Vector3.UnitZ);

            effect.Parameters["WorldMatrix"].SetValue(Matrix.Identity);
            effect.Parameters["ViewMatrix"].SetValue(camera.view);
            effect.Parameters["ProjectionMatrix"].SetValue(camera.projection);
            effect.Parameters["ModelTexture"].SetValue(texture);
            

            foreach (EffectPass pass in effect.CurrentTechnique.Passes)
            {
                pass.Apply();

                GraphicsDevice.DrawIndexedPrimitives(PrimitiveType.TriangleList,
                    0, 0, vertexBuffer.VertexCount, 0, 2);
            }

            GraphicsDevice.SetVertexBuffer(null);
            GraphicsDevice.Indices = null;

            GraphicsDevice.BlendState = prevBlendState;
            GraphicsDevice.RasterizerState = prevRasterizerState;

            // Reset render states
            GraphicsDevice.BlendState = BlendState.Opaque;
        }

    }



}
