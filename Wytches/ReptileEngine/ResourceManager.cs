﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    public interface ResourceManager
    {
        T Load<T>(string resourcePath);

        void UnloadAll();
    }
}