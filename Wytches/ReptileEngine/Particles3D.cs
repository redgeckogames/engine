﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.GameOwn;
using Wytches.ReptileEngine;

namespace Wytches.ReptileEngine
{
    public class Particles3D : DrawableGameComponent
    {
        List<Vector4> billboardList = new List<Vector4>();
        private Texture2D particleTexture;
        private VertexPositionTexture[] billboardVertices;
        private Camera camera;
        private BasicEffect basicEffect;
        private Game game;

        public Particles3D(Texture2D _particleTexture, Microsoft.Xna.Framework.Game game) : base(game)
        {
            this.particleTexture = _particleTexture;
            
            basicEffect = new BasicEffect(GraphicsDevice);
        }

        public void AddBillboards()
        {
            billboardList.Add(new Vector4(0, 0, 0, 10));
            //billboardList.Add(new Vector4(0, 0, 0, 5));
            //billboardList.Add(new Vector4(20, 10, 0, 10));
        }

        public void CreateBBVertices(Camera mainCam)
        {
            billboardVertices = new VertexPositionTexture[billboardList.Count * 6];

            int i = 0;
            foreach (Vector4 currentV4 in billboardList)
            {
                Vector3 center = new Vector3(currentV4.X, currentV4.Y, currentV4.Z);
                float scaling = currentV4.W;

                Matrix bbMatrix = Matrix.CreateBillboard(center, mainCam.Position, mainCam.projection.Up,
                    mainCam.projection.Forward);

                //first triangle
                Vector3 posDL = new Vector3(-0.5f, -0.5f, 0);
                Vector3 billboardedPosDL = Vector3.Transform(posDL*scaling, bbMatrix);
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosDL, new Vector2(1, 1));
                Vector3 posUR = new Vector3(0.5f, 0.5f, 0);
                Vector3 billboardedPosUR = Vector3.Transform(posUR * scaling, bbMatrix);
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosUR, new Vector2(0, 0));
                Vector3 posUL = new Vector3(-0.5f, 0.5f, 0);
                Vector3 billboardedPosUL = Vector3.Transform(posUL * scaling, bbMatrix);
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosUL, new Vector2(1, 0));

                //second triangle, 2 corners already calculated
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosDL, new Vector2(1, 1));
                Vector3 posDR = new Vector3(0.5f, -0.5f, 0);
                Vector3 billboardedPosDR = Vector3.Transform(posDR*scaling, bbMatrix);
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosDR, new Vector2(0, 1));
                billboardVertices[i++] = new VertexPositionTexture(billboardedPosUR, new Vector2(0, 0));
            }
        }

        public void Draw(Camera mainCam, GraphicsDevice device)
        {
            basicEffect.World = Matrix.Identity;
            basicEffect.View = mainCam.view;
            basicEffect.Projection = mainCam.projection;
            basicEffect.TextureEnabled = true;
            basicEffect.Texture = particleTexture;

            
            foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.DrawUserPrimitives<VertexPositionTexture>(PrimitiveType.TriangleList, billboardVertices, 0, billboardList.Count*2);


            }
        }
    }
}
