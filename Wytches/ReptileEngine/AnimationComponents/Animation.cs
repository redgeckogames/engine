﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using SkinnedModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.AnimationComponents
{
    [DataContract]
    [Serializable]
    public class Animation : Renderer
    {
        [DataMember]
        private Model model;

        private Matrix[] boneTransformations;
        private float scale;
        public AnimationPlayer animationPlayer { get; private set; }
        private SkinningData skinningData;
        public AnimationClip clip { get; private set; }

        public AnimationStateName animationStateName { get; set; }

        public Animation(GameObject gameObject) : base(gameObject)
        {
            scale = 1f;
        }

        public Model Model
        {
            get
            {
                return model;
            }
            set
            {
                model = value;
            }
        }

        public void LoadModel(string modelPath)
        {
            this.model = MyContentManager.Load<Model>(modelPath);
        }

        public void LoadAnimation(string animationName)
        {
            // Look up our custom skinning information.
            skinningData = model.Tag as SkinningData;

            if (skinningData == null)
                throw new InvalidOperationException
                    ("This model does not contain a SkinningData tag.");

            // Create an animation player, and start decoding an animation clip.
            animationPlayer = new AnimationPlayer(skinningData);

            clip = skinningData.AnimationClips[animationName];

            animationPlayer.StartClip(clip);
        }

        public override void Update(GameTime gameTime)
        {
            if (animationPlayer != null)
            {
                animationPlayer.Update(gameTime.ElapsedGameTime, true, Matrix.Identity);
                base.Update(gameTime);
            }
        }

        public override void Draw(Camera camera)
        {
            // Render the skinned mesh.
            if (animationPlayer != null)
            {
                Matrix[] bones = animationPlayer.GetSkinTransforms();
                Matrix[] boneTransformations = new Matrix[model.Bones.Count];
                model.CopyAbsoluteBoneTransformsTo(boneTransformations);

                foreach (ModelMesh mesh in model.Meshes)
                {
                    foreach (SkinnedEffect effect in mesh.Effects)
                    {
                        effect.World = boneTransformations[mesh.ParentBone.Index]
                         * this.GameObject.Transform.getRotation() *
                          Matrix.CreateTranslation(GameObject.Transform.Position);

                        effect.SetBoneTransforms(bones);

                        effect.View = camera.view;
                        effect.Projection = camera.projection;

                        effect.EnableDefaultLighting();

                        effect.SpecularColor = new Vector3(0.25f);
                        effect.SpecularPower = 16;
                    }

                    mesh.Draw();
                }
            }
        }

        public Matrix GetMatrix()
        {
            return Matrix.CreateScale(scale)
                        // * Matrix.CreateFromQuaternion(gameObject.Transform.quaterion
                        * this.GameObject.Transform.getRotation()
                        * Matrix.CreateTranslation(GameObject.Transform.Position);
            //  * Matrix.CreateTranslation(gameObject.Transform.origin);
        }
    }
}