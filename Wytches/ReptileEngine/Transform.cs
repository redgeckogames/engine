﻿using System.Diagnostics;
using System.Runtime.Serialization;
using System.Security.AccessControl;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using Newtonsoft.Json;

namespace Wytches.ReptileEngine
{
    [DataContract]
    [Serializable]
    public class Transform : Component
    {
        //private Vector3 localPosition;

        private Vector3 Up;
        private Vector3 Forward;
        private Vector3 Right;

        public Vector3 position;
        private Matrix rotation;
        private Matrix modelOrientation { get; set; }
        private Matrix modelMatrix { get; set; }

        protected Transform()
            : base()
        {
        }

        public Transform(GameObject gameObject) : base(gameObject)
        {
            this.Up = new Vector3(0, 1, 0);
            this.Right = new Vector3(1, 0, 0);
            this.Forward = new Vector3(0, 0, 1);

            this.position = Vector3.Zero;
            this.rotation = Matrix.Identity;

            this.modelOrientation = Matrix.Identity;
            this.modelMatrix = Matrix.Identity;
        }

        public Transform(Transform other, GameObject gameobject)
            : this(gameobject)
        {
            this.Forward = new Vector3(other.Forward.X, other.Forward.Y, other.Forward.Z);
            this.Right = new Vector3(other.Right.X, other.Right.Y, other.Right.Z);
            this.Up = new Vector3(other.Up.X, other.Up.Y, other.Up.Z);
            this.position = new Vector3(other.position.X, other.position.Y, other.position.Z);
            this.rotation = Matrix.Identity;
            this.modelOrientation = Matrix.Identity;
            this.modelMatrix = Matrix.Identity;
           
        }

        public Transform(GameObject gameObject, Vector3 position) : base(gameObject)
        {
            this.Up = new Vector3(0, 1, 0);
            this.Right = new Vector3(1, 0, 0);
            this.Forward = new Vector3(0, 0, 1);

            this.position = position;
            this.rotation = Matrix.Identity;

            this.modelOrientation = Matrix.Identity;
            this.modelMatrix = Matrix.Identity;
            // Debug.WriteLine("sss {0}", rotation.ToString());
        }

        public Transform(GameObject gameObject, Vector3 position, Vector3 rotationVector3, float angle) : base(gameObject)
        {
            Debug.WriteLine("{0}, {1}", rotationVector3.ToString(), angle);
            this.Up = new Vector3(0, 1, 0);
            this.Right = new Vector3(1, 0, 0);
            this.Forward = new Vector3(0, 0, 1);

            this.position = position;

            this.rotation = Matrix.CreateRotationY(MathHelper.ToRadians(angle));
            //Matrix.CreateFromAxisAngle(this.Up, MathHelper.ToRadians(rotationVector3.Y)) *
            //      Matrix.CreateFromAxisAngle(this.Forward, MathHelper.ToRadians(rotationVector3.Z)) *
            //    Matrix.CreateFromAxisAngle(this.Right, MathHelper.ToRadians(rotationVector3.X));

            this.modelOrientation = Matrix.Identity;
            this.modelMatrix = Matrix.Identity;
            //   Debug.WriteLine("ssddddddddds {0}", rotation.ToString());
        }

        public Vector3 Position
        {
            set
            {
                position = value;
            }
            get
            {
                return this.position;
            }
        }

        public Matrix getRotation()
        {
            return rotation;
        }

        public void Translate(Vector3 translation, Space relativeTo = Space.Self)
        {
            switch (relativeTo)
            {
                case Space.Self:
                    Vector3 transX = translation.X * Right;
                    Vector3 transY = translation.Z * Forward;
                    Vector3 transZ = translation.Y * Up;
                   
                    this.position += (transX + transY + transZ);
                    if (this.GameObject.Tag.Equals("wiedzma"))
                    {
                        RotationToFace(translation, 0.2f);
                    }
                    break;

                case Space.World:
                    this.position += translation;
                    break;
            }
        }

        public void RotationToFace(Vector3 wantedDirection, float turnSpeed)
        {
            Vector3 witchFront = this.rotation.Backward; //chyba z rotacja cos jest odwrocone
            Vector3 witchRight;
            Vector3 witchUp = this.rotation.Up;

            float dot = Vector3.Dot(witchFront, wantedDirection);

            if (dot > -0.99)
            {
                witchFront = Vector3.Lerp(witchFront, wantedDirection, turnSpeed);
            }
            else
            {
                // Special case for if we are turning exactly 180 degrees.
                RotationToFace(this.rotation.Right, turnSpeed);
                return;
            }

            Vector3.Cross(ref witchUp, ref witchFront, out witchRight);
            Vector3.Cross(ref witchFront, ref witchRight, out witchUp);

            witchFront.Normalize();
            witchRight.Normalize();
            witchUp.Normalize();

            this.rotation.Backward = witchFront;
            this.rotation.Right = witchRight;
            this.rotation.Up = witchUp;
        }

        public void Rotate(Vector3 rotationVector3, float angle, Space relativeTo = Space.Self)
        {
            switch (relativeTo)
            {
                case Space.Self:
                    //Matrix rotationMatrixX = Matrix.CreateRotationX(rotationVector3.X);
                    //Matrix rotationMatrixY = Matrix.CreateRotationY(rotationVector3.Y);
                    //Matrix rotationMatrixZ = Matrix.CreateRotationZ(rotationVector3.Z);
                    this.rotation *= Matrix.CreateFromAxisAngle(this.Up, MathHelper.ToRadians(rotationVector3.Y)) *
                        Matrix.CreateFromAxisAngle(this.Forward, MathHelper.ToRadians(rotationVector3.Z)) *
                        Matrix.CreateFromAxisAngle(this.Right, MathHelper.ToRadians(rotationVector3.X));

                    //  Debug.WriteLine(rotation.Right + rotation.Left + rotation.Up + rotation.Down);
                    // Debug.WriteLine("aaaaaaaaaaaaaaaaaaaa {0}", rotation.ToString());
                    break;

                case Space.World:
                    //modelOrientation *= Matrix.CreateFromAxisAngle(rotation, angle);

                    break;
            }
        }

        public override string ToString()
        {
            return this.position.ToString();
        }
    }
}