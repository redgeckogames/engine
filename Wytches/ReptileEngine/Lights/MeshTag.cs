﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.Lights
{
    public class MeshTag
    {
        private Vector3 color;
        private Texture2D texture;
        private float specularPower;
        public Dictionary<string, EffectParameter> parameters;
        private Effect cachedEffect = null;

        public Vector3 Color
        {
            get
            {
                return color;
            }
            set
            {
                color = value;
            }
        }

        public Texture2D Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = value;
            }
        }

        public float SpecularPower
        {
            get
            {
                return specularPower;
            }
            set
            {
                specularPower = value;
            }
        }

        public Effect CachedEffect
        {
            get
            {
                return cachedEffect;
            }
            set
            {
                cachedEffect = value;
            }
        }

        public MeshTag(Effect effect)
        {
            parameters = new Dictionary<string, EffectParameter>();

            foreach (var parameter in effect.Parameters)
            {
                parameters.Add(parameter.Name, parameter);
            }
        }
    }
}
