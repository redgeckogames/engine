﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.Lights
{
        public class Material : ICloneable
        {
            protected Effect effect;

            public Material(Effect effect)
            {
                this.effect = effect;
            }

            public Effect GetEffect()
            {
                return effect;
            }

            public virtual void SetParameters()
            {
            }

            public virtual object Clone()
            {
                return new Material(effect);
            }
        }
    }
