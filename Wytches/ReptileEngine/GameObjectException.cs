﻿using System;

namespace Wytches.ReptileEngine
{
    [Serializable]
    public class GameObjectException : Exception
    {
        public GameObjectException(string message) : base(message)
        {
        }
    }
}