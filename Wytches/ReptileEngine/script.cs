﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    [DataContract]
    public class Script : Behaviour
    {
        public Script(GameObject gameObject) : base(gameObject)
        {
            this.GameObject.collisionHandler += OnCollision;
            this.GameObject.CollisionExit += OncollisionExit;
        }

        protected virtual void OncollisionExit(object sender, GameObjectEventArgs e)
        {
        }

        protected virtual void OnCollision(object sender, GameObjectEventArgs e)
        {
            //Debug.WriteLine("In Colision With " + e.gameObject.Tag);
        }
    }
}