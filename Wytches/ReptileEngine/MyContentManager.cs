﻿using Microsoft.Xna.Framework.Content;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    public class MyContentManager
    {
        private static ContentManager contentManager;

        public MyContentManager(ContentManager content)
        {
            contentManager = content;
        }

        public static T Load<T>(string resourcePath)
        {
            return contentManager.Load<T>(resourcePath);
        }

        public void UnloadAll()
        {
            contentManager.Unload();
        }
    }
}