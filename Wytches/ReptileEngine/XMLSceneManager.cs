﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    public class XMLSceneManager : SceneManager
    {
        private ISerializer<Scene> sceneSerializer;
        private readonly AsyncLock asyncLock = new AsyncLock();

        public XMLSceneManager(ISerializer<Scene> serializer)
        {
            this.sceneSerializer = serializer;
        }

        public Scene LoadScene(string sceneName)
        {
            string sceneFilePath = FindSceneFile(sceneName);

            Scene scene = DeserializeScene(sceneFilePath);

            scene.IsLoaded = true;

            return scene;
        }

        private string FindSceneFile(string sceneName)
        {
            string sceneFilePath = CreatePathToSceneFile(@"..\..\Scenes\" + sceneName);

            if (File.Exists(sceneFilePath + ".xml") == false)
                throw new SceneNotFoundException("Scene " + sceneName + " was not found, be sure to keep scene files in Scenes folder");

            return sceneFilePath;
        }

        private string CreatePathToSceneFile(string fileRelativePath)
        {
            string path;
            if (Environment.CurrentDirectory.Contains("Windows"))
            {
                path = Path.Combine(Environment.CurrentDirectory, @"..\..\" + fileRelativePath);
            }
            else
            {
                path = Path.Combine(Environment.CurrentDirectory, fileRelativePath);
            }

            return path;
        }

        private Scene DeserializeScene(string fileName)
        {
            Scene scene = sceneSerializer.ReadObject(fileName);

            return scene;
        }

        public async void SaveCurrentState(Scene scene)
        {
            string sceneFilePath = CreatePathToSceneFile(@"..\..\Scenes\" + scene.Name);
            using (await asyncLock.LockAsync())
            {
                await Task
                    .Factory
                    .StartNew(() => sceneSerializer.WriteObject(sceneFilePath, scene));
            }
        }

        public void UnloadScene(Scene scene)
        {
            if (scene.IsLoaded == false)
                throw new SceneNotLoadedException("Scene was not loaded use LoadScene method with a proper name to load a scene");
            else
            {
                scene.IsLoaded = false;
                SaveCurrentState(scene);
            }
        }
    }
}