﻿using System;

namespace Wytches.ReptileEngine
{
    [Serializable]
    public class ComponentAlreadyAddedException : GameObjectException
    {
        public ComponentAlreadyAddedException(string message) : base(message)
        {
        }
    }
}