﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    public interface SceneManager
    {
        Scene LoadScene(string sceneName);

        void SaveCurrentState(Scene scene);

        void UnloadScene(Scene scene);
    }
}