﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.ReptileEngine;

namespace Wytches.ReptileEngine
{
    public class BoxCollider : Colider
    {
        #region Fields

        private VertexPositionColor[] primitiveList;
        private Vector3[] corners;
        private Vector3 pos;
        private Model model;
        private Vector3 rotation;
        private BoundingBox[] boundingBoxes;
        private Matrix[] transforms;
        private Vector3[] lastPositionMin, lastPositionMax;
        private BoundingSphere completeBoundingSphere;

        public short[] bBoxIndices = {
             0, 1, 1, 2, 2, 3, 3, 0, // Front edges
             4, 5, 5, 6, 6, 7, 7, 4, // Back edges
             0, 4, 1, 5, 2, 6, 3, 7 // Side edges connecting front and back
         };

        #endregion Fields

        public BoundingBox[] BoundingBoxes
        {
            set
            {
                boundingBoxes = value;
            }
            get
            {
                return this.boundingBoxes;
            }
        }

        #region Contructors

        public BoxCollider(GameObject gameObject) : base(gameObject)
        {
            pos = gameObject.Transform.Position;

            if (gameObject.ContainsComponent<MeshRenderer>())
            {
                model = gameObject.GetComponent<MeshRenderer>().Model;
            }
            else
            {
                model = gameObject.GetComponent<AnimationComponents.Animation>().Model;
            }

            //rotation = gameObject.Transform.Rotation;

            // Set up model data
            boundingBoxes = new BoundingBox[model.Meshes.Count];

            transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            int i = 0;
            foreach (ModelMesh mesh in model.Meshes)
            {
                Matrix meshTransform;

                if (gameObject.ContainsComponent<MeshRenderer>())
                {
                    meshTransform = transforms[mesh.ParentBone.Index] * gameObject.GetComponent<MeshRenderer>().GetMatrix();
                }
                else
                {
                    meshTransform = transforms[mesh.ParentBone.Index] * gameObject.GetComponent<AnimationComponents.Animation>().GetMatrix();
                }
                boundingBoxes[i] = BuildBoundingBox(mesh, meshTransform);
                i++;
            }

            lastPositionMin = new Vector3[boundingBoxes.Length];
            lastPositionMax = new Vector3[boundingBoxes.Length];

            completeBoundingSphere = new BoundingSphere();
            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere origMeshSphere = mesh.BoundingSphere;
                BoundingSphere transMeshSphere = TransformBoundingSphere(origMeshSphere, transforms[mesh.ParentBone.Index]);
                completeBoundingSphere = BoundingSphere.CreateMerged(completeBoundingSphere, transMeshSphere);
            }
        }

        public BoxCollider(BoxCollider boxCollider, GameObject gameObject)
            : this(gameObject)
        {
            this.primitiveList = boxCollider.primitiveList;
            this.corners = boxCollider.corners;
            this.pos = boxCollider.pos;
            this.model = boxCollider.model;
            this.rotation = boxCollider.rotation;
            this.boundingBoxes = boxCollider.boundingBoxes;
            this.transforms = boxCollider.transforms;
            this.lastPositionMin = boxCollider.lastPositionMin;
            this.lastPositionMax = boxCollider.lastPositionMin;
        }

        #endregion Contructors

        private BoundingBox BuildBoundingBox(ModelMesh mesh, Matrix meshTransform)
        {
            //pos = gameObject.Transform.Position;

            // Create initial variables to hold min and max xyz values for the mesh
            Vector3 meshMax = new Vector3(float.MinValue);
            Vector3 meshMin = new Vector3(float.MaxValue);

            foreach (ModelMeshPart part in mesh.MeshParts)
            {
                // The stride is how big, in bytes, one vertex is in the vertex buffer
                // We have to use this as we do not know the make up of the vertex
                int stride = part.VertexBuffer.VertexDeclaration.VertexStride;

                VertexPositionNormalTexture[] vertexData = new VertexPositionNormalTexture[part.NumVertices];
                part.VertexBuffer.GetData(part.VertexOffset * stride, vertexData, 0, part.NumVertices, stride);

                // Find minimum and maximum xyz values for this mesh part
                Vector3 vertPosition = new Vector3();

                for (int i = 0; i < vertexData.Length; i++)
                {
                    vertPosition = vertexData[i].Position;

                    // update our values from this vertex
                    meshMin = Vector3.Min(meshMin, vertPosition);
                    meshMax = Vector3.Max(meshMax, vertPosition);
                }
            }

            // transform by mesh bone matrix
            meshMin = Vector3.Transform(meshMin, meshTransform);
            meshMax = Vector3.Transform(meshMax, meshTransform);

            // Create the bounding box
            BoundingBox box = new BoundingBox(new Vector3(meshMin.X, meshMin.Y, meshMin.Z), new Vector3(meshMax.X, meshMax.Y, meshMax.Z));

            return box;
        }

        public BoundingSphere GetCompleteBoundingSphere()
        {
            return completeBoundingSphere;
        }

        public VertexPositionColor[] GetPrimitiveList()
        {
            return primitiveList;
        }

        public BoundingBox[] GetPreciseBoundingBoxes()
        {
            BoundingBox[] boxes = new BoundingBox[model.Meshes.Count];

            for (int i = 0; i < model.Meshes.Count; i++)
            {
                ModelMesh mesh = model.Meshes[i];
                Matrix trans;
                if (GameObject.ContainsComponent<MeshRenderer>())
                {
                    trans = transforms[mesh.ParentBone.Index] * GameObject.GetComponent<MeshRenderer>().GetMatrix();
                }
                else
                {
                    trans = transforms[mesh.ParentBone.Index] * GameObject.GetComponent<AnimationComponents.Animation>().GetMatrix();
                }
                boxes[i] = BuildBoundingBox(mesh, trans);
               // boxes[i] = Colider.TransformBoundingBox(boundingBoxes[i], trans);
            }

            return boxes;
        }

        public static BoundingBox CreateBoxFromSphere(BoundingSphere sphere)
        {
            float radius = sphere.Radius;
            Vector3 outerPoint = new Vector3(radius, radius, radius);

            Vector3 p1 = sphere.Center + outerPoint;
            Vector3 p2 = sphere.Center - outerPoint;

            return new BoundingBox(p1, p2);
        }

        public override void Draw(Camera camera)
        {
            // Debug.WriteLine("rys coll");

            foreach (BoundingBox box in boundingBoxes)
            {
                corners = box.GetCorners();
                primitiveList = new VertexPositionColor[corners.Length];
                // Assign the 8 box vertices
                for (int i = 0; i < corners.Length; i++)
                {
                    primitiveList[i] = new VertexPositionColor(corners[i], Color.White);
                }

                /* Set your own effect parameters here */
                BasicEffect boxEffect = new BasicEffect(Game1.graphics.GraphicsDevice);

                //if (gameObject.ContainsComponent<MeshRenderer>())
                //{
                //    boxEffect.World = gameObject.GetComponent<MeshRenderer>().GetMatrix();
                //}
                //else
                //{
                //    boxEffect.World = gameObject.GetComponent<AnimationComponents.Animation>().GetMatrix();
                //}

                boxEffect.View = camera.view;
                boxEffect.Projection = Game1.projection;
                boxEffect.TextureEnabled = false;

                // Draw the box with a LineList
                foreach (EffectPass pass in boxEffect.CurrentTechnique.Passes)
                {
                    pass.Apply();
                    Game1.graphics.GraphicsDevice.DrawUserIndexedPrimitives(
                        PrimitiveType.LineList, primitiveList, 0, 8,
                        bBoxIndices, 0, 12);
                }

                //Array.Clear(primitiveList, 0, corners.Length);
                //Array.Clear(corners, 0, box.GetCorners().Length);
            }
            base.Draw(camera);
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }
    }
}