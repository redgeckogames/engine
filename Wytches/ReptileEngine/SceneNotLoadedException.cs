﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    public class SceneNotLoadedException : ReptileRuntimeException
    {
        public SceneNotLoadedException(string message) : base(message)
        {
        }
    }
}