﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Wytches.ReptileEngine.InformationWindow
{
    class InformationWindow : DrawableGameComponent
    {

        protected Game game;
        private Camera camera;
        private SpriteBatch spriteBatch;
        private BasicEffect basicEffect;
        private Texture2D informationBoxTexture;
        public Rectangle informationWindowRectangle;
        private SpriteFont dialogFont;
        public String textToDraw;
        public double counter;
        

        public InformationWindow(Microsoft.Xna.Framework.Game game, Camera camera) : base(game)
        {
            this.game = game;
            this.camera = camera;
            spriteBatch = MySpriteBatch.returnSpriteBatch();
            basicEffect = new BasicEffect(GraphicsDevice);
            informationBoxTexture = MyContentManager.Load<Texture2D>("Objects\\dialogBoxBackground");
            this.dialogFont = MyContentManager.Load<SpriteFont>("Objects\\DialogFont");
            informationWindowRectangle = new Rectangle(500, 100, informationBoxTexture.Width, informationBoxTexture.Height);   //the square to draw (local variable to avoid creating a new variable per square)
            textToDraw =
                "Testowa informacja do podania przez okno dialogowe zeby zobaczyc czy dziala itp ladshd oaisjd lasijd a";
            this.counter = 0;
            
        }

        public void InformationShowStart(GameTime gameTime, String information)
        {
            this.textToDraw = information;
            if (Game1.isCounterActive)
                counter += gameTime.ElapsedGameTime.TotalSeconds;
            if(counter <= 4)
                Draw(gameTime);
            else
            {
                Game1.isInformationWindowOpened = false;
                Game1.isCounterActive = false;
                counter = 0; 
            }          
        }

        public override void Draw(GameTime gameTime)
        {
            
            Game.IsMouseVisible = true;

            //wyłącznie Z buffora i rysowanie spirtów na rzeczach z 3d
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.DepthStencilState = DepthStencilState.None;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            //Vector3 textPosition = new Vector3(0, 300, 0);

            basicEffect.World = Matrix.CreateScale(1, -1, 1);
            basicEffect.View = camera.view;
            basicEffect.Projection = camera.projection;
            //start drawing

            spriteBatch.Begin();


            DrawDialogBox();
            DrawText();


            //end drawing
            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void DrawDialogBox()
        {

            Color colorToUse = Color.White;
            spriteBatch.Draw(informationBoxTexture, informationWindowRectangle, Color.White);
        }

        private void DrawText()
        {
            spriteBatch.DrawString(dialogFont, parseText(textToDraw), new Vector2(informationWindowRectangle.X + 5, informationWindowRectangle.Y + 5), Color.White);
            //spriteBatch.DrawString(dialogFont, goFurther, new Vector2(dialogBox.dialogBoxRectangle.X + 420, dialogBox.dialogBoxRectangle.Y + 120), Color.White);
        }

        private String parseText(String text)
        {
            String line = String.Empty;
            String returnString = String.Empty;
            String[] wordArray = text.Split(' ');

            foreach (String word in wordArray)
            {
                if (dialogFont.MeasureString(line + word).Length() > informationWindowRectangle.Width - 5)
                {
                    returnString = returnString + line + '\n';
                    line = String.Empty;
                }

                line = line + word + ' ';
            }

            return returnString + line;
        }

    }
}
