﻿using Microsoft.Xna.Framework;
using System;
using System.Runtime.Serialization;

namespace Wytches.ReptileEngine
{
    [DataContract]
    public class Colider : Component
    {
        public EventHandler collision;
        public bool IsTrigger { get; set; } = false;
        public bool OnTriggerEnter { get; set; } = false;

        public bool ColisionEntered { get; set; } = false;

        public Colider(GameObject gameObject) : base(gameObject)
        {
            collision += gameObject.OnCollision;
        }

        protected virtual void OnCollision()
        {
            if (collision == null)
                collision(this, EventArgs.Empty);
        }

        public static BoundingSphere TransformBoundingSphere(BoundingSphere originalBoundingSphere, Matrix transformationMatrix)
        {
            Vector3 trans;
            Vector3 scaling;
            Quaternion rot;
            transformationMatrix.Decompose(out scaling, out rot, out trans);

            float maxScale = scaling.X;
            if (maxScale < scaling.Y)
                maxScale = scaling.Y;
            if (maxScale < scaling.Z)
                maxScale = scaling.Z;

            float transformedSphereRadius = originalBoundingSphere.Radius * maxScale;
            Vector3 transformedSphereCenter = Vector3.Transform(originalBoundingSphere.Center, transformationMatrix);

            BoundingSphere transformedBoundingSphere = new BoundingSphere(transformedSphereCenter, transformedSphereRadius);

            return transformedBoundingSphere;
        }

        public static BoundingBox TransformBoundingBox(BoundingBox origBox, Matrix matrix)
        {
            Vector3 origCorner1 = origBox.Min;
            Vector3 origCorner2 = origBox.Max;

            Vector3 transCorner1 = Vector3.Transform(origCorner1, matrix);
            Vector3 transCorner2 = Vector3.Transform(origCorner2, matrix);

            return new BoundingBox(transCorner1, transCorner2);
        }
    }
}