﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Input;

namespace Wytches.ReptileEngine
{
    public static class Input
    {
        public static bool GetKeyDown(Keys key)
        {
            return Keyboard.GetState().IsKeyDown(key);
        }

        public static bool GetKeyReleased(Keys key)
        {
            return Keyboard.GetState().IsKeyUp(key);
        }
    }
}