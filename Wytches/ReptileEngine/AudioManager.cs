﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;

namespace Wytches.ReptileEngine
{
    public class AudioManager
    {
        private Song song;
        private List<SoundEffect> soundEffects { get; set; }

        public AudioManager()
        {
            this.soundEffects = new List<SoundEffect>();
        }

        public void AddSoundEffect(SoundEffect soundEffect, String instanceName)
        {
            soundEffects.Add(soundEffect);
        }

        public void PlaySoundEffect(int effectPosition)
        {
            soundEffects[effectPosition].Play();
        }

        public void AddSong(Song song)
        {
            this.song = song;
        }

        public void PlaySong()
        {
            MediaPlayer.Play(this.song);
            MediaPlayer.IsRepeating = true;
        }

        public void StopSong()
        {
            MediaPlayer.Stop();
        }
    }
}