﻿using System;

namespace Wytches.ReptileEngine
{
    [Serializable]
    public class ComponentNotFoundException : GameObjectException
    {
        public ComponentNotFoundException(string message) : base(message)
        {
        }
    }
}