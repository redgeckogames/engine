﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.AStar
{
    public class Map
    {
        Rectangle arena = new Rectangle(-170, -300, 340, 400);
        public List<Rectangle> Walls { get; }
        public List<Rectangle> Walls2 { get; }
        List<GameObject> gameObjectsOnScene;

        public Map()
        {
            Walls2 = new List<Rectangle>();
            Walls = new List<Rectangle>()
            {
                new Rectangle(13, 0, 1, 5),
new Rectangle(13, 5, 3, 1),
new Rectangle(13, 7, 1, 4),
new Rectangle(5, 5, 6, 1),
new Rectangle(10, 6, 1, 5)
};
        }

        public int Width
        {
            get
            {
                return arena.Width;
            }
        }
        public int Height
        {
            get
            {
                return arena.Height;
            }
        }

        public bool IsObstructionAt(Vector3 mapPosition)
        {
            if (!arena.Contains(new Point((int)mapPosition.X, (int)mapPosition.Z)))
            {
                return true;
            }
            foreach (var wall in Walls2)
            {
                if (wall.Contains(new Point((int)mapPosition.X, (int)mapPosition.Z)))
                {
                    return true;
                }
            }
            return false;
        }

        public void SetWall(GameObject gameObcjectMove, List<GameObject> gameObjectsOnScen)
        {
            this.gameObjectsOnScene = (gameObjectsOnScen).Where(o => (!o.ContainsComponent<Light>())).ToList();
            Vector3 min = Vector3.Zero;
            Vector3 max = Vector3.Zero;
            foreach (GameObject gameObjcet in gameObjectsOnScene)
            {
                if (gameObjcet.Equals(gameObcjectMove) == false)
                {
                    BoundingBox[] boxes;
                    if (gameObjcet.ContainsComponent<BoxCollider>())
                    {
                        if (gameObjcet.ContainsComponent<MeshRenderer>())
                        {
                            boxes = new BoundingBox[gameObjcet.GetComponent<MeshRenderer>().Model.Meshes.Count];
                            BoundingBox[] boundingBoxes = gameObjcet.GetComponent<BoxCollider>().GetPreciseBoundingBoxes();
                            for (int i = 0; i < gameObjcet.GetComponent<MeshRenderer>().Model.Meshes.Count; i++)
                            {
                                boxes[i] = CheckMinMax(boundingBoxes[i].Min, boundingBoxes[i].Max);
                            }
                        }
                        else
                        {
                            boxes = new BoundingBox[gameObjcet.GetComponent<AnimationComponents.Animation>().Model.Meshes.Count];
                            BoundingBox[] boundingBoxes = gameObjcet.GetComponent<BoxCollider>().GetPreciseBoundingBoxes();
                            for (int i = 0; i < gameObjcet.GetComponent<AnimationComponents.Animation>().Model.Meshes.Count; i++)
                            {
                                boxes[i] = CheckMinMax(boundingBoxes[i].Min, boundingBoxes[i].Max);
                            }
                        }

                        foreach (BoundingBox bbox in boxes)
                        {
                            min = bbox.Min; max = bbox.Max;
                            if (min.X > bbox.Min.X && min.Y > bbox.Min.Y && min.Z > bbox.Min.Z)
                            {
                                min = bbox.Min;
                            }

                            if (max.X < bbox.Max.X && max.Y < bbox.Max.Y && max.Z < bbox.Max.Z)
                            {
                                max = bbox.Max;
                            }
                        }
                        Walls2.Add(new Rectangle((int)min.X, (int)min.Z,
                           (int)(max.X - min.X), (int)(max.Z - min.Z)));


                    }
                }
            }
        }

        public BoundingBox CheckMinMax(Vector3 min, Vector3 max)
        {
            Vector3 temp;
            temp = min;
            if (temp.X > max.X)
            {
                min.X = max.X;
                max.X = temp.X;
            }
            if (temp.Y > max.Y)
            {
                min.Y = max.Y;
                max.Y = temp.Y;
            }
            if (temp.Z > max.Z)
            {
                min.Z = max.Z;
                max.Z = temp.Z;
            }
            //    Debug.WriteLine("po zamianie" + min + " " + max);
            return new BoundingBox(min, max);
        }
    }
}
