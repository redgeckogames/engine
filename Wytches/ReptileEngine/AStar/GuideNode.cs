﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.AStar
{
    public class GuideNode
    {
        public GuideNode Parent;
        public Vector3 Position;
        public int PathLength;
        public int Cost;

        public GuideNode(GuideNode parent, Vector3 position, Rectangle destination, float estimatedDistanceWeighting)
        {
            Parent = parent;
            Position = position;
            if (parent != null)
            {
                PathLength = parent.PathLength + 1;
            }
            //może trzeba usunać po Y
            var manhattanDistance = (Math.Abs(position.X - destination.X) + Math.Abs(position.Z - destination.Y)); 
            Cost = (int)(PathLength + (estimatedDistanceWeighting *  manhattanDistance));
        }

    }
}
