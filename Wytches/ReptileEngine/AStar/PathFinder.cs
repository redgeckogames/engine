﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.AStar
{
    internal class PathFinder
    {
        private List<GuideNode> openNodes = new List<GuideNode>();
        private HashSet<Vector3> openPositions = new HashSet<Vector3>();
        private HashSet<Vector3> closedPositions = new HashSet<Vector3>();
        public Vector3 Start;
        public Rectangle End;
        public Map map { get; }
        private float estimatedDistanceWeighting;

        public PathFinder(Vector3 start, Rectangle end, bool diagonalsEnabled, float estimatedDistanceWeighting)
        {
            Start = start;
            End = end;

            map = new Map();
            this.estimatedDistanceWeighting = estimatedDistanceWeighting;

            var startNode = new GuideNode(null, start, end, estimatedDistanceWeighting);
            openNodes.Add(startNode);
            GenerateSearchOffsets(diagonalsEnabled);
        }

        private List<Vector3> searchOffsets = new List<Vector3>();

        private void GenerateSearchOffsets(bool diagonalsEnabled)
        {
            searchOffsets.Add(new Vector3(0, 0, -0.5f));
            searchOffsets.Add(new Vector3(-0.5f, 0, 0));
            searchOffsets.Add(new Vector3(0.5f, 0, 0));
            searchOffsets.Add(new Vector3(0, 0, 0.5f));
            if (diagonalsEnabled)
            {
                searchOffsets.Add(new Vector3(-0.5f, 0, -0.5f));
                searchOffsets.Add(new Vector3(0.5f, 0, -0.5f));
                searchOffsets.Add(new Vector3(0.5f, 0, 0.5f));
                searchOffsets.Add(new Vector3(-0.5f, 0, 0.5f));
            }
        }

        public void ConsiderMove()
        {
            openNodes.Sort((a, b) => { return a.Cost.CompareTo(b.Cost); });
            var node = ExtractAndCloseNextNonPathNode();
            foreach (var offset in searchOffsets)
            {
                var possiblePosition = new Vector3(
                node.Position.X + offset.X,
                0,     //node.Position.Y + offset.Y,
                node.Position.Z + offset.Z);

                if (openPositions.Contains(possiblePosition) || closedPositions.Contains(possiblePosition))
                {
                    continue;
                }
                if (map.IsObstructionAt(possiblePosition))
                {
                    continue;
                }
                var possibility = new GuideNode(node, possiblePosition, End, estimatedDistanceWeighting);
                openNodes.Add(possibility);
                openPositions.Add(possibility.Position);
            }
        }

        private GuideNode ExtractAndCloseNextNonPathNode()
        {
            var nodeIndex = 0;
            while (End.Contains(new Point((int)openNodes[nodeIndex].Position.X, (int)openNodes[nodeIndex].Position.Z)) == true)
            {
                nodeIndex++;
            }
            var node = openNodes[nodeIndex];
            openNodes.RemoveAt(nodeIndex);
            openPositions.Remove(node.Position);
            closedPositions.Add(node.Position);
            return node;
        }

        public List<Vector3> Path
        {
            get
            {
                var path = new List<Vector3>();
                if (!PathFound)
                {
                    return path;
                }
                var node = openNodes[0];
                while (node != null)
                {
                    path.Insert(0, node.Position);
                    node = node.Parent;
                }
                return path;
            }
        }

        public bool PathFound
        {
            get
            {
                if (End.Contains(new Point((int)openNodes[0]?.Position.X, (int)openNodes[0]?.Position.Z)) == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }

        public HashSet<Vector3> PointsPendingConsideration
        {
            get
            {
                var points = new HashSet<Vector3>();
                foreach (var node in openNodes)
                {
                    points.Add(node.Position);
                }
                return points;
            }
        }

        public HashSet<Vector3> ConsideredPoints
        {
            get
            {
                var points = new HashSet<Vector3>();
                foreach (var point in closedPositions)
                {
                    points.Add(point);
                }
                return points;
            }
        }
    }
}