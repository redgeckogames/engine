﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Wytches.ReptileEngine
{
    public class GameObjectAnimations
    {
        [XmlAttribute(AttributeName = "tag")]
        public string Tag { get; set; }

        [XmlElement(ElementName = "animation")]
        public List<GameObjectAnimation> listAnimation { get; set; }
    }
}