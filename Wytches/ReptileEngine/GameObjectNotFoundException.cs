﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    public class GameObjectNotFoundException : GameObjectException
    {
        public GameObjectNotFoundException(string message) : base(message)
        {
        }
    }
}