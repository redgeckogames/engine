﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    public interface StateMachine
    {
        void ChangeState(GameObject gameObject, AnimationStateName animationState);
    }
}