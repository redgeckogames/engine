﻿using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    public class MySpriteBatch
    {
        private static SpriteBatch mySpriteBatch;

        public MySpriteBatch(SpriteBatch sprite)
        {
            mySpriteBatch = sprite;
        }

        public static SpriteBatch returnSpriteBatch()
        {
            return mySpriteBatch;
        }
    }
}