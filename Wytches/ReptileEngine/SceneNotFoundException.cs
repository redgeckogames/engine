﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    internal class SceneNotFoundException : ReptileRuntimeException
    {
        public SceneNotFoundException(string message) : base(message)
        {
        }
    }
}