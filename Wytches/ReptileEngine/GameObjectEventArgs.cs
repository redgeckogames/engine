﻿using System;

namespace Wytches.ReptileEngine
{
    public class GameObjectEventArgs : EventArgs
    {
        public GameObject gameObject { get; set; }
    }
}