﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Wytches.ReptileEngine.DialogSystem
{
    public class DialogBox : DrawableGameComponent
    {

        protected Game game;
        private Camera camera;
        private SpriteBatch spriteBatch;
        private BasicEffect basicEffect;
        private Texture2D dialogBoxTexture;
        public Rectangle dialogBoxRectangle;

        public DialogBox(Microsoft.Xna.Framework.Game game, Camera camera) : base(game)
        {
            this.game = game;
            this.camera = camera;
            spriteBatch = MySpriteBatch.returnSpriteBatch();
            basicEffect = new BasicEffect(GraphicsDevice);
            dialogBoxTexture = MyContentManager.Load<Texture2D>("Objects\\dialogBoxBackground");
            dialogBoxRectangle = new Rectangle(200, 600, dialogBoxTexture.Width * 3, dialogBoxTexture.Height * 2);   //the square to draw (local variable to avoid creating a new variable per square)
        }

        public override void Draw(GameTime gameTime)
        {
            Game.IsMouseVisible = true;

            //wyłącznie Z buffora i rysowanie spirtów na rzeczach z 3d
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.DepthStencilState = DepthStencilState.None;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            //Vector3 textPosition = new Vector3(0, 300, 0);

            basicEffect.World = Matrix.CreateScale(1, -1, 1);
            basicEffect.View = camera.view;
            basicEffect.Projection = camera.projection;
            //start drawing

            spriteBatch.Begin();

            
            DrawDialogBox();
            

            //end drawing
            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void DrawDialogBox()
        {
           
            Color colorToUse = Color.White;
            spriteBatch.Draw(dialogBoxTexture, dialogBoxRectangle, Color.White);
        }

        public void Update()
        {
            
        }
    }
}
