﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;

namespace Wytches.ReptileEngine.DialogSystem
{
    class DialogLoader
    {
        private List<DialogImage> dialogImagesList;
        private String currentCharacterName;
        private DialogsList currentDialog;
        private DialogBox dialogBox;
        private DialogsList dialogsList;
        private DialogStuff dialogStuff;
        private bool isActive;
        private bool goFurther;

        //private IButtonState ibuttonStateDialog = IButtonState.None;


        private DialogDraw dialogDraw;

        public DialogLoader(List<DialogImage> dialogImagesList, DialogBox dialogBox, DialogDraw dialogDraw)
        {
            this.dialogStuff = new DialogStuff();
            this.dialogsList = new DialogsList();
            this.dialogImagesList = dialogImagesList;
            this.dialogBox = dialogBox;
            this.dialogDraw = dialogDraw;
            isActive = false;

        }

        public void StartDialog(GameTime gameTime, int number, int verse)
        {
            
                dialogBox.Draw(gameTime);
                currentDialog = dialogStuff.getDialog(number);

            if (verse < currentDialog.dialogsList.Count)
            {
                //foreach (Dialogs dial in currentDialog.dialogsList)
                //{

                foreach (DialogImage img in dialogImagesList)
                {
                    if (img.getCharacterName() == currentDialog.dialogsList[verse].character)
                        img.Draw(gameTime);
                }
                dialogDraw.setTextToDraw(currentDialog.dialogsList[verse].text);
                dialogDraw.Draw(gameTime);


                //if (Input.GetKeyDown(Keys.Space) && ibuttonStateDialog == IButtonState.None)
                //{
                //    shouldGoWithDialog = !shouldGoWithDialog;
                //    ibuttonStateDialog = IButtonState.IsBeingPressd;
                //    dialogNumber++;
                //}
                //if (Input.GetKeyReleased(Keys.Space) && ibuttonStateDialog == IButtonState.IsBeingPressd)
                //    ibuttonStateDialog = IButtonState.None;


            }
            else
            {
                Game1.isDialogActive = false;
            }
            //}

            


        }

        public void Update(bool isActive)
        {
            this.isActive = isActive;
        }

    }
}


