﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Wytches.ReptileEngine.DialogSystem
{
    class DialogImage : DrawableGameComponent
    {

        private String characterName;
        private Texture2D characterImage;
        protected Game game;
        private Camera camera;
        private SpriteBatch spriteBatch;
        private BasicEffect basicEffect;

        public DialogImage(Microsoft.Xna.Framework.Game game, Camera camera, Texture2D characterTexture, String characterName) : base(game)
        {
            this.camera = camera;
            this.game = game;
            this.characterImage = characterTexture;
            this.characterName = characterName;
            spriteBatch = MySpriteBatch.returnSpriteBatch();
            basicEffect = new BasicEffect(GraphicsDevice);
        }

        public override void Draw(GameTime gameTime)
        {
            //wyłącznie Z buffora i rysowanie spirtów na rzeczach z 3d
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.DepthStencilState = DepthStencilState.None;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            //Vector3 textPosition = new Vector3(0, 300, 0);

            basicEffect.World = Matrix.CreateScale(1, -1, 1);
            basicEffect.View = camera.view;
            basicEffect.Projection = camera.projection;
            //start drawing

            spriteBatch.Begin();


            DrawDialogImage();


            //end drawing
            spriteBatch.End();

            base.Draw(gameTime);
        }

        private void DrawDialogImage()
        {
            Rectangle dialogBoxRectangle = new Rectangle(-70, 560, characterImage.Width/6, characterImage.Height/6);   //the square to draw (local variable to avoid creating a new variable per square)
            Color colorToUse = Color.White;
            spriteBatch.Draw(characterImage, dialogBoxRectangle, Color.White);
        }

        public String getCharacterName()
        {
            return characterName;
        }
    }
}
