﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace Wytches.ReptileEngine.DialogSystem
{
    class DialogDraw :DrawableGameComponent
    {
        protected Game game;
        private Camera camera;
        private SpriteBatch spriteBatch;
        private BasicEffect basicEffect;
        private SpriteFont dialogFont;
        private String textToDraw;
        private String goFurther;
        private DialogBox dialogBox;

        public DialogDraw(Microsoft.Xna.Framework.Game game, Camera camera, DialogBox dialogBox) : base(game)
        {
            this.camera = camera;
            this.game = game;
            spriteBatch = MySpriteBatch.returnSpriteBatch();
            basicEffect = new BasicEffect(GraphicsDevice);
            this.dialogFont = MyContentManager.Load<SpriteFont>("Objects\\DialogFont");
            this.textToDraw = "Wcisnij spacje";
            this.goFurther = "Wcisnij spacje";
            this.dialogBox = dialogBox;

        }

        public void setTextToDraw(String text)
        {
            this.textToDraw = text;
        }

        public override void Draw(GameTime gameTime)
        {
            //wyłącznie Z buffora i rysowanie spirtów na rzeczach z 3d
            GraphicsDevice.BlendState = BlendState.AlphaBlend;
            GraphicsDevice.DepthStencilState = DepthStencilState.None;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearClamp;

            //Vector3 textPosition = new Vector3(0, 300, 0);

            basicEffect.World = Matrix.CreateScale(1, -1, 1);
            basicEffect.View = camera.view;
            basicEffect.Projection = camera.projection;
            //start drawing

            spriteBatch.Begin();


            DrawText();


            //end drawing
            spriteBatch.End();
            base.Draw(gameTime);
        }

        private void DrawText()
        {
            spriteBatch.DrawString(dialogFont, parseText(textToDraw), new Vector2(dialogBox.dialogBoxRectangle.X+20, dialogBox.dialogBoxRectangle.Y+20), Color.White);
            spriteBatch.DrawString(dialogFont, goFurther, new Vector2(dialogBox.dialogBoxRectangle.X+420, dialogBox.dialogBoxRectangle.Y+120), Color.White);
        }

        private String parseText(String text)
        {
            String line = String.Empty;
            String returnString = String.Empty;
            String[] wordArray = text.Split(' ');

            foreach (String word in wordArray)
            {
                if (dialogFont.MeasureString(line + word).Length() > dialogBox.dialogBoxRectangle.Width-20)
                {
                    returnString = returnString + line + '\n';
                    line = String.Empty;
                }

                line = line + word + ' ';
            }

            return returnString + line;
        }
    }
}
