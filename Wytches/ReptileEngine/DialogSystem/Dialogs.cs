﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.DialogSystem
{
    class Dialogs
    {
        public String character;
        public String text;
        public int number;

        public Dialogs(String character, String text, int number)
        {
            this.character = character;
            this.text = text;
            this.number = number;
        }
    }
}
