﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Wytches.GameOwn;
using Wytches.Scripts;
using Wytches.ReptileEngine;

namespace Wytches.ReptileEngine.DialogSystem
{
    public class DialogEvents : Script
    {

        private InventoryScript inventoryScript;
        private CauldronScript cauldronScript;
        private WitchScript witchScript;
        private RitualScript ritualScript;


        public DialogEvents(GameObject gameobject) : base(gameobject)
        {
            inventoryScript = gameobject.GetComponent<InventoryScript>();
            inventoryScript.ItemConsumed += OnItemConsumed;
            cauldronScript = gameobject.GetComponent<CauldronScript>();
            cauldronScript.cauldronOpened += OnCauldronOpened;
            cauldronScript.healthPotionMade += OnPotionMade;
            cauldronScript.manaPotionMade += OnManaPotionMade;
            ritualScript = gameobject.GetComponent<RitualScript>();
            ritualScript.alterOpened += OnAltarOpened;

        }

        private void OnCauldronOpened(object sender, EventArgs e)
        {
            Game1.isDialogActive = true;
            Game1.dialogNumber = 2;
            Game1.verse = 0;
            cauldronScript.cauldronOpened -= OnCauldronOpened;
        }

        private void OnPotionMade(object sender, EventArgs e)
        {
            Game1.isDialogActive = true;
            Game1.dialogNumber = 3;
            Game1.verse = 0;
            cauldronScript.manaPotionMade -= OnPotionMade;
        }

        private void OnItemConsumed(object sender, ItemEventArgs e)
        {
            Game1.isDialogActive = true;
            Game1.dialogNumber = 4;
            Game1.verse = 0;
            inventoryScript.ItemConsumed -= OnItemConsumed;
        }

        private void OnManaPotionMade(object sender, EventArgs e)
        {
            Game1.isDialogActive = true;
            Game1.dialogNumber = 5;
            Game1.verse = 0;
            cauldronScript.manaPotionMade -= OnManaPotionMade;
        }

        private void OnAltarOpened(object sender, EventArgs e)
        {
            Game1.isDialogActive = true;
            Game1.dialogNumber = 6;
            Game1.verse = 0;
            ritualScript.alterOpened -= OnAltarOpened;
        }
    }
}
