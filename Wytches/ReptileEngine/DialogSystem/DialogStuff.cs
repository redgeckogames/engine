﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine.DialogSystem
{
    class DialogStuff
    {
        public List<DialogsList> dialogsLists;

        public DialogStuff()
        {
            dialogsLists = new List<DialogsList>();
            MakeDialogs();
        }

        private void MakeDialogs()
        {
            DialogsList dialog1 = new DialogsList();
            dialog1.addDialog("demon", "Witaj Czarownico! Otworz kociołek, ktory stoi obok Ciebie!", 1);
            dialog1.addDialog("witch", "Kim jesteś i czego ode mnie chcesz?!", 2);
            dialog1.addDialog("demon", "Twoja ciotka przyzwała mnie tu dawno temu! Chodź, pokażę ci okolicę!", 3);
            dialog1.addDialog("witch", "Nie wiem, czy powinnam ci ufać, ale dobrze, dam ci szansę", 4);
            dialog1.addDialog("demon", "Gdy juz podejdziesz do kociołka wciśnij klawisz 'E' aby go otworzyć.", 5);
            dialogsLists.Add(dialog1);

            //DialogsList dialog1 = new DialogsList();
            //dialog1.addDialog("narrator", "W krainie Alkara zyla sobie wiedzma. Wiodla spokojne zycie az do momentu, w ktorym dowiedziala sie o spadku po zmarlej ciotce-czarownicy. Otrzymala gospodarstwo, do ktorego chciala sie niezwlocznie wprowadzic. Nie wiedziala jednak co ja czeka w niedalekiej przyszlosci...", 1);
            //dialogsLists.Add(dialog1);

            DialogsList dialog2 = new DialogsList();
            dialog2.addDialog("demon", "Mozesz przeciagnac teraz trzy skladniki ze swojej sakwy do kociolka, aby stworzyc miksture. " +
                                       "Po ich dodaniu  wcisnij przycisk 'Mieszaj' a w twojej sakwie pojawi sie mikstura. Uwazaj! Nie kazda kombinacja skladnikow da Ci uzyteczna miksture! " +
                                       "Biala mikstura nie posiada zadnego efektu!", 1);
            dialog2.addDialog("witch", "Dobrze, zrobie jak mowisz. Mam nadzieje, ze sie nie poparze.", 2);
            dialogsLists.Add(dialog2);

            DialogsList dialog3 = new DialogsList();
            dialog3.addDialog("demon", "Brawo, stworzylas swoja pierwsza miksture! Aby ja wypic wcisnij na niej prawy przycisk myszy!", 1);
            dialogsLists.Add(dialog3);

            DialogsList dialog4 = new DialogsList();
            dialog4.addDialog("demon", "Teraz gdy wiesz juz jak tworzyc mikstury, powinnas stworzyc miksture many, aby uzupelnic jej braki.", 1);
            dialog4.addDialog("witch", "Tak, musze to koniecznie zrobic. Ale jak?!", 2);
            dialog4.addDialog("demon", "Jak widzisz wszedzie wkolo jest wiele roznych roslin. Podejdz do ktorejs z nich i wcisnij klawisz 'E' a " +
                                       "wtedy zbierzesz ja i pojawi sie w Twoim ekwipunku. Aby zajrzec do ekwipunku mozesz w dowolnym momencie wcisnac klawisz 'I'. " +
                                       "Zbieraj skladniki, aby tworzyc mikstury. Jak juz pewnie zauwazylas Twoja energia spada co pewien czas. To wszystko przez wiesniakow, ktorzy" +
                                       "wycinaja nasze drzewa!", 3);
            dialog4.addDialog("witch", "Ale jak to?! Co moge z tym zrobic?!", 4);
            dialog4.addDialog("demon", "Pomoge Ci, ale najpierw musisz zebrac skladniki na miksture many.", 5);
            dialog4.addDialog("witch", "Dobrze, juz biegne!", 6);
            dialogsLists.Add(dialog4);

            DialogsList dialog5 = new DialogsList();
            dialog5.addDialog("demon", "Stworzylas miksture many! Jesli ja wypijesz ilosc Twojej many wzrosnie, wiec moge Ci teraz powiedziec jak powstrzymac tych wiesniakow." +
                                       "Musisz pojsc do oltarza majacego ksztalt demona. Gdy do niego podejdziesz, wcisnij klawisz 'E' i postepuj zgodnie z instrukcjami." +
                                       "Przywolamy Stworzenie, ktore zajmie sie tymi niewdziecznikami!", 1);
            dialog5.addDialog("witch", "Brzmi powaznie... Ale nalezy im sie! Zrobmy to!", 2);
            dialog5.addDialog("demon", "Pamietaj, aby po drodze zebrac ze soba trzy skladniki. Powiem Ci po co sa one potrzebne, gdy dotrzesz na miejsce...", 3);
            dialogsLists.Add(dialog5);

            DialogsList dialog6 = new DialogsList();
            dialog6.addDialog("demon", "Bardzo dobrze, dotarlas do oltarza. Teraz mozemy wezwac demona! Bedzie Ci sluzyc przez jakis czas, w zaleznosci od tego, jakich skladnikow " +
                                       "uzyjesz do zlozenia ofiary aby go wezwac. Przeciagnij trzy dowolne skladniki na wolne miejsca oltarza i stworz demona! " +
                                       "To potezne stworzenie ruszy na wioske i zrobi wszystko, zeby ja zniszczyc, a wtedy wygramy! A wiec do dziela, przyzwij demona!", 1);
            dialog6.addDialog("witch", "Czy jestes pewien, ze to zadziala? Nigdy nie uzywalam nigdy tak poteznej magii.", 2);
            dialog6.addDialog("demon", "Tak, jestem pewien, ze to zadziala. Gdy demon bedzie juz w naszym swiecie, od razu ruszy na wioske. Jesli jego czas zycia dobiegnie konca przed " +
                                       "zniszczeniem wszystkich domow, bedziesz musiala wezwac kolejnego. Pamietaj tez aby caly czas kontrolowac poziom swojego zycia!", 3);
            dialog6.addDialog("witch", "Czas pokazac tym pacholkom kto tu rzadzi!", 4);
            dialogsLists.Add(dialog6);

        }

        public DialogsList getDialog(int number)
        {

            DialogsList dial = dialogsLists[number-1];
            return dial;
        }
    }
}
