﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;
using Wytches.ReptileEngine.Lights;

namespace Wytches.ReptileEngine
{
    [DataContract]
    [Serializable]
    public class MeshRenderer : Renderer
    {
        [DataMember]
        private Model model;

        private Effect effect;
        private Texture2D texture;
        private Texture2D normal;
        public Material Material { get; set; }

        private Matrix[] boneTransformations;
        private float scale;

        public MeshRenderer(GameObject gameObject) : base(gameObject)
        {
            scale = 1f;
        }

        public MeshRenderer(MeshRenderer meshRenderer, GameObject gameObject)
            : this(gameObject)
        {
        }

        public Model Model
        {
            get
            {
                return model;
            }
            set
            {
                model = value;
                this.effect = Material.GetEffect();
            }
        }

        public Effect Effect
        {
            get
            {
                return effect;
            }
            set
            {
                effect = value;
            }
        }

        public Texture2D Texture
        {
            get
            {
                return texture;
            }
            set
            {
                texture = value;
            }
        }

        public Texture2D NormalTexture
        {
            get
            {
                return normal;
            }
            set
            {
                normal = value;
            }
        }

        public void LoadModel(string modelPath)
        {
            this.model = MyContentManager.Load<Model>(modelPath);
            this.effect = Material.GetEffect();
        }

        public void LoadEffect(string modelPath)
        {
            this.effect = MyContentManager.Load<Effect>(modelPath);
        }

        //do rysowania cieni
        public void Draw(Matrix View, Matrix Projection, Vector3 Position)
        {
            boneTransformations = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(boneTransformations);
            if (this.GameObject.Transform != null)
            {
                foreach (ModelMesh mesh in model.Meshes)
                {
                    foreach (ModelMeshPart meshPart in mesh.MeshParts)
                    {
                        meshPart.Effect = effect;
                        Material.SetParameters();
                        Matrix World = (boneTransformations[mesh.ParentBone.Index]
                                                             * Matrix.CreateScale(scale)
                                                             * this.GameObject.Transform.getRotation()
                                                             * Matrix.CreateTranslation(gameObject.Transform.Position));
                        effect.Parameters["WorldMatrix"]?.SetValue(World);
                        effect.Parameters["ViewMatrix"]?.SetValue(View);
                        effect.Parameters["ProjectionMatrix"]?.SetValue(Projection);
                        effect.Parameters["CameraPosition"]?.SetValue(Position);
                        if (texture != null)
                        {
                            effect.Parameters["BasicTexture"]?.SetValue(texture);
                            effect.Parameters["TextureEnabled"]?.SetValue(true);
                        }
                        else
                        {
                            effect.Parameters["TextureEnabled"]?.SetValue(false);
                        }

                        if (normal != null)
                        {
                            effect.Parameters["NormalTexture"]?.SetValue(normal);
                            effect.Parameters["NormalEnabled"]?.SetValue(true);
                        }
                        else
                        {
                            effect.Parameters["NormalEnabled"]?.SetValue(false);
                        }

                        effect.Parameters["WorldInverseTranspose"]?.SetValue(Matrix.Transpose(Matrix.Invert(mesh.ParentBone.Transform * World)));
                        effect.Parameters["ViewVector"]?.SetValue(Vector3.Transform(Vector3.Forward, gameObject.GetComponent<Transform>().getRotation()));
                    }

                    mesh.Draw();
                }
            }
        }

        public override void Draw(Camera camera)
        {
            boneTransformations = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(boneTransformations);
            if (this.GameObject.Transform != null)
            {
                foreach (ModelMesh mesh in model.Meshes)
                {
                    foreach (ModelMeshPart meshPart in mesh.MeshParts)
                    {
                        meshPart.Effect = effect;
                        Material.SetParameters();
                        Matrix World = (boneTransformations[mesh.ParentBone.Index]
                                                            * Matrix.CreateScale(scale)
                                                            * this.GameObject.Transform.getRotation()
                                                            * Matrix.CreateTranslation(gameObject.Transform.Position));
                        effect.Parameters["WorldMatrix"]?.SetValue(World);
                        effect.Parameters["ViewMatrix"]?.SetValue(camera.view);
                        effect.Parameters["ProjectionMatrix"]?.SetValue(camera.projection);
                        effect.Parameters["CameraPosition"]?.SetValue(camera.Position);
                        if (texture != null)
                        {
                            effect.Parameters["BasicTexture"]?.SetValue(texture);
                            effect.Parameters["TextureEnabled"]?.SetValue(true);
                        }
                        else
                        {
                            effect.Parameters["TextureEnabled"]?.SetValue(false);
                        }

                        if (normal != null)
                        {
                            effect.Parameters["NormalTexture"]?.SetValue(normal);
                            effect.Parameters["NormalEnabled"]?.SetValue(true);
                        }
                        else
                        {
                            effect.Parameters["NormalEnabled"]?.SetValue(false);
                        }

                        effect.Parameters["WorldInverseTranspose"]?.SetValue(Matrix.Transpose(Matrix.Invert(mesh.ParentBone.Transform * World)));
                        effect.Parameters["ViewVector"]?.SetValue(Vector3.Transform(Vector3.Forward, gameObject.GetComponent<Transform>().getRotation()));

                        //if (effect is BasicEffect)
                        //{
                        //   ((BasicEffect)effect).World = boneTransformations[mesh.ParentBone.Index]
                        //        * Matrix.CreateScale(scale)
                        //        * this.GameObject.Transform.getRotation() *
                        //        Matrix.CreateTranslation(GameObject.Transform.Position);

                        //    ((BasicEffect)effect).View = camera.view;
                        //    ((BasicEffect)effect).Projection = camera.projection;
                        //    ((BasicEffect)effect).EnableDefaultLighting();
                        //}
                        //else
                        //{
                        //    Material.SetParameters();

                        //    effect.Parameters["WorldMatrix"]?.SetValue(this.GameObject.Transform.getRotation() * Matrix.CreateScale(scale) *
                        //       Matrix.CreateTranslation(GameObject.Transform.Position) * mesh.ParentBone.Transform);
                        //    effect.Parameters["ViewMatrix"]?.SetValue(camera.view);
                        //    effect.Parameters["ProjectionMatrix"]?.SetValue(camera.projection);
                        //    effect.Parameters["CameraPosition"]?.SetValue(camera.Position);
                        //    //   effect.Parameters["ViewVector"].SetValue(camera.Position);
                        //    //    effect.Parameters["ModelTexture"].SetValue(texture);

                        //    Matrix worldInverseTransposeMatrix = Matrix.Transpose(Matrix.Invert(mesh.ParentBone.Transform * Matrix.CreateScale(scale)
                        //        * this.GameObject.Transform.getRotation() *
                        //        Matrix.CreateTranslation(GameObject.Transform.Position)));
                        //  //   effect.Parameters["WorldInverseTransposeMatrix"]?.SetValue(worldInverseTransposeMatrix);

                        //}
                    }

                    mesh.Draw();
                }
            }
        }

        public Matrix GetMatrix()
        {
            return Matrix.CreateScale(scale)
                        // * Matrix.CreateFromQuaternion(gameObject.Transform.quaterion
                        * this.GameObject.Transform.getRotation()
                        * Matrix.CreateTranslation(GameObject.Transform.Position);
            //  * Matrix.CreateTranslation(gameObject.Transform.origin);
        }

        //shader

        private void generateTags()
        {
            foreach (ModelMesh mesh in Model.Meshes)
                foreach (ModelMeshPart part in mesh.MeshParts)
                    if (part.Effect is BasicEffect)
                    {
                        BasicEffect effect = (BasicEffect)part.Effect;
                        MeshTag tag = new MeshTag(effect);
                        part.Tag = tag;
                    }
        }

        //// Store references to all of the model's current effects
        //public void CacheEffects()
        //{
        //    foreach (ModelMesh mesh in model.Meshes)
        //        foreach (ModelMeshPart part in mesh.MeshParts)
        //            ((MeshTag)part.Tag).CachedEffect = part.Effect;
        //}

        //// Restore the effects referenced by the model's cache
        //public void RestoreEffects()
        //{
        //    foreach (ModelMesh mesh in model.Meshes)
        //        foreach (ModelMeshPart part in mesh.MeshParts)
        //            if (((MeshTag)part.Tag).CachedEffect != null)
        //                part.Effect = ((MeshTag)part.Tag).CachedEffect;
        //}

        public void SetModelEffect(Effect effect, bool CopyEffect)
        {
            this.effect = effect;
            if (texture != null)
            {
                effect.Parameters["BasicTexture"]?.SetValue(texture);
                effect.Parameters["TextureEnabled"]?.SetValue(true);
            }
            //            foreach (ModelMesh mesh in Model.Meshes)
            //                foreach (ModelMeshPart part in mesh.MeshParts)
            //                {
            //                    Effect toSet = effect;
            //                    // Copy the effect if necessary
            //                    if (CopyEffect)
            //                        toSet = effect.Clone();
            //                    //MeshTag tag = ((MeshTag)part.Tag);
            //                    //// If this ModelMeshPart has a texture, set it to the effect
            //                    if (texture != null)
            //  {
            //    effect.Parameters["BasicTexture"]?.SetValue(texture);
            //    effect.Parameters["TextureEnabled"]?.SetValue(true);
            //  }
            //                    //}
            //                    //else
            //                        setEffectParameter(toSet, "TextureEnabled", false);
            //                    // Set our remaining parameters to the effect
            ////  setEffectParameter(toSet, "DiffuseColor", tag.Color);
            ////  setEffectParameter(toSet, "SpecularPower", tag.SpecularPower);
            //                    part.Effect = toSet;
            //                }
        }

        private void setEffectParameter(Effect effect, string paramName, object val)
        {
            if (effect.Parameters[paramName] == null)
                return;

            if (val is Vector3)
                effect.Parameters[paramName].SetValue((Vector3)val);
            else if (val is bool)
                effect.Parameters[paramName].SetValue((bool)val);
            else if (val is Matrix)
                effect.Parameters[paramName].SetValue((Matrix)val);
            else if (val is Texture2D)
                effect.Parameters[paramName].SetValue((Texture2D)val);
        }

        public void SetModelMaterial(Material material)
        {
            this.Material = (Material)material.Clone();
            this.effect = material.GetEffect();
            if (texture != null)
            {
                effect.Parameters["BasicTexture"]?.SetValue(texture);
                effect.Parameters["TextureEnabled"]?.SetValue(true);
            }
        }
    }
}