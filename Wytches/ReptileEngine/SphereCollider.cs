﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    internal class SphereCollider : Colider
    {
        #region Fields

        private BoundingSphere[] boundingSphere;
        private Matrix[] transforms;
        private Model model;
        private Vector3 rotation;
        private Vector3 pos;
        private BoundingSphere completeBoundingSphere;

        #endregion Fields

        public BoundingSphere[] BoundingSpheree
        {
            set
            {
                boundingSphere = value;
            }
            get
            {
                return this.boundingSphere;
            }
        }
        #region Contructors

        public SphereCollider(GameObject gameObject) : base(gameObject)
        {
            if (gameObject.ContainsComponent<MeshRenderer>())
            {
                model = gameObject.GetComponent<MeshRenderer>().Model;
            }
            else
            {
                model = gameObject.GetComponent<AnimationComponents.Animation>().Model;
            }
            //   rotation = gameObject.Transform.Rotation;
            pos = gameObject.Transform.Position;

            transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            completeBoundingSphere = new BoundingSphere();
            foreach (ModelMesh mesh in model.Meshes)
            {
                BoundingSphere origMeshSphere = mesh.BoundingSphere;
                BoundingSphere transMeshSphere = TransformBoundingSphere(origMeshSphere, transforms[mesh.ParentBone.Index]);
                completeBoundingSphere = BoundingSphere.CreateMerged(completeBoundingSphere, transMeshSphere);
            }
        }

        public SphereCollider(SphereCollider sphereCollider, GameObject gameObject)
            : this(gameObject)
        {
            this.boundingSphere = sphereCollider.boundingSphere;
            this.pos = sphereCollider.pos;
            this.model = sphereCollider.model;
            this.rotation = sphereCollider.rotation;
            this.completeBoundingSphere = sphereCollider.completeBoundingSphere;
            this.transforms = sphereCollider.transforms;
        }

        #endregion Contructors

        public BoundingSphere GetCompleteBoundingSphere()
        {
            return completeBoundingSphere;
        }

        public BoundingSphere[] GetPreciseBoundingSpheres()
        {
            //  transforms = new Matrix[model.Bones.Count];
            //  model.CopyAbsoluteBoneTransformsTo(transforms);

            BoundingSphere[] spheres = new BoundingSphere[model.Meshes.Count];

            for (int i = 0; i < model.Meshes.Count; i++)
            {
                ModelMesh mesh = model.Meshes[i];
                BoundingSphere origSphere = mesh.BoundingSphere;

                Matrix trans;

                if (GameObject.ContainsComponent<MeshRenderer>())
                {
                    trans = transforms[mesh.ParentBone.Index] * GameObject.GetComponent<MeshRenderer>().GetMatrix();
                }
                else
                {
                    trans = transforms[mesh.ParentBone.Index] * GameObject.GetComponent<AnimationComponents.Animation>().GetMatrix();
                }
                BoundingSphere transSphere = TransformBoundingSphere(origSphere, trans);
                spheres[i] = transSphere;
            }

            return spheres;
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public void DrawSphereSpikes(BoundingSphere sphere, GraphicsDevice device, Matrix worldMatrix, Matrix viewMatrix, Matrix projectionMatrix)
        {
            Vector3 up = sphere.Center + sphere.Radius * Vector3.Up;
            Vector3 down = sphere.Center + sphere.Radius * Vector3.Down;
            Vector3 right = sphere.Center + sphere.Radius * Vector3.Right;
            Vector3 left = sphere.Center + sphere.Radius * Vector3.Left;
            Vector3 forward = sphere.Center + sphere.Radius * Vector3.Forward;
            Vector3 back = sphere.Center + sphere.Radius * Vector3.Backward;

            VertexPositionColor[] sphereLineVertices = new VertexPositionColor[6];
            sphereLineVertices[0] = new VertexPositionColor(up, Color.White);
            sphereLineVertices[1] = new VertexPositionColor(down, Color.White);
            sphereLineVertices[2] = new VertexPositionColor(left, Color.White);
            sphereLineVertices[3] = new VertexPositionColor(right, Color.White);
            sphereLineVertices[4] = new VertexPositionColor(forward, Color.White);
            sphereLineVertices[5] = new VertexPositionColor(back, Color.White);

            BasicEffect basicEffect = new BasicEffect(device);

            //  basicEffect.World = worldMatrix;
            basicEffect.View = viewMatrix;
            basicEffect.Projection = projectionMatrix;
            basicEffect.VertexColorEnabled = true;

            foreach (EffectPass pass in basicEffect.CurrentTechnique.Passes)
            {
                pass.Apply();
                device.DrawUserPrimitives(PrimitiveType.LineList, sphereLineVertices, 0, 3);
            }

            //DebugDraw debugDraw = new DebugDraw(device);

            //debugDraw.Begin(viewMatrix, projectionMatrix);
            //debugDraw.DrawWireSphere(sphere, Color.Yellow);
            //debugDraw.End();
        }
    }
}