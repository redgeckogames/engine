﻿using System;

namespace Wytches.ReptileEngine
{
    public class ReptileRuntimeException : Exception
    {
        public ReptileRuntimeException(string message) : base(message)
        {
        }
    }
}