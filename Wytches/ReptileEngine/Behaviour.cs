﻿using System.Runtime.Serialization;

namespace Wytches.ReptileEngine
{
    [DataContract]
    public class Behaviour : Component
    {
        [DataMember]
        public bool Enabled { get; set; } = false;

        public Behaviour(GameObject gameObject) : base(gameObject)
        {
        }
    }
}