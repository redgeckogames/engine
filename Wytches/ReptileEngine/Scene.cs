﻿using Microsoft.Xna.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Wytches.ReptileEngine
{
    [DataContract]
    public class Scene
    {
        [DataMember]
        private string name = "DefaultSceneName";

        private Rectangle arena = new Rectangle(-200, -140, 400, 280);

        public string Name
        {
            get
            {
                return this.name;
            }
        }

        [DataMember]
        public bool IsLoaded { get; set; } = false;

        [DataMember]
        private List<GameObject> gameObjectsOnScene;

        public Scene()
        {
            gameObjectsOnScene = new List<GameObject>();
        }

        public Scene(string name)
        {
            this.name = name;
            gameObjectsOnScene = new List<GameObject>();
        }

        public Scene(List<GameObject> gameobjects, string name)
        {
            this.name = name;
            gameObjectsOnScene = gameobjects;
        }

        public void AddGameObject(GameObject gameObject)
        {
            gameObject.Scene = this;
            gameObjectsOnScene.Add(gameObject);
        }

        public void RemoveAllGameObjectsWithTag(string gameObjectTag)
        {
            gameObjectsOnScene.Remove(FindGameObjectWithtag(gameObjectTag));
        }

        public void RemoveGameObject(GameObject gameObject)
        {
            gameObjectsOnScene.Remove(gameObject);
        }

        public GameObject FindGameObjectWithtag(string gameObjectTag)
        {
            GameObject obj = gameObjectsOnScene.Where(g => g.Tag.Equals(gameObjectTag) && g.Tag != "").FirstOrDefault();
            if (obj == null)
                throw new GameObjectNotFoundException("GameObject With a tag " + gameObjectTag + " does not exist on this Scene");

            return obj;
        }

        public List<GameObject> GetAllGameObjects()
        {
            return this.gameObjectsOnScene;
        }

        public void Update(GameTime gameTime)
        {
            foreach (GameObject gameObject in gameObjectsOnScene.ToList())
            {
                gameObject.Update(gameTime);
            }
        }

        public void Draw(Camera camera)
        {
            IEnumerable<GameObject> activeGameObjects = GetActiveGameObjects();

            foreach (GameObject gameObject in activeGameObjects.ToList())
            {
                gameObject.Draw(camera);
            }
        }

        public bool IsObstructionAt(Vector3 possiblePosition)
        {
            if (!arena.Contains(new Point((int)possiblePosition.X, (int)possiblePosition.Z)))
            {
                return true;
            }
            foreach (var gameObject in gameObjectsOnScene)
            {
                if (gameObject.ContainsComponent<BoxCollider>())
                {
                    foreach (BoundingBox boundingBox in gameObject.GetComponent<BoxCollider>().BoundingBoxes)
                    {
                        if (boundingBox.Contains(possiblePosition) == ContainmentType.Contains)
                            return true;
                    }
                }
                else if (gameObject.ContainsComponent<SphereCollider>())
                {
                    foreach (BoundingSphere sphereBox in gameObject.GetComponent<SphereCollider>().BoundingSpheree)
                    {
                        if (sphereBox.Contains(possiblePosition) == ContainmentType.Contains)
                            return true;
                    }
                }
            }
            return false;
        }

        private IEnumerable<GameObject> GetActiveGameObjects()
        {
            return this.gameObjectsOnScene.Where(g => g.isActive == true);
        }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }
            else
            {
                Scene scene = obj as Scene;
                return scene.name == this.name && this.gameObjectsOnScene.Equals(this.gameObjectsOnScene);
            }
        }

        public override int GetHashCode()
        {
            return this.name.GetHashCode() ^ this.gameObjectsOnScene.GetHashCode();
        }
    }
}