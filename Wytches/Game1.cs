using System;
using System.Collections.Generic;
using System.Diagnostics;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Wytches.ReptileEngine;
using Wytches.Scripts;
using SkinnedModel;
using Wytches.ReptileEngine.AnimationComponents;
using Wytches.GameOwn;
using Wytches.ReptileEngine.EventSystem;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
using Wytches.Wytches_Game;
using Wytches.ReptileEngine.AStar;
using Wytches.ReptileEngine.Lights;
using Wytches.GameOwn.Interactions;
using Wytches.ReptileEngine.DialogSystem;
using NLog;
using Wytches.ReptileEngine.InformationWindow;
using Wytches.ReptileEngine.ParticlesNew;

namespace Wytches
{
    internal enum IButtonState
    {
        IsBeingPressd,
        None
    }

    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        private static readonly Logger logger = LogManager.GetCurrentClassLogger();
        private IButtonState ibuttonState = IButtonState.None;
        private IButtonState ibuttonStatePaused = IButtonState.None;
        private IButtonState ibuttonStateDialog = IButtonState.None;
        public static GraphicsDeviceManager graphics { get; private set; }
        public static SpriteBatch spriteBatch;

        private RenderCapture renderCapture;
        private PostProcessor postprocessor;

        private PrelightingRenderer lightRenderer;

        private GameObject pointLight1;
        private GameObject pointLight2;
        private GameObject pointLight3;
        private GameObject pointLight4;
        private GameObject pointLight5;
        private GameObject pointLight6;

        private GameObject terren;

        private GameObject rock;
        private GameObject house2;
        private GameObject house3;
        private GameObject house4;
        private List<GameObject> listHouse;

        private GameObject tree;
        private GameObject tree2;
        private GameObject tree3;
        private GameObject tree4;
        private GameObject tree5;
        private GameObject tree6;
        private GameObject tree7;
        private GameObject tree8;

        private GameObject tree9;
        private GameObject tree10;
        private GameObject tree11;
        private GameObject tree12;
        private GameObject tree13;
        private GameObject tree14;
        private GameObject tree15;
        private GameObject tree16;
        private GameObject tree17;
        private List<GameObject> listTree;
        private List<GameObject> humanList = new List<GameObject>();
        private GameObject homeWitch;

        private GameObject modelTest;
        private GameObject modelTest2;
        private GameObject water;
        private GameObject water2;

        private GameObject wallUp;
        private GameObject wallDown;
        private GameObject wallLeft;
        private GameObject wallRight;

        private GameObject playerWitch;
        private GameObject farmtile;
        private GameObject farmtile2;
        private GameObject farmtile3;
        private GameObject farmtile4;
        private GameObject farmtile5;
        private GameObject farmtile6;
        private GameObject farmtile7;
        private GameObject farmtile8;
        private GameObject farmtile9;
        private GameObject farmtile10;
        private GameObject farmtile11;
        private GameObject farmtile12;

        private GameObject siano;
        private GameObject siano2;

        public static GameObject alter;
        public static List<GameObject> demonList;

        public static GameObject ligthingObject;

        public static Scene scene;
        private Scene mainScene;
        private Camera mainCam;
        private ColliderManager colliderManager;
        private SceneManager sceneManager;
        private MyContentManager contentManager;
        private MySpriteBatch mySpriteBatch;

        private TextureDB textureImageInventory;
        private ModelPlantDB model3dPlant;

        public static Matrix projection { get; private set; }

        private InventoryDraw inventoryDraw;
        private CauldronDraw cauldronDraw;
        private AlterDraw alterDraw;
        private DragAndDropScript dragAndDropScript;
        public static bool boolInventory;
        public static bool boolCauldron;
        public static bool boolAlter;
        public static bool shouldDrawInventory;
        private bool shouldisPaused;
        public bool shouldGoWithDialog;

        private Song song;
        public static AudioManager audioManager;

        private ParticleEngine particleEngine;
        public static bool createParticleEngine;

        public Texture2D texture;
        public VertexBuffer vertexBuffer;
        public IndexBuffer indexBuffer;
        private Effect effect;
        private List<Vector3> positonBillboard;
        private List<Billboard> listBillboard;

        private Texture2D manaLifeBars;
        private UserInterface userInterfaceDraw;

        private MenuInGame menuInGame;
        private MenuInGame mainMenu;
        private bool isPaused;
        private Button quitButton;
        private Button playButton;
        private Button playGameButton;
        private Button quitGameButton;

        private Texture2D mainMenuBackground;
        private Texture2D logoScreen;

        private Particles3D particles3D;

        private PathDestinationFinder pathFinder;

        private Material defaultMaterial;
        private Material transparencyMaterial;
        private Material treemat;

        private ScenesEnum scenesEnum;

        private DialogLoader dialogLoader;
        private DialogBox dialogBox;
        private DialogDraw dialogDraw;
        private DialogImage witchDialogImage;
        private DialogImage demonDialogImage;
        private DialogImage wiesniakDialogImage;
        private List<DialogImage> dialogImagesList;

        public static bool isDialogActive;
        public static int dialogNumber = 1;
        public static int verse = 0;
        public DialogEvents dialogEvents;

        private Texture2D redGeckoLogo;
        private Texture2D victoryScreen;
        private Texture2D gameOverScreen;

        public static bool isInformationWindowOpened;
        private InformationWindow informationWindow;
        public static String infoMessage;
        public static bool isCounterActive;

        public static bool particleStarted;
        public static int tex = 0;
        public SpriteFont dialogFont;
        public String textToDraw;
        public Rectangle boxRectangle;
        private int i = 0;

        // // ///

        #region modeleDOzaladowania

        private ModelDictionary treeModel;
        private ModelDictionary dom1Model;
        private ModelDictionary dom2Model;
        private ModelDictionary waterModel;

        #endregion modeleDOzaladowania

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.GraphicsProfile = GraphicsProfile.HiDef;

            contentManager = new MyContentManager(Content);
            sceneManager = new XMLSceneManager(new MyXmlSerializer<Scene>());

            Content.RootDirectory = "Content";

            audioManager = new AudioManager();
            createParticleEngine = false;
            isPaused = false;
            isDialogActive = true;
            dialogImagesList = new List<DialogImage>();
            this.scenesEnum = ScenesEnum.Logo;
            isInformationWindowOpened = false;
            isCounterActive = false;
            particleStarted = false;
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            LoadModel();

            scene = new Scene("ScenaTestowa");
            AnimationState.LoadAnimationsFromTheFile("Animations");
            scene.IsLoaded = true;

            boolInventory = false;
            boolCauldron = false;
            boolAlter = false;
            shouldDrawInventory = false;
            shouldisPaused = false;
            shouldGoWithDialog = false;

            listHouse = new List<GameObject>();

            ////for(int i = 0; i<40; i++)
            ////{
            ////    scene.AddGameObject();
            ////}

            //// TODO: Add your initialization logic here
            graphics.PreferredBackBufferWidth = 1366;
            graphics.PreferredBackBufferHeight = 768;
            graphics.IsFullScreen = false;
            graphics.ApplyChanges();

            pointLight1 = new GameObject(new Vector3(50, 10, -200));
            pointLight1.AddComponent<Light>();
            pointLight1.GetComponent<Light>().Color = Color.Red;
            pointLight1.GetComponent<Light>().Attenuation = 20;
            pointLight1.GetComponent<Light>().Direction = new Vector3(-50f, 100f, -200);

            pointLight2 = new GameObject(new Vector3(-60, 5, -220));
            pointLight2.AddComponent<Light>();
            pointLight2.GetComponent<Light>().Color = Color.GreenYellow;
            pointLight2.GetComponent<Light>().Attenuation = 10;
            pointLight2.GetComponent<Light>().Direction = new Vector3(40f, 0f, 0);

            pointLight3 = new GameObject(new Vector3(30, 10f, 0));
            pointLight3.AddComponent<Light>();
            pointLight3.GetComponent<Light>().Color = Color.Yellow;
            pointLight3.GetComponent<Light>().Attenuation = 20;
            pointLight3.GetComponent<Light>().Direction = new Vector3(40f, 0f, 0);

            pointLight4 = new GameObject(new Vector3(-70, 100f, 70));
            pointLight4.AddComponent<Light>();
            pointLight4.GetComponent<Light>().Color = Color.White;
            pointLight4.GetComponent<Light>().Attenuation = 150;
            pointLight4.GetComponent<Light>().Direction = new Vector3(40f, 0f, 0);


            pointLight5 = new GameObject(new Vector3(70, 100f, 70));
            pointLight5.AddComponent<Light>();
            pointLight5.GetComponent<Light>().Color = Color.White;
            pointLight5.GetComponent<Light>().Attenuation = 150;
            pointLight5.GetComponent<Light>().Direction = new Vector3(40f, 0f, 0);

            pointLight6 = new GameObject(new Vector3(-70, 100f, -70));
            pointLight6.AddComponent<Light>();
            pointLight6.GetComponent<Light>().Color = Color.White;
            pointLight6.GetComponent<Light>().Attenuation = 150;
            pointLight4.GetComponent<Light>().Direction = new Vector3(40f, 0f, 0);

            terren = new GameObject(new Vector3(0, -2, 0));
            terren.AddComponent<MeshRenderer>();

            rock = new GameObject(new Vector3(-20, 0, 0));
            rock.AddComponent<MeshRenderer>();

            house2 = new GameObject(new Vector3(120, 0, 80));
            house2.AddComponent<MeshRenderer>();
            house2.Tag = "Domek2";
            house3 = new GameObject(new Vector3(70, 0, -50), Vector3.Up, 180);
            house3.AddComponent<MeshRenderer>();
            house3.Tag = "Domek3";
            house4 = new GameObject(new Vector3(0, 0, 70), Vector3.Up, 180);
            house4.AddComponent<MeshRenderer>();
            house4.Tag = "Domek4";

            //  listHouse.Add(house1);
            listHouse.Add(house2);
            listHouse.Add(house3);
            listHouse.Add(house4);
            InteractionSet structureInteractionSet = new InteractionSet();
            structureInteractionSet.AddInterAction("HitMe", new HitMe());
            Structure farmGouseStructure = new FarmHouse()
            {
                Attributes = new WitchAttribiutes()
                {
                    currentHealthPoints = 20,
                    maxHealthPoints = 20
                },
                Interactions = structureInteractionSet,
                NpcCapacity = 3
            };
            listHouse.ForEach(house => house.AddComponent<StructureScript>().Structure = new FarmHouse(farmGouseStructure));

            GameObject pesant = new GameObject(new Vector3(0, 0, -60));

            pesant.AddComponent<Animation>();
            pesant.GetComponent<Animation>().Model = Content.Load<Model>("wiesniakmixamo\\wiesniakrunpoprawa");
            pesant.GetComponent<Animation>().LoadAnimation("Take 001");
            pesant.Tag = "wiesniak";
            pesant.AddComponent<HumanScript>();
            humanList.Add(pesant);
            scene.AddGameObject(pesant);
            listTree = new List<GameObject>();
            tree = new GameObject(new Vector3(100, 0, -140));
            tree.AddComponent<MeshRenderer>();
            tree.Tag = "tree";
            tree2 = new GameObject(new Vector3(80, 0, -180));
            tree2.AddComponent<MeshRenderer>();
            tree2.Tag = "tree2";
            tree3 = new GameObject(new Vector3(30, 0, -160));
            tree3.AddComponent<MeshRenderer>();
            tree3.Tag = "tree3";
            tree4 = new GameObject(new Vector3(10, 0, -240));
            tree4.AddComponent<MeshRenderer>();
            tree4.Tag = "tree4";
            tree5 = new GameObject(new Vector3(5, 0, -90));
            tree5.AddComponent<MeshRenderer>();
            tree5.Tag = "tree5";
            tree6 = new GameObject(new Vector3(70, 0, -260));
            tree6.AddComponent<MeshRenderer>();
            tree6.Tag = "tree6";
            tree7 = new GameObject(new Vector3(110, 0, -280));
            tree7.AddComponent<MeshRenderer>();
            tree7.Tag = "tree7";
            tree8 = new GameObject(new Vector3(120, 0, -230));
            tree8.AddComponent<MeshRenderer>();
            tree8.Tag = "tree8";

            tree9 = new GameObject(new Vector3(-20, 0, -30));
            tree9.AddComponent<MeshRenderer>();
            tree9.Tag = "tree9";
            tree10 = new GameObject(new Vector3(-50, 0, -50));
            tree10.AddComponent<MeshRenderer>();
            tree10.Tag = "tree10";
            tree11 = new GameObject(new Vector3(-35, 0, -150));
            tree11.AddComponent<MeshRenderer>();
            tree11.Tag = "tree11";
            tree12 = new GameObject(new Vector3(-100, 0, -90));
            tree12.AddComponent<MeshRenderer>();
            tree12.Tag = "tree12";
            tree13 = new GameObject(new Vector3(-150, 0, -120));
            tree13.AddComponent<MeshRenderer>();
            tree13.Tag = "tree13";
            tree14 = new GameObject(new Vector3(-95, 0, 0));
            tree14.AddComponent<MeshRenderer>();
            tree14.Tag = "tree14";
            tree15 = new GameObject(new Vector3(-105, 0, 70));
            tree15.AddComponent<MeshRenderer>();
            tree15.Tag = "tree15";
            tree16 = new GameObject(new Vector3(-123, 0, 101));
            tree16.AddComponent<MeshRenderer>();
            tree16.Tag = "tree16";
            tree17 = new GameObject(new Vector3(-147, 0, -35));
            tree17.AddComponent<MeshRenderer>();
            tree17.Tag = "tree17";

            listTree.Add(tree);
            listTree.Add(tree2);
            listTree.Add(tree3);
            listTree.Add(tree4);
            listTree.Add(tree5);
            listTree.Add(tree6);
            listTree.Add(tree7);
            listTree.Add(tree8);

            listTree.Add(tree9);
            listTree.Add(tree10);
            listTree.Add(tree11);
            listTree.Add(tree12);
            listTree.Add(tree13);
            listTree.Add(tree14);
            listTree.Add(tree15);
            listTree.Add(tree16);
            listTree.Add(tree17);

            siano = new GameObject(new Vector3(140, 0, 31));
            siano.AddComponent<MeshRenderer>();
            siano2 = new GameObject(new Vector3(157, 0,-16));
            siano2.AddComponent<MeshRenderer>();

            Structure treeStructure = new Structure()
            {
                Attributes = new WitchAttribiutes()
                {
                    currentHealthPoints = 70,
                    maxHealthPoints = 70
                },
                Interactions = structureInteractionSet,
            };

            listTree.ForEach(tree => tree.AddComponent<StructureScript>().Structure = new Structure(treeStructure));

            homeWitch = new GameObject(new Vector3(-120, 0, -250));
            homeWitch.AddComponent<MeshRenderer>();
            homeWitch.Tag = "homeWitch";

            // TODO: Add your initialization logic here
            modelTest = new GameObject(new Vector3(-40, 0, -50));
            modelTest.AddComponent<Animation>();

            modelTest2 = new GameObject(new Vector3(-60, 2, -220));
            modelTest2.AddComponent<MeshRenderer>();
            modelTest2.Tag = "Cauldron";

            water = new GameObject(new Vector3(0, 2, -325));
            water.AddComponent<MeshRenderer>();
            water.Tag = "woda";
            water2 = new GameObject(new Vector3(0, 1, -330));
            water2.AddComponent<MeshRenderer>();
            water2.Tag = "woda";

            playerWitch = new GameObject(new Vector3(-40, 0, -220));
            playerWitch.AddComponent<Animation>();
            playerWitch.AddComponent<InventoryScript>();
            playerWitch.AddComponent<WitchAudioEvents>();
            
            playerWitch.Tag = "wiedzma";
            playerWitch.IsDynamic = true;

            alter = new GameObject(new Vector3(50, 2, -200));
            alter.AddComponent<MeshRenderer>();
            alter.Tag = "Alter";

            demonList = new List<GameObject>();

            farmtile = new GameObject(new Vector3(-80, 1, -275));
            farmtile.AddComponent<MeshRenderer>();
            farmtile.Tag = "ros";
            farmtile2 = new GameObject(new Vector3(-50, 1, -280));
            farmtile2.AddComponent<MeshRenderer>();
            farmtile2.Tag = "ros2";
            farmtile3 = new GameObject(new Vector3(48, 1, -276));
            farmtile3.AddComponent<MeshRenderer>();
            farmtile3.Tag = "ros3";
            farmtile4 = new GameObject(new Vector3(-140, 1, 20));
            farmtile4.AddComponent<MeshRenderer>();
            farmtile4.Tag = "ros4";

            farmtile5 = new GameObject(new Vector3(-10, 1, -50));
            farmtile5.AddComponent<MeshRenderer>();
            farmtile5.Tag = "ros5";
            farmtile6 = new GameObject(new Vector3(-117, 1, -118));
            farmtile6.AddComponent<MeshRenderer>();
            farmtile6.Tag = "ros6";
            farmtile7 = new GameObject(new Vector3(-1, 1, -155));
            farmtile7.AddComponent<MeshRenderer>();
            farmtile7.Tag = "ros7";
            farmtile8 = new GameObject(new Vector3(-66, 1, 67));
            farmtile8.AddComponent<MeshRenderer>();
            farmtile8.Tag = "ros8";
            farmtile9 = new GameObject(new Vector3(123, 1, -174));
            farmtile9.AddComponent<MeshRenderer>();
            farmtile9.Tag = "ros9";

            wallUp = new GameObject(new Vector3(0, 0, 120));
            wallUp.AddComponent<MeshRenderer>();
            wallUp.isActive = false;

            wallDown = new GameObject(new Vector3(0, 0, -300));
            wallDown.AddComponent<MeshRenderer>();
            wallDown.isActive = false;

            wallLeft = new GameObject(new Vector3(160, 0, -50));
            wallLeft.AddComponent<MeshRenderer>();
            wallLeft.isActive = false;

            wallRight = new GameObject(new Vector3(-160, 0, -50));
            wallRight.AddComponent<MeshRenderer>();
            wallRight.isActive = false;

            ligthingObject = new GameObject(new Vector3(0, 0, -500));
            ligthingObject.AddComponent<MeshRenderer>();
            ligthingObject.Tag = "lightingObject";

            scene.AddGameObject(pointLight1);
            scene.AddGameObject(pointLight2);
            scene.AddGameObject(pointLight3);
            scene.AddGameObject(pointLight4);
            scene.AddGameObject(pointLight5);
            scene.AddGameObject(pointLight6);
            scene.AddGameObject(terren);

            scene.AddGameObject(siano);
            scene.AddGameObject(siano2);
            
            //  scene.AddGameObject(rock);
            scene.AddGameObject(house2);
            scene.AddGameObject(house3);
            scene.AddGameObject(house4);

            foreach (var treee in listTree)
            {
                scene.AddGameObject(treee);
            }

            scene.AddGameObject(homeWitch);

          //  scene.AddGameObject(modelTest);
            scene.AddGameObject(modelTest2);
            scene.AddGameObject(water2);
          //  scene.AddGameObject(water);
            scene.AddGameObject(playerWitch);
            scene.AddGameObject(farmtile);
            scene.AddGameObject(farmtile2);
            scene.AddGameObject(farmtile3);
            scene.AddGameObject(farmtile4);
            scene.AddGameObject(farmtile5);
            scene.AddGameObject(farmtile6);
            scene.AddGameObject(farmtile7);
            scene.AddGameObject(farmtile8);
            scene.AddGameObject(farmtile9);
            //  //scene.AddGameObject(testAnimacja);
            scene.AddGameObject(alter);

            scene.AddGameObject(wallUp);
            scene.AddGameObject(wallDown);
            scene.AddGameObject(wallLeft);
            scene.AddGameObject(wallRight);

            scene.AddGameObject(ligthingObject);

            projection = Matrix.CreatePerspectiveFieldOfView(MathHelper.ToRadians(45f), graphics.GraphicsDevice.Viewport.AspectRatio, .1f, 1000f);

            //sceneManager.SaveCurrentState(scene);

            textureImageInventory = new TextureDB("tekstury.txt");
            model3dPlant = new ModelPlantDB("modelPlant.txt");

            //show the mouse
            IsMouseVisible = false;

            positonBillboard = new List<Vector3>();
            listBillboard = new List<Billboard>();
            InitializePositionBillboard(150f);

            particles3D = new Particles3D(Content.Load<Texture2D>("Objects\\billboardtexture"), this);
            particles3D.AddBillboards();

            this.textToDraw =
                "Kieruj poczynaniami wiedźmy i zniszcz domki wieśniakow wzywając potężnego demona! Zbieraj składniki i tworz mikstury, ale uważaj, z każdym kolejnym wyciętym drzewem tracisz część swojej energii, a zatem musisz się pospieszyć!";
            this.boxRectangle = new Rectangle(100, 50, 600, 150);


            base.Initialize();
        }

        public void InitializePositionBillboard(float width)
        {
            int count = 15;
            for (int i = (-count / 2); i < count / 2; i++)
            {
                positonBillboard.Add(new Vector3(i * 40, 20f, 130f)); //góra
                positonBillboard.Add(new Vector3(i * 40 - 20, 20f, 110f)); //góra
                                                                           //positonBillboard.Add(new Vector3(i * 25-10, 20f, -120f)); //
                                                                           //  positonBillboard.Add(new Vector3(i * 25, 20f, -130f)); //dół
                positonBillboard.Add(new Vector3(190f, 20f, i * 40));
                positonBillboard.Add(new Vector3(170f, 20f, i * 40 - 20));
                positonBillboard.Add(new Vector3(-190f, 20f, i * 40));
                positonBillboard.Add(new Vector3(-170f, 20f, i * 40 - 20));
            }
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            renderCapture = new RenderCapture(graphics.GraphicsDevice);
            postprocessor = new PostProcessor(Content.Load<Effect>("Objects\\Sepia"), graphics.GraphicsDevice);

            #region Effects

            Effect effect2 = Content.Load<Effect>("Objects\\PhongBlinnShader");
            Effect pointLight = Content.Load<Effect>("Objects\\PointLight");
            Effect spotLight = Content.Load<Effect>("Objects\\SpotLight");
            Effect transparency = Content.Load<Effect>("Objects\\WaterEffect");
            Effect albedo = Content.Load<Effect>("Objects\\Albedo");
            defaultMaterial = new DefaultMaterial(effect2);
            transparencyMaterial = new TransparencyMaterial(transparency);
            treemat = new DefaultMaterial(albedo);
            ((TransparencyMaterial)transparencyMaterial).DiffuseLightDirection = ((DefaultMaterial)defaultMaterial).LightDirection;
            //((DefaultMaterial) defaultMaterial).LightColor = pointLight1.GetComponent<Light>().Color.ToVector3();
            //((DefaultMaterial) defaultMaterial).LightDirection = pointLight1.GetComponent<Light>().Direction;
            //((DefaultMaterial)defaultMaterial).SpecularColor = pointLight1.GetComponent<Light>().Color.ToVector3();

            #endregion Effects

            mainMenuBackground = Content.Load<Texture2D>("Objects\\mainMenuBackground");
            logoScreen = Content.Load<Texture2D>("Objects\\gameNameScreen");

            //texture
            terren.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>("Objects\\terentestowyskala\\GRASS1");
          //  rock.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>("Objects\\ASSETY\\Rock1");
            //   water.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>("Objects\\HelicopterTexture");
            water.GetComponent<MeshRenderer>().Texture = waterModel.DiffuseTexture;
            water2.GetComponent<MeshRenderer>().Texture = waterModel.DiffuseTexture;
            //  modelTest.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>("Objects\\box\\Box001DiffuseMap");
            homeWitch.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>("Objects\\lowhouse_LOD1\\Textures\\Diffuse");
            modelTest2.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>("Objects\\kociolek\\Cauldron_02DiffuseMap");
            house2.GetComponent<MeshRenderer>().Texture = dom2Model.DiffuseTexture;
            house3.GetComponent<MeshRenderer>().Texture = dom2Model.DiffuseTexture;
            house4.GetComponent<MeshRenderer>().Texture = dom1Model.DiffuseTexture;
            alter.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>("Objects\\alter\\altarDiffuseMap");

            foreach (var treee in listTree)
            {
                treee.GetComponent<MeshRenderer>().Texture = treeModel.DiffuseTexture;
            }
            siano.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>("Objects\\siano\\Hay_bDiffuseMap");
            siano2.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>("Objects\\siano\\Hay_bDiffuseMap");

            ligthingObject.GetComponent<MeshRenderer>().Texture = MyContentManager.Load<Texture2D>("Objects\\cic\\koldiff");

            //noramls
            homeWitch.GetComponent<MeshRenderer>().NormalTexture = MyContentManager.Load<Texture2D>("Objects\\lowhouse_LOD1\\Textures\\Normals");
            modelTest2.GetComponent<MeshRenderer>().NormalTexture = MyContentManager.Load<Texture2D>("Objects\\kociolek\\Cauldron_02NormalsMap");
            water.GetComponent<MeshRenderer>().NormalTexture = waterModel.NormalTexture;
            water2.GetComponent<MeshRenderer>().NormalTexture = waterModel.NormalTexture;
            house2.GetComponent<MeshRenderer>().NormalTexture = dom2Model.NormalTexture;
            house3.GetComponent<MeshRenderer>().NormalTexture = dom2Model.NormalTexture;
            house4.GetComponent<MeshRenderer>().NormalTexture = dom1Model.NormalTexture;
            alter.GetComponent<MeshRenderer>().NormalTexture = MyContentManager.Load<Texture2D>("Objects\\alter\\altarNormalsMap");
            ligthingObject.GetComponent<MeshRenderer>().NormalTexture = MyContentManager.Load<Texture2D>("Objects\\cic\\kolnormal");

            siano.GetComponent<MeshRenderer>().NormalTexture = MyContentManager.Load<Texture2D>("Objects\\siano\\Hay_bNormalsMap");
            siano2.GetComponent<MeshRenderer>().NormalTexture = MyContentManager.Load<Texture2D>("Objects\\siano\\Hay_bNormalsMap");


            List<MeshRenderer> models = new List<MeshRenderer>();
            List<Light> lights = new List<Light>();

            foreach (GameObject o in scene.GetAllGameObjects())
            {
                if (o.ContainsComponent<MeshRenderer>())
                {
                    models.Add(o.GetComponent<MeshRenderer>());
                    o.GetComponent<MeshRenderer>().Material = defaultMaterial;
                }
                if (o.ContainsComponent<Light>())
                {
                    lights.Add(o.GetComponent<Light>());
                }
            }

            water.GetComponent<MeshRenderer>().Material = transparencyMaterial;
        //    foreach (var treee in listTree)
          //      treee.GetComponent<MeshRenderer>().Material = treemat;

            lightRenderer = new PrelightingRenderer(graphics.GraphicsDevice, Content);
            lightRenderer.ShadowLightPosition = new Vector3(-300, 700, -300);
            lightRenderer.ShadowLightTarget = new Vector3(0, 100, 0);
            lightRenderer.DoShadowMapping = true;
            lightRenderer.ShadowMult = 0.3f;
            //    scene = sceneManager.LoadScene("ScenaTestowa").Result;
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);
            mySpriteBatch = new MySpriteBatch(spriteBatch);

            // TODO: use this.Content to load your game content here
            Effect simpleEffectt = Content.Load<Effect>("Objects\\SpotLight");
            terren.GetComponent<MeshRenderer>().LoadModel("Objects\\terentestowyskala\\teren");

            terren.GetComponent<MeshRenderer>().SetModelEffect(simpleEffectt, true);

          //  rock.GetComponent<MeshRenderer>().LoadModel("Objects\\ASSETY\\kamieniecalyteren");
           // rock.AddComponent<BoxCollider>();

            // house1.GetComponent<MeshRenderer>().LoadModel("Objects\\DWADOMY\\dwadomy");
            // house1.AddComponent<BoxCollider>();
            house2.GetComponent<MeshRenderer>().Model = dom2Model.Model;
            house2.AddComponent<BoxCollider>();
            house3.GetComponent<MeshRenderer>().Model = dom2Model.Model;
            house3.AddComponent<BoxCollider>();
            house4.GetComponent<MeshRenderer>().Model = dom1Model.Model;
            house4.AddComponent<BoxCollider>();

            foreach (var treee in listTree)
            { 
                treee.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxMat");
                treee.AddComponent<BoxCollider>();
                treee.GetComponent<MeshRenderer>().Model = treeModel.Model;
            }

            homeWitch.GetComponent<MeshRenderer>().LoadModel("Objects\\lowhouse_LOD1\\lowdom");
            homeWitch.AddComponent<BoxCollider>();

            modelTest2.GetComponent<MeshRenderer>().LoadModel("Objects\\kociolek\\uvkociolek");
            modelTest2.AddComponent<BoxCollider>();

            siano.GetComponent<MeshRenderer>().LoadModel("Objects\\siano\\siano");
            siano.AddComponent<BoxCollider>();
            siano2.GetComponent<MeshRenderer>().LoadModel("Objects\\siano\\siano");
            siano2.AddComponent<BoxCollider>();

            // to nie działa
            //modelTest.GetComponent<Animation>().Model = MyContentManager.Load<Model>("wiesniakmixamo\\wiesniakwalkmixamo");

            // a to działą
            modelTest.GetComponent<Animation>().Model = MyContentManager.Load<Model>("wiesniakmixamo\\wiesniakrunpoprawa");

            modelTest.AddComponent<PathComponent>();
            pathFinder = PathDestinationFinder.Instance;

            GameObject destination = new GameObject(new Vector3(-10, 0, -40));
            InteractionSet interactionSet = new InteractionSet();
            Structure structure = new Structure()
            {
                Attributes = new WitchAttribiutes()
                {
                    currentHealthPoints = 20,
                    maxHealthPoints = 20
                },
                Interactions = interactionSet,
            };
            interactionSet.AddInterAction("HitMe", new HitMe());
            destination.AddComponent<StructureScript>();
            destination.GetComponent<StructureScript>().Structure = structure;

            GameObject destination1 = new GameObject(new Vector3(10, 0, 40));
            InteractionSet interactionSet1 = new InteractionSet();
            Structure structure1 = new Structure()
            {
                Attributes = new WitchAttribiutes()
                {
                    currentHealthPoints = 20,
                    maxHealthPoints = 20
                },
                Interactions = interactionSet1,
            };
            interactionSet1.AddInterAction("HitMe", new HitMe());
            destination1.AddComponent<StructureScript>();
            destination1.GetComponent<StructureScript>().Structure = structure1;

            //pathFinder.AddDestinationTargetRange(DestinationTarget.House, new List<GameObject>() { destination, destination1 });
            pathFinder.AddDestinationTargetRange(DestinationTarget.House, listHouse);
            pathFinder.AddDestinationTargetRange(DestinationTarget.Tree, listTree);
            scene.AddGameObject(destination);
            //pathFinder = new PathDestinationFinder(new List<GameObject>() { new GameObject(new Vector3(-10, 0, -40)), new GameObject(new Vector3(10, 0, 40)) });

            //playerWitch.GetComponent<Animation>().Model = MyContentManager.Load<Model>("Objects\\wiesmac\\wiesm");
            //playerWitch.GetComponent<Animation>().Model = MyContentManager.Load<Model>("Objects\\Witch-anim\\witchnowabez3szieletow");
            playerWitch.GetComponent<Animation>().Model = MyContentManager.Load<Model>("Objects\\witchStay\\witchstay2");

            //  playerWitch.GetComponent<Animation>().Model = MyContentManager.Load<Model>("cyclop_atak\\mixamodemon");

            //  playerWitch.GetComponent<Animation>().Model = MyContentManager.Load<Model>("demonmxamowalk\\1");
            //  playerWitch.GetComponent<Animation>().Model = MyContentManager.Load<Model>("wiesniakmixamo\\wiesniakfallmixamo");
            // playerWitch.GetComponent<Animation>().Model = MyContentManager.Load<Model>("wiesniakmixamo\\wiesniakrunmixamo");

             // playerWitch.GetComponent<Animation>().Model = MyContentManager.Load<Model>("wiesniakmixamo\\wiesniakcutcut");

            //  modelTest.AddComponent<BoxCollider>();
            // modelTest.AddComponent<SphereCollider>();

            //      modelTest2.AddComponent<SphereCollider>();

            ligthingObject.GetComponent<MeshRenderer>().LoadModel("Objects\\cic\\kol");

            alter.GetComponent<MeshRenderer>().LoadModel("Objects\\alter\\altar");
            alter.AddComponent<BoxCollider>();

            playerWitch.AddComponent<BoxCollider>();
            //  playerWitch.AddComponent<SphereCollider>();
            playerWitch.AddComponent<Camera>();

            mainCam = playerWitch.GetComponent<Camera>();
            playerWitch.AddComponent<Movement>();
            playerWitch.AddComponent<CameraFollow>();
            playerWitch.AddComponent<CauldronScript>();
            playerWitch.AddComponent<WitchScript>();

            farmtile.AddComponent<FarmScript>();
            farmtile.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxMat");
            farmtile.GetComponent<FarmScript>().PlantSeed(new Plant(12, "carrot", new HealthRestoration(new SimpleEffect()), 10000));
            farmtile.AddComponent<BoxCollider>();
            farmtile.AddComponent<Colider>();
            farmtile.GetComponent<Colider>().IsTrigger = true;

            farmtile2.AddComponent<FarmScript>();
            farmtile2.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxMat");
            farmtile2.GetComponent<FarmScript>().PlantSeed(new Plant(11, "potato", new MinorManaRestoration(new SimpleEffect()), 10000));
            farmtile2.AddComponent<BoxCollider>();
            farmtile2.AddComponent<Colider>();
            farmtile2.GetComponent<Colider>().IsTrigger = true;

            farmtile3.AddComponent<FarmScript>();
            farmtile3.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxMat");
            farmtile3.GetComponent<FarmScript>().PlantSeed(new Plant(12, "carrot", new HealthRestoration(new SimpleEffect()), 10000));
            farmtile3.AddComponent<BoxCollider>();
            farmtile3.AddComponent<Colider>();
            farmtile3.GetComponent<Colider>().IsTrigger = true;

            farmtile4.AddComponent<FarmScript>();
            farmtile4.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxMat");
            farmtile4.GetComponent<FarmScript>().PlantSeed(new Plant(11, "potato", new MinorManaRestoration(new SimpleEffect()), 10000));
            farmtile4.AddComponent<BoxCollider>();
            farmtile4.AddComponent<Colider>();
            farmtile4.GetComponent<Colider>().IsTrigger = true;

            farmtile5.AddComponent<FarmScript>();
            farmtile5.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxMat");
            farmtile5.GetComponent<FarmScript>().PlantSeed(new Plant(11, "pumpkin", new MinorManaRestoration(new SimpleEffect()), 5000));
            farmtile5.AddComponent<BoxCollider>();
            farmtile5.AddComponent<Colider>();
            farmtile5.GetComponent<Colider>().IsTrigger = true;

            farmtile6.AddComponent<FarmScript>();
            farmtile6.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxMat");
            farmtile6.GetComponent<FarmScript>().PlantSeed(new Plant(11, "mushroom", new HealthRestoration(new SimpleEffect()), 5000));
            farmtile6.AddComponent<BoxCollider>();
            farmtile6.AddComponent<Colider>();
            farmtile6.GetComponent<Colider>().IsTrigger = true;

            farmtile7.AddComponent<FarmScript>();
            farmtile7.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxMat");
            farmtile7.GetComponent<FarmScript>().PlantSeed(new Plant(11, "flower", new HealthRestoration(new SimpleEffect()), 5000));
            farmtile7.AddComponent<BoxCollider>();
            farmtile7.AddComponent<Colider>();
            farmtile7.GetComponent<Colider>().IsTrigger = true;

            farmtile8.AddComponent<FarmScript>();
            farmtile8.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxMat");
            farmtile8.GetComponent<FarmScript>().PlantSeed(new Plant(11, "flower2", new MinorManaRestoration(new SimpleEffect()), 5000));
            farmtile8.AddComponent<BoxCollider>();
            farmtile8.AddComponent<Colider>();
            farmtile8.GetComponent<Colider>().IsTrigger = true;

            farmtile9.AddComponent<FarmScript>();
            farmtile9.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxMat");
            farmtile9.GetComponent<FarmScript>().PlantSeed(new Plant(11, "flower", new HealthRestoration(new SimpleEffect()), 5000));
            farmtile9.AddComponent<BoxCollider>();
            farmtile9.AddComponent<Colider>();
            farmtile9.GetComponent<Colider>().IsTrigger = true;

            playerWitch.GetComponent<Animation>().LoadAnimation("Take 001");
            //testAnimacja.GetComponent<Animation>().LoadAnimation("jump");

            playerWitch.GetComponent<InventoryScript>().AddItem(new Item(1, "carrot", new HealthRestoration(new SimpleEffect())));
            playerWitch.GetComponent<InventoryScript>().AddItem(new Item(1, "carrot", new HealthRestoration(new SimpleEffect())));
            playerWitch.GetComponent<InventoryScript>().AddItem(new Item(1, "carrot", new HealthRestoration(new SimpleEffect())));
            playerWitch.AddComponent<HarverstingScript>();
            playerWitch.AddComponent<RitualScript>();
            //AddItem(new Item(6, "carrot", new HealthRestoration(new SimpleEffect())));
            //playerWitch.GetComponent<InventoryScript>().AddItem(new Item(2, "potato", new HealthRestoration(new SimpleEffect())));
            //playerWitch.GetComponent<InventoryScript>().AddItem(new Item(6, "carrot", new HealthRestoration(new SimpleEffect())));
            //playerWitch.GetComponent<InventoryScript>().AddItem(new Item(2, "potato", new HealthRestoration(new SimpleEffect())));
            //playerWitch.GetComponent<InventoryScript>().AddItem(new Item(6, "carrot", new HealthRestoration(new SimpleEffect())));
            inventoryDraw = new InventoryDraw(this, mainCam, playerWitch.GetComponent<InventoryScript>());
            cauldronDraw = new CauldronDraw(this, mainCam, playerWitch.GetComponent<CauldronScript>());
            alterDraw = new AlterDraw(this, mainCam, playerWitch.GetComponent<RitualScript>());
            dragAndDropScript = new DragAndDropScript(playerWitch.GetComponent<InventoryScript>(), playerWitch.GetComponent<CauldronScript>(), playerWitch.GetComponent<RitualScript>(),
                                    inventoryDraw.InventoryPosition, cauldronDraw.CauldronListPosition, cauldronDraw.CauldronTextPosition,
                                    alterDraw.AlterListPosition, alterDraw.AlterTextPosition);

            wallUp.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxUp");
            wallUp.AddComponent<BoxCollider>();
            wallDown.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxUp");
            wallDown.AddComponent<BoxCollider>();
            wallLeft.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxLeft");
            wallLeft.AddComponent<BoxCollider>();
            wallRight.GetComponent<MeshRenderer>().LoadModel("Objects\\box\\boxLeft");
            wallRight.AddComponent<BoxCollider>();

            this.song = Content.Load<Song>("Objects\\Audio\\Theme");
            MediaPlayer.Play(song);
            MediaPlayer.IsRepeating = true;
            audioManager.AddSoundEffect((SoundEffect)Content.Load<SoundEffect>("Objects\\Audio\\HealthPotionSound"), "drinkHealthPotion");
            audioManager.AddSong(Content.Load<Song>("Objects\\Audio\\Theme"));
             audioManager.PlaySong();

            texture = Content.Load<Texture2D>("Objects\\treeBo\\3");
            effect = Content.Load<Effect>("Objects\\Albedo");
            effect.CurrentTechnique = effect.Techniques["Albedo"];
            setListBillboard(positonBillboard);

            userInterfaceDraw = new UserInterface(this, mainCam);
            //manaLifeBars = Content.Load<Texture2D>("Objects\\bars\\barspr1");
            menuInGame = new MenuInGame(this, mainCam, new Vector2(450, 150));
            quitButton = new Button(Content.Load<Texture2D>("Objects\\buttons\\quitButton"), new Vector2(650, 400), this, mainCam);
            playButton = new Button(Content.Load<Texture2D>("Objects\\buttons\\playButton"), new Vector2(650, 300), this, mainCam);
            quitGameButton = new Button(Content.Load<Texture2D>("Objects\\buttons\\quitButton"), new Vector2(1040, 600), this, mainCam);
            playGameButton = new Button(Content.Load<Texture2D>("Objects\\buttons\\playButton"), new Vector2(1040, 500), this, mainCam);

            dialogBox = new DialogBox(this, mainCam);
            dialogDraw = new DialogDraw(this, mainCam, dialogBox);
            witchDialogImage = new DialogImage(this, mainCam, Content.Load<Texture2D>("Objects\\dialogImages\\witchSprite"), "witch");
            demonDialogImage = new DialogImage(this, mainCam, Content.Load<Texture2D>("Objects\\dialogImages\\demonSprite"), "demon");
            wiesniakDialogImage = new DialogImage(this, mainCam, Content.Load<Texture2D>("Objects\\dialogImages\\wiesniakSprite"), "wiesniak");
            dialogImagesList.Add(witchDialogImage);
            dialogImagesList.Add(demonDialogImage);
            dialogImagesList.Add(wiesniakDialogImage);
            dialogLoader = new DialogLoader(dialogImagesList, dialogBox, dialogDraw);
            redGeckoLogo = Content.Load<Texture2D>("Objects\\RedGeckoLogo");
            victoryScreen = Content.Load<Texture2D>("Objects\\Victory");
            gameOverScreen = Content.Load<Texture2D>("Objects\\GameOver");
            this.dialogFont = MyContentManager.Load<SpriteFont>("Objects\\DialogFont");
            //   water.GetComponent<MeshRenderer>().LoadModel("Objects\\Helicopter\\Helicopter");
            water.GetComponent<MeshRenderer>().Model = waterModel.Model;
            water2.GetComponent<MeshRenderer>().Model = waterModel.Model;

            colliderManager = new ColliderManager(scene.GetAllGameObjects());
            colliderManager.ObjectColided += playerWitch.OnObjectColided;

            modelTest.GetComponent<PathComponent>().SetDestination(pathFinder.GetRandomTarget(DestinationTarget.House));
            modelTest.AddComponent<BoxCollider>();
            modelTest.GetComponent<PathComponent>().FindPath();
            modelTest.GetComponent<PathComponent>().StartGoPath();
            modelTest.GetComponent<Animation>().LoadAnimation("Take 001");

            playerWitch.AddComponent<DialogEvents>();

            informationWindow = new InformationWindow(this, mainCam);

            lightRenderer.Camera = mainCam;
            lightRenderer.Lights = lights;
            lightRenderer.Models = models;
        }

        public void setListBillboard(List<Vector3> positonBillboard)
        {
            foreach (Vector3 x in positonBillboard)
                listBillboard.Add(new Billboard(GraphicsDevice, 28f, 28f, x, texture));
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            try
            {
                switch (scenesEnum)
                {
                    case ScenesEnum.Logo:
                        IsMouseVisible = true;
                        var mouseState = Mouse.GetState();
                        if (mouseState.LeftButton == ButtonState.Pressed)
                        {
                            scenesEnum = ScenesEnum.MainMenu;
                        }
                        break;

                    case ScenesEnum.MainMenu:
                        IsMouseVisible = true;
                        quitGameButton.Update();
                        playGameButton.Update();
                        if (playGameButton.isClicked)
                        {
                            scenesEnum = ScenesEnum.GameScene;
                            playGameButton.isClicked = false;
                        }
                        if (quitGameButton.isClicked)
                            Exit();

                            break;

                        case ScenesEnum.GameScene:
                            if (Input.GetKeyDown(Keys.Escape) && ibuttonStatePaused == IButtonState.None)
                            {
                                shouldisPaused = !shouldisPaused;
                                ibuttonStatePaused = IButtonState.IsBeingPressd;
                                isPaused = !isPaused;
                            }
                            if (Input.GetKeyReleased(Keys.Escape) && ibuttonStatePaused == IButtonState.IsBeingPressd)
                            {
                                ibuttonStatePaused = IButtonState.None;
                            }

                        if (Input.GetKeyDown(Keys.Space) && ibuttonStateDialog == IButtonState.None)
                        {
                            shouldGoWithDialog = !shouldGoWithDialog;
                            ibuttonStateDialog = IButtonState.IsBeingPressd;
                            verse++;
                            //particleStarted = true;
                        }
                        if (Input.GetKeyReleased(Keys.Space) && ibuttonStateDialog == IButtonState.IsBeingPressd)
                        {
                            ibuttonStateDialog = IButtonState.None;
                        }

                        if (Input.GetKeyDown(Keys.I) && ibuttonState == IButtonState.None)
                            {
                                shouldDrawInventory = !shouldDrawInventory;
                                ibuttonState = IButtonState.IsBeingPressd;
                                boolInventory = !boolInventory;
                            }
                            if (Input.GetKeyReleased(Keys.I) && ibuttonState == IButtonState.IsBeingPressd)
                            {
                                ibuttonState = IButtonState.None;
                            }

                            if (isPaused)
                            {
                                quitButton.Update();
                                playButton.Update();
                                audioManager.StopSong();
                                if (playButton.isClicked)
                                {
                                    isPaused = false;
                                    shouldisPaused = false;
                                    playButton.isClicked = false;
                                }
                                if (quitButton.isClicked)
                                {
                                    scenesEnum = ScenesEnum.MainMenu;
                                    quitButton.isClicked = false;
                                }
                                return;
                            }
                            //Exit();

                            colliderManager.Update();

                            if (boolCauldron || boolInventory || boolAlter)
                            {
                                dragAndDropScript.Update(boolInventory, boolCauldron, boolAlter);
                                cauldronDraw.Update();
                                alterDraw.Update();
                            }
                            else
                                IsMouseVisible = false;

                            //if (Input.GetKeyDown(Keys.T))
                            //    playerWitch.GetComponent<InventoryScript>().AddItem(new Item(3, "carrot", new HealthRestoration(new SimpleEffect())));

                            scene.Update(gameTime);
                        // TODO: Add your update logic here
                        //ParticleRenderer.Instance.Update(gameTime, mainCam);
                        ParticleRenderer.Instance.Update(gameTime, mainCam);
                        //ParticleRenderer.Instance.StartParticles(playerWitch, mainCam, gameTime);
                        //if (particleStarted)
                        //{
                            
                        //}
                            
                        //particleEngine.EmitterLocation = new Vector2(400, 220);
                        if (createParticleEngine)
                        {
                            CreateParticlesEngineInstance();
                            
                        }

                        if (particleStarted)
                        {
                            ParticleRenderer.Instance.particlesActive = true;
                            ParticleRenderer.Instance.StartParticles(playerWitch, mainCam, gameTime, 5, tex);
                            particleStarted = false;
                        }

                        if (particleEngine != null)
                            {
                            //particleEngine.Update();
                            
                                


                            if (particleEngine.getParticleEngineLifetime() <= 0)
                                    this.particleEngine = null;
                                ParticleRenderer.Instance.particlesActive = false;
                            }
                            userInterfaceDraw.Update(playerWitch);
                        //menuInGame.Update(gameTime);
                        //particles3D.CreateBBVertices(mainCam);
                        

                        break;
                    case ScenesEnum.Victory:
                        break;
                    case ScenesEnum.GameOver:
                        break;
                    case ScenesEnum.End:
                            break;
                }
            }
            catch (Victory victory)
            {
                //TODO: tutaj zamiast throw poprosze jakas obsługę do tego np załadowanie scenny innej z komunikatem ze sie wygrało
                scenesEnum = ScenesEnum.Victory;
            }
            catch (GameOver gameOver)
            {
                //TODO: tutaj zamiast throw poprosze jakas obsługę do tego np załadowanie scenny innej z komunikatem ze sie przegało
                scenesEnum = ScenesEnum.GameOver;
            }
            catch (Exception otherException)
            {
                logger.Error(otherException, message: "Unexpected Exception");
            }
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            switch (scenesEnum)
            {
                case ScenesEnum.Logo:
                    spriteBatch.Begin();
                    spriteBatch.Draw(logoScreen, new Rectangle(500, 350, logoScreen.Width, logoScreen.Height), null,  Color.White);
                    spriteBatch.Draw(redGeckoLogo, new Rectangle(580, 600, redGeckoLogo.Width/4, redGeckoLogo.Height/4), null,  Color.White);
                    spriteBatch.End();
                    break;

                case ScenesEnum.MainMenu:
                    renderCapture.Begin();
                    GraphicsDevice.Clear(Color.CornflowerBlue);
                    //  GraphicsDevice.Clear(Color.White);
                    GraphicsDevice.BlendState = BlendState.Opaque;
                    GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                    GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;

                    spriteBatch.Begin();
                    spriteBatch.Draw(mainMenuBackground, new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.White);
                    spriteBatch.Draw(logoScreen, new Rectangle(940, 200, logoScreen.Width, logoScreen.Height), null, Color.White);
                    
                    spriteBatch.End();
                    mainMenu = new MenuInGame(this, mainCam, new Vector2(850, 350));
                    //quitButton = new Button(Content.Load<Texture2D>("Objects\\buttons\\quitButton"), new Vector2(350, 300), this, mainCam);
                    mainMenu.Draw(gameTime);
                    playGameButton.Draw();
                    quitGameButton.Draw();

                    spriteBatch.Begin();
                    
                    spriteBatch.Draw(MyContentManager.Load<Texture2D>("Objects\\dialogBoxBackground"), new Rectangle(100, 50, 600, 250), Color.White);
                    spriteBatch.DrawString(dialogFont, parseText(textToDraw), new Vector2(boxRectangle.X + 20, boxRectangle.Y + 20), Color.White);

                    spriteBatch.End();
                    // End capturing
                    renderCapture.End();
                    GraphicsDevice.Clear(Color.Black);
                    // Perform postprocessing with the render of the scene
                    postprocessor.Input = renderCapture.GetTexture();
                    postprocessor.Draw();

                    
                    break;

                case ScenesEnum.GameScene:

                    lightRenderer.Draw();

                    //odpwoedzialny za rysowanie rzeczy w 3d
                    //  renderCapture.Begin();
                    // GraphicsDevice.Clear(Color.CornflowerBlue);

                    scene.Draw(mainCam);

                    ParticleRenderer.Instance.Draw(mainCam);
                    //   graphics.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                    //  modelTest.Draw(mainCam);
                    //  graphics.GraphicsDevice.BlendState = BlendState.Opaque;

                    //        playerWitch.GetComponent<SphereCollider>().DrawSphereSpikes(playerWitch.GetComponent<SphereCollider>().GetPreciseBoundingSpheres()[0], graphics.GraphicsDevice, playerWitch.GetComponent<Animation>().GetMatrix(), playerWitch.GetComponent<Camera>().view, projection);

                    foreach (Billboard x in listBillboard)
                        x.DrawBillboard(mainCam, effect);

                    //       modelTest.GetComponent<SphereCollider>().DrawSphereSpikes(modelTest.GetComponent<SphereCollider>().GetPreciseBoundingSpheres()[0], graphics.GraphicsDevice, modelTest.GetComponent<MeshRenderer>().GetMatrix(), mainCam.view, projection);
                    //     modelTest2.GetComponent<SphereCollider>().DrawSphereSpikes(playerWitch.GetComponent<SphereCollider>().GetPreciseBoundingSpheres()[0], graphics.GraphicsDevice, modelTest2.GetComponent<MeshRenderer>().GetMatrix(), mainCam.view, projection);
                    //najpierw rysujemy wszytsko co jest 3d a później to co jest 2d i wyłączamy Z buffor

                    if (shouldDrawInventory)
                        inventoryDraw.Draw(gameTime);

                    cauldronDraw.Draw(gameTime);
                    alterDraw.Draw(gameTime);

                    userInterfaceDraw.Draw(gameTime);
                    if (shouldisPaused)
                    {
                        menuInGame.Draw(gameTime);
                        quitButton.Draw();
                        playButton.Draw();
                    }
                    //particles3D.Draw(mainCam, GraphicsDevice);
                    if (particleEngine != null)
                    {
                       // ParticleRenderer.Instance.StartParticles(playerWitch, mainCam, gameTime);
                    }
                           
                        //particleEngine.Draw(spriteBatch);

                    dragAndDropScript.Draw();

                    GraphicsDevice.BlendState = BlendState.Opaque;
                    GraphicsDevice.DepthStencilState = DepthStencilState.Default;
                    GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;
                    if (isDialogActive)
                    {
                        dialogLoader.StartDialog(gameTime, dialogNumber, verse);
                    }

                    if (isInformationWindowOpened)
                    {
                        informationWindow.InformationShowStart(gameTime, infoMessage);
                    }

                    graphics.GraphicsDevice.BlendState = BlendState.AlphaBlend;
                    water.Draw(mainCam); // i tu rysujemy nasz model, który ma efekt przezroczystości
                    graphics.GraphicsDevice.BlendState = BlendState.Opaque;

                    //if (isDialogActive)
                    //{
                    //    //dialogLoader.Update(true);
                    //    dialogLoader.StartDialog(gameTime, 2);
                    //    isDialogActive = false;

                    //}


                    // End capturing
                    //renderCapture.End();
                    //GraphicsDevice.Clear(Color.Black);
                    // Perform postprocessing with the render of the scene
                    // postprocessor.Input = renderCapture.GetTexture();
                    //postprocessor.Draw();

                    break;
                case ScenesEnum.Victory:
                    spriteBatch.Begin();
                    spriteBatch.Draw(victoryScreen, new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.White);
                    spriteBatch.End();
                    break;
                case ScenesEnum.GameOver:
                    spriteBatch.Begin();
                    spriteBatch.Draw(gameOverScreen, new Rectangle(0, 0, graphics.PreferredBackBufferWidth, graphics.PreferredBackBufferHeight), Color.White);
                    spriteBatch.End();
                    break;
                case ScenesEnum.End:
                    break;
            }

            base.Draw(gameTime);
        }

        public void CreateParticlesEngineInstance()
        {
            if (particleEngine == null)
            {
                List<Texture2D> textures = new List<Texture2D>();
                textures.Add(Content.Load<Texture2D>("Objects\\Particles\\circle"));
                textures.Add(Content.Load<Texture2D>("Objects\\Particles\\star"));
                textures.Add(Content.Load<Texture2D>("Objects\\Particles\\diamond"));
                particleEngine = new ParticleEngine(textures, new Vector2(playerWitch.Transform.position.X, playerWitch.Transform.position.Z));
                createParticleEngine = false;
            }
        }

        public void LoadModel()
        {
            treeModel = new ModelDictionary("Objects\\tree\\tree", "Objects\\tree\\Foliage001DiffuseMap", "Objects\\tree\\Foliage001NormalsMap");
            dom1Model = new ModelDictionary("Objects\\domb\\domb", "Objects\\domb\\Building_bDiffuseMap", "Objects\\domb\\Building_bNormalsMap");
            dom2Model = new ModelDictionary("Objects\\domc\\domc", "Objects\\domc\\Building_cDiffuseMap", "Objects\\domc\\Building_cNormalsMap");
            waterModel = new ModelDictionary("water\\water", "water\\Cylinder001DiffuseMap", "water\\WaterNormalsMap");
        }

        private String parseText(String text)
        {
            String line = String.Empty;
            String returnString = String.Empty;
            String[] wordArray = text.Split(' ');

            foreach (String word in wordArray)
            {
                if (dialogFont.MeasureString(line + word).Length() > boxRectangle.Width - 20)
                {
                    returnString = returnString + line + '\n';
                    line = String.Empty;
                }

                line = line + word + ' ';
            }

            return returnString + line;
        }
    }
}