﻿float4x4 WorldMatrix;
float4x4 ViewMatrix;
float4x4 ProjectionMatrix;

texture2D BasicTexture;
sampler2D basicTextureSampler = sampler_state
{
	texture = <BasicTexture>;
	addressU = wrap;
	addressV = wrap;
	minfilter = anisotropic;
	magfilter = anisotropic;
	mipfilter = linear;
};
bool TextureEnabled = false;

texture2D LightTexture;
sampler2D LightTextureSampler = sampler_state
{
	texture = <LightTexture>;
	minfilter = point;
	magfilter = point;
	mipfilter = point;
};

bool DoShadowMapping = true;
float4x4 ShadowView;
float4x4 ShadowProjection;
texture2D ShadowMap;
sampler2D shadowSampler = sampler_state {
	texture = <ShadowMap>;
	minfilter = point;
	magfilter = point;
	mipfilter = point;
};

float3 ShadowLightPosition;
float ShadowFarPlane;
float ShadowMult = 0.3f;
float ShadowBias = 1.0f / 50.0f;


float4 AmbientColor = float4(1, 1, 1, 1);
float AmbientIntensity = 0.5;

float3 LightDirection;
float4 DiffuseColor = float4(1, 1, 1, 1);
float DiffuseIntensity = 0.4;

float4 SpecularColor = float4(1, 1, 1, 1);
float SpecularIntensity = 0.3;

float3 CamPosition;

#include "PPShared.fxh"

struct VertexShaderInput
{
	float4 Position : POSITION;
	float2 UV : TEXCOORD0;
	float3 Normal : NORMAL;
	//	float3 View : TEXCOORD1;
};


struct VertexShaderOutput
{
	float4 Position : POSITION;
	float2 UV : TEXCOORD0;
	float4 PositionCopy : TEXCOORD1;
	float3 Normal : NORMAL;
	float3 View : TEXCOORD2;
	float4 ShadowScreenPosition : TEXCOORD3;
};

//			Blinn
VertexShaderOutput VertexShaderFunction(in VertexShaderInput input)
{
	VertexShaderOutput output;

	float4 worldPosition = mul(input.Position, WorldMatrix);
	float4 viewPosition = mul(worldPosition, ViewMatrix);
	output.Position = mul(viewPosition, ProjectionMatrix);
	float3 normal = normalize(mul(input.Normal, WorldMatrix));
	output.Normal = normal;
	output.View = normalize(float4(CamPosition, 1.0) - worldPosition);

	output.PositionCopy = output.Position;
	output.UV = input.UV;

	output.ShadowScreenPosition = mul(mul(input.Position, WorldMatrix),
		mul(ShadowView, ShadowProjection));

	return output;
}

float sampleShadowMap(float2 UV)
{
	if (UV.x < 0 || UV.x > 1 || UV.y < 0 || UV.y > 1)
		return 1;
	return tex2D(shadowSampler, UV).r;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR0
{
	// Sample model's texture
	float3 basicTexture = tex2D(basicTextureSampler, input.UV);

	if (!TextureEnabled)
		basicTexture = float4(1, 1, 1, 1);

	// Extract lighting value from light map
	float2 texCoord = postProjToScreen(input.PositionCopy) +
		halfPixel();
	float3 light = tex2D(LightTextureSampler, texCoord);

	float4 normal = float4(input.Normal, 1.0);
	float4 diffuse = saturate(dot(-LightDirection,normal));
	float4 reflect = normalize(2 * diffuse*normal - float4(LightDirection, 1.0));
	float4 specular = pow(saturate(dot(reflect, input.View)),15);

	light += AmbientColor * AmbientIntensity;

	float4 BlinnColor = DiffuseIntensity * DiffuseColor * diffuse + SpecularIntensity*SpecularColor*specular;

	float2 shadowTexCoord = postProjToScreen(input.ShadowScreenPosition)
		+ halfPixel();
	float mapDepth = sampleShadowMap(shadowTexCoord);

	float realDepth = input.ShadowScreenPosition.z / ShadowFarPlane;
	float shadow = 1;
	if (realDepth < 1 && realDepth - ShadowBias > mapDepth)
		shadow = ShadowMult;
	return float4(basicTexture * BlinnColor * light * shadow, 1);
	
}

technique Blinn
{
	pass P0
	{
		VertexShader = compile vs_5_0 VertexShaderFunction();
		PixelShader = compile ps_5_0 PixelShaderFunction();
	}

	/*pass P1
	{
	VertexShader = compile VS_SHADERMODEL ShadowMapVS();
	PixelShader = compile PS_SHADERMODEL ShadowMapPS();
	}*/
};