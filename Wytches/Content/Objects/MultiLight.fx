float4x4 WorldMatrix;
float4x4 ViewMatrix;
float4x4 ProjectionMatrix;

#define NUMLIGHTS 3

float3 AmbientColor = float3(.15, .15, .15);
float3 DiffuseColor = float3(.85, .85, .85);
float3 LightPosition[NUMLIGHTS];
float3 LightDirection[NUMLIGHTS];
float SpecularPower = 32;
float3 SpecularColor = float3(1, 1, 1);

texture BasicTexture;
sampler BasicTextureSampler = sampler_state {
	texture = <BasicTexture>;
	MinFilter = Anisotropic; // Minification Filter
	MagFilter = Anisotropic; // Magnification Filter
	MipFilter = Linear; // Mip-mapping
	AddressU = Wrap; // Address Mode for U Coordinates
	AddressV = Wrap; // Address Mode for V Coordinates
};

bool TextureEnabled = false;

struct VertexShaderInput
{
	float4 Position : POSITION0;
	float2 UV : TEXCOORD0;
	float3 Normal : NORMAL0;
};

struct VertexShaderOutput
{
	float4 Position : POSITION0;
	float2 UV : TEXCOORD0;
	float3 Normal : TEXCOORD1;
	float3 ViewDirection : TEXCOORD2;
};

VertexShaderOutput VertexShaderFunction(in VertexShaderInput input)
{
	VertexShaderOutput output;
	float4 worldPosition = mul(input.Position, WorldMatrix);
	float4 viewPosition = mul(worldPosition, ViewMatrix);
	output.Position = mul(viewPosition, ProjectionMatrix);
	output.WorldPosition = worldPosition;
	output.UV = input.UV;
	output.Normal = mul(input.Normal, WorldMatrix);
	return output;
}

float4 PixelShaderFunction(VertexShaderOutput input) : COLOR
{
	// Start with diffuse color
	float3 color = DiffuseColor;

	// Texture if necessary
	if (TextureEnabled)
		color *= tex2D(BasicTextureSampler, input.UV);

	// Start with ambient lighting
	float3 lighting = AmbientColor;

	float3 normal = normalize(input.Normal);
	float3 view = normalize(input.ViewDirection);

	// Perform lighting calculations per light
	for (int i = 0; i < NUMLIGHTS; i++)
	{
		float3 lightDir = normalize(LightDirection[i]);
		// Add lambertian lighting
		lighting += saturate(dot(lightDir, normal)) * LightColor[i];
		float3 refl = reflect(lightDir, normal);
		// Add specular highlights
		lighting += pow(saturate(dot(refl, view)), SpecularPower)
			* SpecularColor;
	}

	// Calculate final color
	float3 output = saturate(lighting) * color;

	return float4(output, 1);
}

technique MultiLight
{
	pass
	{
		VertexShader = compile vs_5_0 VertexShaderFunction();
		PixelShader = compile ps_5_0 PixelShaderFunction();
	}
};