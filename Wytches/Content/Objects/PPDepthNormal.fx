﻿float4x4 WorldMatrix;
float4x4 ViewMatrix;
float4x4 ProjectionMatrix;


texture2D NormalTexture;
sampler2D normalSampler = sampler_state
 {
	Texture = <NormalTexture>;
	Minfilter = linear;
	MagFilter = linear;
	MipFilter = linear;
	AddressU = Wrap;
	AddressV = Wrap;
	};
bool NormalEnabled = false;



struct VertexShaderInput
{
	float4 Position : POSITION0;
	float3 Normal : NORMAL0;
	float2 UV : TEXCOORD0;
};

struct VertexShaderOutput
{
	float4 Position : POSITION0;
	float2 UV : TEXCOORD0;
	float2 Depth : TEXCOORD1;
	float3 Normal : TEXCOORD2;
	
};

struct PixelShaderOutput
{
	float4 Normal : COLOR0;
	float4 Depth : COLOR1;
};

VertexShaderOutput VertexShaderFunction(in VertexShaderInput input)
{
	VertexShaderOutput output;

	float4x4 viewProjection = mul(ViewMatrix, ProjectionMatrix);
	float4x4 worldViewProjection = mul(WorldMatrix, viewProjection);

	output.Position = mul(input.Position, worldViewProjection);

	output.UV = input.UV;

	output.Normal = mul(input.Normal, WorldMatrix);
	output.Depth.xy = output.Position.zw;

	return output;
}

PixelShaderOutput PixelShaderFunction(VertexShaderOutput input)
{
	PixelShaderOutput output = (PixelShaderOutput)0;

	float4 color;

	color = input.Depth.x / input.Depth.y;

	float3 normal = tex2D(normalSampler, input.UV).rgb;
	normal = normal * 2 - 1;
	
		if (!NormalEnabled)
		normal = float3(1, 1, 1);
	
		output.Normal.xyz = (normalize(input.Normal * normal).xyz / 2) + .5;

	color.a = 1;
	output.Normal.a = 1;

	output.Depth = color;

	return output;
}

technique DepthNormal
{
	pass P0
	{
		VertexShader = compile vs_5_0 VertexShaderFunction();
		PixelShader = compile ps_5_0 PixelShaderFunction();
	}
};
