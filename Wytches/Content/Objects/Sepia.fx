
texture ScreenTexture;
 

sampler TextureSampler = sampler_state
{
    Texture = <ScreenTexture>;
};

struct VertexShaderOutput
{
    float4 Position : SV_Position;
    float4 Color : COLOR0;
    float2 TextureCoordinate : TEXCOORD0;
};
 

float4 PixelShaderFunction(VertexShaderOutput i) : COLOR0
{
    float4 color = tex2D(TextureSampler, i.TextureCoordinate);
   
//	 float intensity = 0.3f * color.r
//+ 0.59f * color.g
//+ 0.11f * color.b;
//    return float4(intensity, intensity, intensity, color.a);
	float4 outputColor = color;
	outputColor.r = (color.r * 0.393) + (color.g * 0.769) + (color.b * 0.189);
	outputColor.g = (color.r * 0.349) + (color.g * 0.686) + (color.b * 0.168);
	outputColor.b = (color.r * 0.272) + (color.g * 0.534) + (color.b * 0.131);
 
    return outputColor;
}


technique Sepia
{
    pass Pass1
    {
        PixelShader = compile ps_5_0 PixelShaderFunction();
    }
}
