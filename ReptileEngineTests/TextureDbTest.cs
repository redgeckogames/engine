﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wytches.Scripts;
using FluentAssertions;

namespace ReptileEngineTests
{
    [TestClass]
    public class TextureDbTest
    {
        private TextureDB textures;

        [TestInitialize]
        public void SetUp()
        {
            textures = new TextureDB("TextureDB.txt");
        }

        [TestMethod]
        public void GetTexturePath()
        {
            TextureDB.AddTexture("TestItem", "TestitemTexture");

            TextureDB.GetTexturePath("TestItem").ShouldAllBeEquivalentTo("TestitemTexture");
        }

        [TestMethod]
        [ExpectedException(typeof(ItemTextureNameException))]
        public void TexturealreadyMapped()
        {
            TextureDB.AddTexture("TestItem", "TestitemTexture");
            TextureDB.AddTexture("TestItem", "DiffrentPath");
        }

        [TestMethod]
        [ExpectedException(typeof(TexturePathNotFoundException))]
        public void TextureDoesNotExist()
        {
            TextureDB.AddTexture("TestItem", "TestitemTexture");

            TextureDB.GetTexturePath("WrongItemName");
        }

        [TestMethod]
        public void FileProperlyLoaded()
        {
            TextureDB.GetTexturePath("Apple").ShouldBeEquivalentTo("Fruit/apple");
            TextureDB.GetTexturePath("Pear").ShouldBeEquivalentTo("Fruit/pear");
            TextureDB.GetTexturePath("Pe ar").ShouldBeEquivalentTo("Fruit/pear");
        }
    }
}