﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wytches.ReptileEngine;
using FluentAssertions;
using System.IO;
using System.Threading;

namespace ReptileEngineTests
{
    [TestClass]
    public class SceneManagerTest
    {
        private SceneManager sceneManager;

        [TestInitialize]
        public void SetUp()
        {
            sceneManager = new XMLSceneManager(new MyXmlSerializer<Scene>());
        }

        [TestMethod]
        public void SaveFile()
        {
            File.WriteAllText("Test.txt", "wewewe");
        }

        [TestMethod]
        public void SaveNewSceneToFile()
        {
            Scene scene = new Scene("TestScene");
            sceneManager.SaveCurrentState(scene);

            File.Exists("../../../Wytches/Scenes/TestScene.xml").Should().BeTrue();
        }

        [TestMethod]
        public void SaveSceNeWithGameObject()
        {
            Scene scene = new Scene("OneGameObjectScene");
            scene.IsLoaded = true;
            scene.AddGameObject(SetUpGameObject());

            sceneManager.SaveCurrentState(scene);
            Thread.Sleep(1000);
            Scene loadedScene = sceneManager.LoadScene("OneGameObjectScene");

            scene.ShouldBeEquivalentTo(loadedScene);
        }

        [TestMethod]
        public void UnloadScene()
        {
            Scene scene = sceneManager.LoadScene("OneGameObjectScene");

            scene.IsLoaded.Should().BeTrue();

            sceneManager.UnloadScene(scene);

            scene.IsLoaded.Should().BeFalse();
        }

        private GameObject SetUpGameObject()
        {
            GameObject obj = new GameObject()
            {
                Tag = "TestTag"
            };
            obj.AddComponent<MeshRenderer>();

            return obj;
        }

        [TestMethod]
        public void saveGameObj()
        {
            GameObject obj = new GameObject()
            {
                Tag = "TestTag"
            };
            obj.AddComponent<MeshRenderer>();

            MyXmlSerializer<GameObject> ser = new MyXmlSerializer<GameObject>();
            ser.WriteObject("Object", obj);
        }
    }
}