﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wytches.ReptileEngine;
using System.Collections.Generic;
using FluentAssertions;

namespace ReptileEngineTests
{
    [TestClass]
    public class AnimationFileTest
    {
        private List<GameObjectAnimations> gameObjectAnimations;
        private GameObject correctGameObject;

        private GameObject corretWitch = new GameObject()
        {
            Tag = "wiedzma"
        };

        [TestInitialize]
        public void SetUp()
        {
            correctGameObject = new GameObject()
            {
                Tag = "CorrectGameObject"
            };

            gameObjectAnimations = new List<GameObjectAnimations>()
            {
                new GameObjectAnimations()
                {
                   Tag = "CorrectGameObject",
                   listAnimation = new List<GameObjectAnimation>()
                   {
                       new GameObjectAnimation()
                       {
                           Name="Idle",
                           Path="Idle\\PATH"
                       }
                   }
                },
                 new GameObjectAnimations()
                {
                   Tag = "WrongGameObject",
                   listAnimation = new List<GameObjectAnimation>()
                   {
                       new GameObjectAnimation()
                       {
                           Name="Idle",
                           Path="Idle\\PATH1"
                       }
                   }
                }
            };
            AnimationState.animations.gameObjectAnimation = gameObjectAnimations;
        }

        [TestMethod]
        public void GetCorrectAnimationPath()
        {
            string path = AnimationState.FindAnimationPathForGameObject(correctGameObject, AnimationStateName.Idle);

            path.ShouldBeEquivalentTo("Idle\\PATH");
        }

        [TestMethod]
        public void ReadAnimationFromTheFile()
        {
            AnimationState.LoadAnimationsFromTheFile("Animations");
            string path = AnimationState.FindAnimationPathForGameObject(corretWitch, AnimationStateName.Idle);

            path.ShouldBeEquivalentTo("Objects\\witchStay\\witchstay2");
        }
    }
}