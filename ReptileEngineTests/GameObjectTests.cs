﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FluentAssertions;
using Wytches.ReptileEngine;
using Microsoft.Xna.Framework.Graphics;

namespace ReptileEngineTests
{
    [TestClass]
    public class GameObjectTests
    {
        private GameObject testObject;

        [TestInitialize]
        public void SetUp()
        {
            testObject = new GameObject();
        }

        [TestMethod]
        public void AddComponent_Should_ReturnNewComponent()
        {
            (testObject.AddComponent<Component>()).Should().NotBeNull();
        }

        [TestMethod]
        [ExpectedException(typeof(ComponentAlreadyAddedException))]
        public void AddComponent_ShouldThrow_ComponentAlreadyAddedException_When_Trying_To_add_existing_component()
        {
            testObject.AddComponent<Component>();
            testObject.AddComponent<Component>();
        }

        [TestMethod]
        public void GetComponent_Should_Return_matchingComponents()
        {
            var addedComponent = testObject.AddComponent<Component>();

            var returnedComponent = testObject.GetComponent<Component>();

            returnedComponent.ShouldBeEquivalentTo(addedComponent);
        }

        [TestMethod]
        [ExpectedException(typeof(ComponentNotFoundException))]
        public void GetComponent_When_component_Was_not_Added_throw_exception()
        {
            testObject.GetComponent<Component>();
        }

        [TestMethod]
        public void GameObjectInstatiate()
        {
            GameObject newObject = GameObject.Instantiate(testObject, new Microsoft.Xna.Framework.Vector3(0.0f, 1.0f, 2.0f), new Microsoft.Xna.Framework.Vector3(0.0f, 0.0f, 0.0f), 0.0f);
            newObject.Transform.Position.X.ShouldBeEquivalentTo(0.0f);
            newObject.Transform.Position.Y.ShouldBeEquivalentTo(1.0f);
            newObject.Transform.Position.Z.ShouldBeEquivalentTo(2.0f);
            newObject.Tag.ShouldBeEquivalentTo(testObject.Tag);
            newObject.isActive.ShouldBeEquivalentTo(testObject.isActive);
        }

        [TestMethod]
        public void InstatniateObjextWithMeshRenderer()
        {
            testObject.AddComponent<MeshRenderer>();
            GameObject newObject = GameObject.Instantiate(testObject, new Microsoft.Xna.Framework.Vector3(0.0f, 1.0f, 2.0f), new Microsoft.Xna.Framework.Vector3(0.0f, 0.0f, 0.0f), 0.0f);
            newObject.ContainsComponent<MeshRenderer>().Should().BeTrue();
            newObject.GetComponent<MeshRenderer>().Model.ShouldBeEquivalentTo(testObject.GetComponent<MeshRenderer>().Model);
        }
    }
}