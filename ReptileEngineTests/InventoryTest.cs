﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wytches.Scripts;
using Wytches.ReptileEngine;
using Wytches.GameOwn;
using FluentAssertions;
namespace ReptileEngineTests
{
    [TestClass]
    public class InventoryTest
    {
        private InventoryScript inventory;
        [TestInitialize]
        public void SetUp()
        {
            inventory = new InventoryScript(new GameObject());
        }

        [TestMethod]
        public void AddItemToInventory()
        {
            inventory.AddItem(new Item(1,"TestItem",new HealthRestoration(new SimpleEffect())));
            inventory.Inventory.Should().Contain(p => p.Item != null);
        }

        [TestMethod]
        public void AddItem_ItemShould_Be_On_Position_0x0()
        {
            inventory.AddItem(new Item(1, "TestItem", new HealthRestoration(new SimpleEffect())));
            Assert.IsTrue((inventory.Inventory[0].Item != null) 
                && (inventory.Inventory[0].PositionInInventory.X == 0)); 
        }

        [TestMethod]
        [ExpectedException(typeof(InventoryFullException))]
        public void InventoryFull()
        {
            for (int i = 0; i < 20; i++)
            {
                inventory.AddItem(new Item(1, "TestItem", new HealthRestoration(new SimpleEffect())));
            }
        }
    }
}
