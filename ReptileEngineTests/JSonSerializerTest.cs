﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wytches.ReptileEngine;
using FluentAssertions;
using Wytches.Scripts;

namespace ReptileEngineTests
{
    [TestClass]
    public class JSonSerializerTest
    {
        [TestMethod]
        public void SerializeGameObject()
        {
            ISerializer<GameObject> serializer = new JsonSerializer<GameObject>();

            GameObject savedObject = new GameObject();

            serializer.WriteObject("EmptyGameObject.json", savedObject);

            GameObject readObject = serializer.ReadObject("EmptyGameObject.json");

            readObject.ShouldBeEquivalentTo(savedObject);
        }

        [TestMethod]
        public void SerializeScene()
        {
            ISerializer<Scene> serializer = new JsonSerializer<Scene>();
            GameObject obj = new GameObject()
            {
                Tag = "TestTag"
            };
            obj.AddComponent<MeshRenderer>();
            obj.AddComponent<Colider>();
            obj.AddComponent<InventoryScript>();

            GameObject savedObject = new GameObject();

            Scene scene = new Scene();

            scene.AddGameObject(obj);
            scene.AddGameObject(savedObject);

            serializer.WriteObject("Scene.json", scene);

            Scene readScene = serializer.ReadObject("Scene.json");

            readScene.ShouldBeEquivalentTo(scene);
        }
    }
}