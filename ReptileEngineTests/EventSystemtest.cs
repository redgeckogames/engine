﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wytches.ReptileEngine.EventSystem;
using Moq;
using FluentAssertions;

namespace ReptileEngineTests
{
    [TestClass]
    public class EventSystemtest
    {
        private EventManager eventManager;
        private TestEventPublisher publisher;

        [TestInitialize]
        public void SetUp()
        {
            eventManager = new EventManager();
            publisher = new TestEventPublisher();
        }

        [TestMethod]
        public void PublishEvent()
        {
            EventManager.PublishEvent("TestEvent", publisher.testAction);

            eventManager.EventList.Count.ShouldBeEquivalentTo(1);
        }

        [TestMethod]
        public void TriggerEvent()
        {
            Mock<TestEventPublisher> publisher = new Mock<TestEventPublisher>();
            publisher
                .Setup(p => p.testAction())
                .Verifiable();

            EventManager.PublishEvent("TestEvent", publisher.Object.testAction);

            EventManager.TriggerEvent("TestEvent");

            publisher.Verify(m => m.testAction(), Times.Once);
        }

        [TestMethod]
        [ExpectedException(typeof(EventNotFoundException))]
        public void TriggerNonExistingEvent()
        {
            EventManager.TriggerEvent("WrongEventName");
        }
    }

    public class TestEventPublisher
    {
        public virtual void testAction()
        {
        }
    }
}