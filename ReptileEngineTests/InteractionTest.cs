﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wytches.GameOwn;
using Wytches.GameOwn.Interactions;
using FluentAssertions;

namespace ReptileEngineTests
{
    [TestClass]
    public class InteractionTest
    {
        private Interactable Demon { get; set; }

        private Interactable Human { get; set; }

        [TestInitialize]
        public void SetUp()
        {
            InteractionSet set = new InteractionSet();
            set.AddInterAction("Hit Me!", new HitMe());
            Demon = new Demon()
            {
                Name = "Lucifer",
                Attributes = new WitchAttribiutes() { currentHealthPoints = 10, currentSpellPower = 12 },
                Interactions = set
            };

            Human = new Human()
            {
                Name = "Azazel",
                Attributes = new WitchAttribiutes() { currentHealthPoints = 10 },
                Interactions = set
            };
        }

        [TestMethod]
        public void HitDemon()
        {
            Demon.Interact("Hit Me!", 10);

            Demon.Attributes.currentHealthPoints.ShouldBeEquivalentTo(0f);
        }
    }
}