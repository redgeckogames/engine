﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wytches.ReptileEngine;
using FluentAssertions;

namespace ReptileEngineTests
{
    [TestClass]
    public class SceneTest
    {
        private Scene scene;
        private readonly string WRONG_GAME_OBJECT_TAG = "wrongTag";

        [TestInitialize]
        public void SetUp()
        {
            scene = new Scene();
        }

        [TestMethod]
        [ExpectedException(typeof(GameObjectNotFoundException))]
        public void FindGameObjectWithtag_wrongTag()
        {
            scene.AddGameObject(new GameObject());
            scene.FindGameObjectWithtag(WRONG_GAME_OBJECT_TAG);
        }

        [TestMethod]
        [ExpectedException(typeof(GameObjectNotFoundException))]
        public void RemoveNonExistingGameObjectFromTheScene()
        {
            scene.AddGameObject(new GameObject());
            scene.RemoveAllGameObjectsWithTag(WRONG_GAME_OBJECT_TAG);
        }

        [TestMethod]
        public void RemoveGameObject_CountOfObjectOnSceneHouldBe_0()
        {
            GameObject goblin = new GameObject();
            goblin.Tag = "Goblin";

            scene.AddGameObject(goblin);

            scene.RemoveAllGameObjectsWithTag("Goblin");

            scene.GetAllGameObjects().Count.ShouldBeEquivalentTo(0);
        }
    }
}