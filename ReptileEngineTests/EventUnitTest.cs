﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wytches.ReptileEngine.EventSystem;
using FluentAssertions;

namespace ReptileEngineTests
{
    [TestClass]
    public class EventUnitTest
    {
        private Event<EventArgs> testEvent;

        [TestInitialize]
        public void SetUp()
        {
            testEvent = new Event<EventArgs>()
            {
                Name = "testEvent"
            };
        }

        [TestMethod]
        public void AreEventsEqual()
        {
            Event<EventArgs> seccondEvent = new Event<EventArgs>()
            {
                Name = "testEvent"
            };

            seccondEvent.ShouldBeEquivalentTo(testEvent);
        }
    }
}