﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Wytches.GameOwn;
using FluentAssertions;

namespace WytchesSummoningTests

{
    [TestClass]
    public class NpcFactoryTest
    {
        private NpcFactory factory;

        [TestMethod]
        public void DemonFactory_ShouldReturnNPCOfTypeDemon()
        {
            factory = new DemonFactory(new DemonNameGenerator());

            Npc npc = factory.CreateNpc(new SimpleEffect().GetUssageEffect());

            npc.GetType().ShouldBeEquivalentTo(typeof(Demon));
        }

        [TestMethod]
        public void FactoryNameGenerationTest_Sholud_Generate_TheTenacious_Or_TheUndying()
        {
            factory = new DemonFactory(new DemonNameGenerator());

            Npc npc = factory.CreateNpc(new HealthRestoration(new SimpleEffect()).GetUssageEffect());

            Demon demon = npc as Demon;

            demon.Name.Should().NotBeEmpty();

            Console.WriteLine(demon.Name);
        }
    }
}